﻿#ifndef EFUNCTION_H
#define EFUNCTION_H

#include "eobject.h"
#include "edict.h"
#include "ebytecode.h"


struct eVm;
typedef struct eVm eVm;
typedef int (*native_fn)(eVm*);

typedef struct ClosureEnv
{
    edict_t upvalues;
}ClosureEnv;

typedef struct eFrame
{
   edict_t locals;
   ClosureEnv* cl_envp;
   ebyte_t* ip;
   eObject func;
   eBytecode* bcode;
   eObject lself;
}eFrame;

struct eClass;

typedef struct LetLocal
{
    char* name;
    int depth;
    bool isupvalue;
}LetLocal;

typedef struct eFunction
{
    eGcObject gcobj;
    int scope;
    esize_t argc;
    esize_t dargc;
    eObject name;
    eObject vargname;
    bool has_vargs;
    bool is_closure;
    bool is_native;
    bool is_ctor;
    bool is_method;
    bool is_child;
    esize_t true_argc;
    ebyte_t skip_addr[FUNC_ARITY];
    struct eFunction* outer;
    native_fn fp;
    ClosureEnv* cl_env;
    eObject klass;
    struct eBytecode* code;


    LetLocal let_locals[SMALL_BUFFER];

    int let_count;
    int let_depth;

    eObject lself;

}eFunction;



eFunction* efunction_new(const char* name, eBytecode *bytecode);
void efunction_delete(eFunction* f);
eObject eobj_function(const char* name, eBytecode *bytecode);
eFunction* efunction_clone(eFunction* f);

typedef struct ClosureFunc
{
    eGcObject gcobj;
    eFunction* f;
    ClosureEnv* cl_env;
}ClosureFunc;

ClosureFunc* closuref_new(eObject func, ClosureEnv *closure_env);
void closuref_delete(ClosureFunc* cl);
eObject eobj_closure_func(eObject func, ClosureEnv *closure_env);



typedef struct ClassEnv
{
    edict_t sfield;
    edict_t fields;
}ClassEnv;

typedef struct eClass
{
    eGcObject gcobj;
    eObject name;
    eObject cobject;
    bool is_c;
    bool has_init;
    bool is_inst;
    bool has_parent;
    bool is_parent;
    bool has_innerc;
    bool is_innerc;
    ClassEnv* env;
    struct eClass* parent;
    struct eClass* child;
    struct eBytecode* code;
    ebyte_t argc;
}eClass;


eClass* eclass_new(const char* name, eBytecode* bytecode);
void eclass_delete(eClass* klass);
eObject eobj_class(const char* name, eBytecode* bytecode);
eClass* eclass_clone(eClass* c);

#endif

