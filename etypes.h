#ifndef ETYPES_H
#define ETYPES_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include <ctype.h>
#include <setjmp.h>

#include "econstants.h"

#define signedi(x) ((int) x)


#ifdef DEBUG_LOG
#define elog(...) fprintf(stdout, __VA_ARGS__)
#else
#define elog(...)
#endif


typedef jmp_buf eException;
typedef unsigned long int esize_t;
typedef unsigned char ebyte_t;

struct eObject;
typedef struct eObject eObject;


typedef struct Input
{
    size_t text_len;
    size_t path_len;
    char fname[PATH_LENGTH_MAX];
    char* text;

} Input;


void* emalloc(esize_t n);
void* erealloc(void* p, esize_t n);
uint64_t eraw_time(void);

Input* input_new(FILE* file, const char* path);
char* file_text_get(const char* filename);
void input_delete(Input* input);
char *quotes(const char* str);
int isnumeric (const char * s);

#endif // ETYPES_H

