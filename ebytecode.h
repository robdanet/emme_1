#ifndef EBYTECODE_H
#define EBYTECODE_H

#include "evector.h"
#include "eobject.h"
#include "etypes.h"


typedef enum
{
    OP_HALT,
    OP_CONST,
    OP_ADD,
    OP_SUB,
    OP_MUL,
    OP_DIV,
    OP_CONCAT,
    OP_GSTORE,
    OP_GLOAD,
    OP_STORE,
    OP_LOAD,
    OP_JF,
    OP_JMP,
    OP_JMP_BACK,
    OP_FOR_ITER,
    OP_GET_ITER,
    OP_CALL,
    OP_RET,
    OP_NIL_VAL,
    OP_PRINT,
    OP_ULOAD,
    OP_MAKE_CLOSURE,
    OP_NOT,
    OP_NEG,
    OP_GT,
    OP_LT,
    OP_EQ,
    OP_NE,
    OP_AND,
    OP_OR,
    OP_USTORE,
    OP_MAKE_VECTOR,
    OP_MAKE_DICT,
    OP_ARRAY_UPDATE,
    OP_ARRAY_LOOKUP,
    OP_POP_VAL,
    OP_SETUP_LOOP,
    OP_GE,
    OP_LE,
    OP_IMPORT_LIB,
    OP_FIELD_UPDATE,
    OP_FIELD_GET,
    OP_LSHFT,
    OP_RSHFT,
    OP_FSTORE,
    OP_FLOAD,
    OP_MAKE_INSTANCE,
    OP_LSELF,
    OP_MAKE_SUBCLASS,
    OP_INERIT,
    OP_MODULO,
    OP_CALLINIT,
    OP_CALLMETH,
    OP_DUP_TOS,
    OP_COMPADD,
    OP_COMPSUB,
    OP_COMPMUL,
    OP_COMPDIV,
    OP_SWAP,
    OP_SLICEA,
    OP_SLICEA_UPDATE,
    OP_BREAK,
    OP_CONTINUE,
    OP_POP_LOOP,
    OP_LLOAD,
    OP_LSTORE,
    OP_BAND,
    OP_BOR,
    OP_BXOR

}Opcode;


typedef struct eBytecode
{
    eGcObject gcobj;
    eVector* argnames;
    eVector* locals;
    eVector* globals;
    eVector* upvalues;
    ebyte_t* raw;
    esize_t size;
    esize_t space;
    esize_t* lines;
}eBytecode;

eBytecode* bytecode_new();
void bytecode_delete(eBytecode* bytecode);
int bytecode_push(eBytecode* bcode, ebyte_t value, esize_t line);
void bytecode_writeat(eBytecode* bytecode, ebyte_t value, int idx);
bool bytecode_findlocal(eBytecode* bytecode, eObject obj, ebyte_t *idx);
bool bytecode_findglobal(eBytecode* bytecode, eObject obj, ebyte_t* idx);
ebyte_t bytecode_constadd(eBytecode* bytecode, eObject obj);
ebyte_t bytecode_constaddg(eBytecode* bytecode, eObject obj);
ebyte_t bytecode_constadduv(eBytecode* bytecode, eObject obj);



#endif // EBYTECODE_H

