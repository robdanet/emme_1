#ifndef ECOMPILER_H
#define ECOMPILER_H

#include "elexer.h"
#include "emme.h"

typedef struct Local
{
    eToken token;
    int depth;
}Local;

typedef struct eCompiler
{
    eToken last;
    eToken current;
    eLexer* lexer;

    eBytecode* bcode;
    eObject curr_fn;
    int depth;
    int fn_scope;
    ErrorType err;
    const char* err_msg;
    bool external;


    bool can_return;
    int inside_classbdy;
    int inside_loop;
    int continue_addr;

}eCompiler;


ErrorType compile_source(eCompiler* compiler);
ErrorType eval_script(const char* path);
ErrorType print_comperr(eCompiler* compiler);
void init_front(eLexer* lexer, eCompiler* compiler, const char* source, const char* filename);
ErrorType compile_line(eCompiler* compiler);
//extern void syntax_error(eCompiler* compiler, const char* msg);
#endif // ECOMPILER_H

