#include "ebytecode.h"

eBytecode* bytecode_new()
{
    eBytecode* bytecode = (eBytecode*)egcobject_new(EBCODE, sizeof(eBytecode));
    bytecode->size = 0;
    bytecode->space = 0;
    bytecode->raw = NULL;
    bytecode->lines = NULL;

    bytecode->argnames = evector_new();
    bytecode->locals = evector_new();
    bytecode->upvalues = evector_new();
    bytecode->globals = evector_new();

    return bytecode;
}

void bytecode_delete(eBytecode* bytecode)
{
    free(bytecode->raw);
    free(bytecode->lines);
    free(bytecode);
}

int bytecode_push(eBytecode* bcode, ebyte_t value, esize_t line)
{
    if ((bcode->space - bcode->size) == 0)
    {
        void* r = NULL, *l = NULL;
        bcode->space += ARRAY_GROW_SZ;
        r = erealloc(bcode->raw, bcode->space * sizeof(ebyte_t));
        l = erealloc(bcode->lines, bcode->space * sizeof(esize_t));
        bcode->raw = r;
        bcode->lines = l;
    }
    bcode->raw[bcode->size] = value;
    bcode->lines[bcode->size++] = line;
    return bcode->size - 1;
}

void bytecode_writeat(eBytecode* bytecode, ebyte_t value, int idx)
{
    if (idx < 0 || idx >= signedi(bytecode->size))
    {
        fprintf(stderr,"trying to write out of bytecode array's bounds' ");
        exit(1);
    }
    bytecode->raw[idx] = value;
}

bool bytecode_findlocal(eBytecode* bytecode, eObject obj, ebyte_t* idx)
{
    for (ebyte_t i=0; i<bytecode->locals->size; i++)
    {
        if (eobj_isequal(bytecode->locals->data[i], obj))
        {
            //char* str = eobj_to_cstr(bytecode->locals.constants[i]);
            //char* str2 = eobj_to_cstr(obj);
            //elog("lobj %s == %s\n",str, str2);
            //free(str); free(str2);
            return *idx = i, true;
        }
    }
//    for (ebyte_t i=0; i<bytecode->argnames->size; i++)
//    {
//        if (eobj_isequal(bytecode->argnames->data[i], obj))
//        {
//            //char* str = eobj_to_cstr(bytecode->locals.constants[i]);
//            //char* str2 = eobj_to_cstr(obj);
//            //elog("lobj %s == %s\n",str, str2);
//            //free(str); free(str2);
//            return *idx = i, true;
//        }
//    }
    return false;
}

bool bytecode_findglobal(eBytecode* bytecode, eObject obj, ebyte_t* idx)
{
    for (ebyte_t i=0; i<bytecode->globals->size; i++)
    {
        if (eobj_isequal(bytecode->globals->data[i], obj))
        {
            return *idx = i, true;
        }
    }
    return false;
}

ebyte_t bytecode_constadd(eBytecode* bytecode, eObject obj)
{
    ebyte_t idx;
    if (bytecode_findlocal(bytecode, obj, &idx))
    {
        return idx;
    }
    return evector_push(bytecode->locals, obj);
}

ebyte_t bytecode_constaddg(eBytecode* bytecode, eObject obj)
{
    ebyte_t idx;
    if (bytecode_findglobal(bytecode, obj, &idx))
    {
        return idx;
    }
    return evector_push(bytecode->globals, obj);
}

ebyte_t bytecode_constadduv(eBytecode* bytecode, eObject obj)
{
    return evector_push(bytecode->upvalues, obj);
}




