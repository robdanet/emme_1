#ifndef EERRORS_H
#define EERRORS_H

#include "etypes.h"

struct eLexer;
struct eCompiler;

typedef enum
{
    NoError,
    SyntaxError,
    RuntimeError,
    SystemError

}ErrorType;

typedef struct Syntaxerror
{
    char* filename;
    char* msg;
    long line;
    long col;

}Syntaxerror;

typedef struct Runerror
{
    ErrorType type;
    char* filename;
    char* msg;
    long line;

}Runerror;


#endif // EERRORS_H

