#ifndef ELEXER_H
#define ELEXER_H

#include "etypes.h"


typedef enum
{
    /* operators */
    ETOKEN_PLUS, ETOKEN_MINUS, ETOKEN_MULT, ETOKEN_DIV, ETOKEN_EQUAL,
    ETOKEN_OR, ETOKEN_AND, ETOKEN_NOT, ETOKEN_MODULO,
    ETOKEN_B_OR, ETOKEN_B_AND, ETOKEN_B_XOR,
    ETOKEN_EQ, ETOKEN_NE, ETOKEN_LT, ETOKEN_GT, ETOKEN_LE, ETOKEN_GE,
    ETOKEN_DOT_DOT, ETOKEN_ELLIPSIS, ETOKEN_R_SHIFT, ETOKEN_L_SHIFT,

    /* unary operators */
    ETOKEN_PLUS_PLUS, ETOKEN_MINUS_MINUS,

    /* compound operators */
    ETOKEN_PLUS_EQUAL, ETOKEN_MINUS_EQUAL, ETOKEN_MULT_EQUAL, ETOKEN_DIV_EQUAL,

    /* delimiters */
    ETOKEN_BANG, ETOKEN_L_PAREN, ETOKEN_R_PAREN,
    ETOKEN_L_CURLY, ETOKEN_R_CURLY,
    ETOKEN_L_BRACK, ETOKEN_R_BRACK,
    ETOKEN_COLON, ETOKEN_S_COLON, ETOKEN_COMMA, ETOKEN_POINTER, ETOKEN_DOT,

    /* atoms */
    ETOKEN_STRING, ETOKEN_FRMT_STRING,
    ETOKEN_NUMBER, ETOKEN_XNUMBER, ETOKEN_SYM,

    /* keywords - reserved words*/
    ETOKEN_TRUE, ETOKEN_FALSE, ETOKEN_NIL,
    ETOKEN_FUNC, ETOKEN_CLASS, ETOKEN_VAR, ETOKEN_LAMBDA, ETOKEN_INIT,
    ETOKEN_PRINT, ETOKEN_RET, ETOKEN_NEW, ETOKEN_SELF, ETOKEN_LET,
    ETOKEN_SUPER, ETOKEN_ASM, ETOKEN_IF, ETOKEN_ELIF, ETOKEN_ELSE,
    ETOKEN_WHILE, ETOKEN_FOR, ETOKEN_FOREACH, ETOKEN_SWITCH, ETOKEN_DO,
    ETOKEN_CASE, ETOKEN_IN, ETOKEN_IMPORT, ETOKEN_FROM, ETOKEN_AS, ETOKEN_USE,
    ETOKEN_SKIP, ETOKEN_BREAK,

    /* other */
    ETOKEN_EOS, ETOKEN_ERROR, ETOKEN_COMMERR, ETOKEN_UNKNOWN

} TokenType;

typedef struct eToken
{
    TokenType type;
    const char* start;
    esize_t line;
    esize_t length;
}eToken;

typedef struct eLexer
{
    const char* filename;
    const char* ln_start;
    const char* tk_start;
    const char* textp;
    esize_t line;
}eLexer;

void lexer_init(eLexer* lexer, const char* source, const char* filename);

eToken token_error(eLexer* lexer, const char* msg);
bool is_eos(eLexer* lexer);
void print_line_with_error(eLexer* lexer, const char *msg);
eToken token_get(eLexer* lexer);


#endif // ELEXER_H

