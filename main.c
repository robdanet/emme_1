//#include "eobject.h"
//#include "efunction.h"
#include "emme.h"
//#include "evector.h"
//#include "edict.h"
#include "elexer.h"
//#include "ebytecode.h"
//#include "econstants.h"
#include "edisasm.h"
#include "ecompiler.h"
#include "elibs.h"
#include "evm.h"

eVm vm;
eGc gc;
jmp_buf go;


static void gc_markall(eVm* vm)
{
    for (eObject* p=&vm->stack[0]; p<vm->sp; p++)
    {
        //        printf("mark object @ %p\n", p);
        gc_mark(vm->sp);
    }
}

void gc_run(eGc* gc, eVm* vm)
{
    elog("RUN GC");
    gc_markall(vm);
    gc_sweep(gc);
}

void dump_code(eVm* vm, eCompiler* compiler)
{
    evm_init(vm,OBJ_AS_FUNC(compiler->curr_fn)->code, compiler->lexer->filename);
    int counter = vm->bcode->size;
    printf("core dump: size %d", counter);
    do{
        ebyte_t b = FETCH();
        printf("\\%d", b);
    }while(counter--);
}


#define esetjump(b) if(!setjmp(b))
#define EMME_TRY(buf) esetjump(buf)
#define EMME_CATCH else
#define EMME_THROWS(e,m) \
    do{\
    fprintf(stderr,"%s\n",m);\
    print_line_with_error(&lexer);\
    free((char*)m);\
    return e;\
    } while (0)


void init_back(eVm* vm, eCompiler* compiler)
{
    vm->bcode = OBJ_AS_FUNC(compiler->curr_fn)->code;
    vm->sp = vm->stack;
    vm->csp = vm->cstack;
    vm->ip = vm->bcode->raw;
    vm->fp = vm->call_stack;
    vm->cp = vm->callers_stack;
    vm->bsp = vm->block_stack;
    vm->run = true;
    vm->filename = compiler->lexer->filename;
}

void repl_auto_print(eVm* vm)
{
    if (eobj_isequal(TOS, NIL))return;
    char* str = eobj_to_cstr(TOS);
    printf("%s\n", str);
    free(str);
}

ErrorType repl_run(eVm* vm, eCompiler* compiler)
{
    ErrorType error = NoError;
    if (!setjmp(go))
    {
        error = run_bcode(vm);
        if (error == NoError && (vm->sp - vm->stack)<2)repl_auto_print(vm);
        dis_bcode(OBJ_AS_FUNC(compiler->curr_fn)->code, "emme dis");
    }
    else
    {
        return RuntimeError;
    }
    return error;
}

void clean(eVm* vm, eLexer* lexer)
{

    evm_delete(vm);
}

#define REPL_HEADER "Emme language interactive interpreter"
#define EMME_VER_STR "0.0.1"
#define EMME_COPYRIGHT "Copyright RobDaNet 2019"
#define PROMPT1 printf("> ")
#define TABS1   printf(". ")

static void repl_banner()
{
    printf("%s v(%s).\n", REPL_HEADER, EMME_VER_STR);
    printf("%s\n\n", EMME_COPYRIGHT);
}

static void repl()
{
    ErrorType error = NoError;
    eLexer lexer;
    eCompiler compiler;
    eVm vm;
    edict_init(&vm.globals, EMME_DICT_SIZE);

    repl_banner();

    init_libs(&vm);
    make_bltins_type();
    char line[LARGE_BUFFER];

       for (;;) {

           PROMPT1;
        if (!fgets(line, sizeof(line), stdin)) {
            line[strlen(line)] = '\0';
            printf("\n");
            break;
        }

        if (strchr(line, '{') != NULL)
        {
            esize_t index = 0;

            for (;;) {

                char open = 0, close = 0;
                char buff[LARGE_BUFFER];

                TABS1;
                if (!fgets(buff, sizeof(buff), stdin)) {
                    printf("\n");
                }

                strcat(line, buff);
                index = strlen(line);
                for (esize_t i=0; i<index; i++)
                {
                    if (line[i] == '{') open++;
                    if (line[i] == '}') close++;
                }
//                printf("open %lu\n", open);
//                printf("close %lu\n", close);
                if (open == close)break;
            }
        }

        init_front(&lexer, &compiler, line, "<stdin>");
        error = compile_line(&compiler);
        if (error != NoError)continue;
        init_back(&vm, &compiler);
        error = repl_run(&vm, &compiler);fflush(stdin);
        if (error != NoError)continue;
        if (vm.run == false)break;
    }
    clean(&vm, &lexer);
}

//#define dict_cast(obj) ((edict_t*)obj)
//void workit(eGcObject* obj)
//{
//    edict_t* dict = dict_cast(obj);
//    edict_insert(dict, "Hello1", NUMBER(10));
//    edict_insert(dict, "Hello2", NUMBER(20));
//    edict_insert(dict, "Hello3", NUMBER(30));
//    edict_print(dict);

//    //printf("type %d\n", dict->gcobj.type);

//}



int main(int argc, char** argv)
{
    double start_time = eraw_time();
    if (argc == 1)
    {
        repl();
    }
    else if (argc == 2)
    {
        gc_init(&gc);
        eval_script(argv[1]);
//        eval_source("", "<stdin>");

        gc_run(&gc, &vm);
    }
    printf("%f sec.\n\n",(double)(eraw_time() - start_time) / 1000000000.0);


#if defined(_WIN32)
    getchar();
#endif

//    int base = 16;
//    char *endptr, *str = "0x121";
//    long val;
//    val = strtol(str, &endptr, base);
//    printf("strtol() returned %ld %s\n", val, endptr);

    //            eVector* v;
    //            v = evector_new();

    //            evector_push(v,NUMBER(2));
    //            evector_push(v,NUMBER(20));
    //            evector_push(v,NUMBER(30));
    //            evector_push(v, STRING("Hello"));
    //            evector_push(v, STRING("Emme_1"));

    //            eObject o = evector_splice(v,3,2);
    //            char str[1000];
    //            evector_to_cstring(v, str);
    //            printf("%s %lu %s\n", str, v->size, eobj_to_cstr(o));
    //            evector_delete(v);

    //        printf("value %g\n",OBJ2FLOAT(v.data[0]));
    //        printf("value %ld\n",OBJ2INT(v.data[1]));
    //        printf("value %ld\n",OBJ2INT(v.data[2]));
    //        printf("value %s\n",OBJ2CSTR(v.data[0]));
    //        printf("value %s\n",OBJ2CSTR(v.data[1]));
    //    eobj_delete(v.data[0]);
    //    eobj_delete(v.data[1]);
    //    eObject n = NUMBER(3.14);

//        edict_t* dict = edict_new(EMME_DICT_SIZE);

//        printf("%d\n", edict_insert(dict,"tango",NUMBER(10)));
//        printf("%d\n", edict_insert(dict,"beta",NUMBER(5)));
//        printf("%d\n", edict_insert(dict,"alpha",NUMBER(22)));
//        printf("%d\n", edict_insert(dict,"gamma",NUMBER(2)));

//        edict_sortvalues(dict);
////    ////    edict_insert(&dict,"PI",&n);
//        char buf[80];
//        edict_to_cstring(dict, buf);
//        printf("%s\n", buf);

//        printf("%d\n", edict_insert(dict,"Gamma",NUMBER(2)));
//        printf("%d\n", edict_insert(dict,"Famma",NUMBER(2)));
//        printf("%d\n", edict_insert(dict,"namma",NUMBER(2)));
//        edict_to_cstring(dict, buf);
//        printf("%s\n", buf);

    //    eObject ref = edict_find(&dict,"twenty");
    //    void* vref = edict_find(&dict,"PI");
    //    printf("value %ld\n",OBJ2INT(ref));
    //    printf("value %g\n",OBJ2FLOAT(OBJUNREF(vref)));

    //    evector_delete(&v);
    //    edict_erase(&dict);
    //        char* b = "tre";
    //        eObject s = efmtstring_fromtext(b,3);
    ////        eObject vec = eobj_vector(v);
    //        array_iterator* it = make_iter(&s , has_next);
    //        while( it->has_next(it->ary, it) )
    //        {
    //            char* str = OBJ_AS_CSTR(iter(it));

    //            printf("--%s\n",str);
    //        }
    //   37.60334523677632
    //   printf("%.9g",123456789.1);
    //    edict_t dst;
    //    char* keys[3]={"uno", "due", "tree"};
    //    edict_init(&src,EMME_DICT_SIZE);
    //    edict_init(&dst,EMME_DICT_SIZE);
    //    edict_insert(&src,&keys[0][0],NUMBER(10));
    //    edict_insert(&src,&keys[1][0],NUMBER(20));
    //    edict_insert(&src,&keys[2][0],NUMBER(30));
    //    edict_copy(&dst, &dict);
    //    printf("size %lu\n", dst.size);
    //    eObject ref2 = edict_find(&dst,"twenty");
    //    void* vref = edict_find(&dict,"PI");
    //    printf("value %ld\n",OBJ2INT(ref2));
    //    eTime* t = etime_new();
    //    time(&t->rawtime);
    //    t->timeinfo = localtime(&t->rawtime);
    //    char buffer [80];
    //    strftime (buffer,80,"Now it's %I:%M%p.",t->timeinfo);
    //    puts(buffer);
    //    FILE* fp = stdin;
    //    printf("%c",fgetc(fp));
//    eArray array;
//    array.size = 0;
//    array.space = ARRAY_GROW_SZ;
//    array.data = NULL;
//    array.data = EXPAND(eObject, array.data, ARRAY_GROW_SZ);
//    array_push(eObject, array, NUMBER(10));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, BOOL(false));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    array_push(eObject, array, NUMBER(20));
//    printf("%lu %lu", array.size, array.space);


//    eGcObject* objp = (eGcObject*)edict_new(EMME_DICT_SIZE);
//    workit(objp);

    return 0;
}


