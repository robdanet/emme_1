#ifndef EMME
#define EMME

#include "etypes.h"
#include "eobject.h"
#include "evector.h"
#include "ebytecode.h"
#include "edict.h"
#include "econstants.h"
#include "efunction.h"
#include "eerrors.h"
#include "evm.h"


extern eVm vm;
extern eGc gc;
extern jmp_buf go;



extern void evm_init(eVm* vm, eBytecode* bcode, const char* filename);
extern void evm_delete(eVm*);
extern ErrorType run_bcode(eVm* vm);
extern void dis_bcode(eBytecode* bytecode, const char* name);

extern int line_index(eVm* vm);
extern ErrorType runtime_error(eVm* vm, const char* msg);
extern void fmt_error(eVm* vm, char* fmt, ...);
extern void make_bltins_type();
void init_libs(eVm* vm);
extern eClass* get_builtin_instance(ValueType type);


typedef struct BuiltinFunc
{
    const char* name;
    native_fn fp;
}BuiltinFunc;

typedef struct LibField
{
    char* name;
    eObject value;
}LibField;

typedef struct LibMethod
{
    const char* name;
    native_fn fp;
}LibMethod;

typedef struct Library
{
    char* name;
    LibMethod* methods;
    LibField* fields;
}Library;

struct LibManager;

extern void init_library(char* name,
                         struct LibManager* manager,
                         struct Library* library,
                         struct LibMethod* methods,
                         struct LibField* fields);


// c module loading functions
extern eVector* stack_getargs(eVm* vm, int leaven);

extern int scan_argvec(eVm* vm, eVector* argv, char* fmt, ...);

#endif // EMME

