#include "evector.h"
#include "etypes.h"
#include "eobject.h"

eVector *evector_new()
{
    eVector* v = (eVector*)egcobject_new(EVECTOR, sizeof(eVector));
    v->size = 0;
    v->space = 0;
    v->data = NULL;
    return v;
}

eObject eobj_vector(eVector* v)
{
    eObject obj;
    obj.type = EVECTOR;
    obj.value.object = v;

    return obj;
}

void evector_delete(eVector* v)
{
    free(v->data);
    free(v);
    v = NULL;
}

esize_t evector_push(eVector* v, eObject element)
{
    if ((v->space - v->size) == 0)
    {
        void* t = NULL;
        v->space += VECTOR_GROW_SZ;
        t = erealloc(v->data, v->space * sizeof(eObject));

        v->data = t;
    }
    v->data[v->size++] = element;
    return v->size - 1;
}

eObject evector_pop(eVector* v)
{
    if (v->size == 0)return NIL;
    eObject r = v->data[v->size - 1];

    void* t = NULL;
    t = erealloc(v->data, (v->size - 1) * sizeof(eObject));
    v->data = t;
    v->size -= 1;
    return r;
}

eObject evector_find(eVector* v, eObject look, bool* found, int* idx)
{
    for (esize_t i=0; i<v->size; i++)
        if (eobj_isequal(v->data[i], look))
        {
            *found = true;
            *idx=i;
            return v->data[i];
        }
    *found = false;
    return NIL;
}

eObject evector_splice(eVector* v, esize_t idx, esize_t num)
{
    if (idx >= v->size && idx < 0 || (idx + num) > v->size || (idx + num) < 0)
        return NIL;

    eVector* t = evector_new();
    esize_t old_size = v->size;
    esize_t new_size = v->size - num;

    eObject res;

    eVector* vr = NULL;
    if (num > 1)
        vr = evector_new();
    else
        res = v->data[idx];



    for (esize_t i=0; i<old_size; i++)
    {
        if (i >= idx && i < (idx + num))
        {
            if (num > 1)evector_push(vr, v->data[i]);
        }
        evector_push(t, v->data[i]);
    }
    v->data = erealloc(v->data, (new_size) * sizeof(eObject));
    for (esize_t i=0; i<new_size; i++)
    {
        v->data[i] = t->data[i];
    }
    v->size = new_size;

    if (num > 1)res = eobj_vector(vr);
    return res;
}

void evector_to_cstring(eVector* v, char* result)
{

    char buf[1024];

    memset(buf, '\0', 1024);

    strcpy(buf, "[");
    for (esize_t i=0; i<v->size; i++)
    {
        char* strp = eobj_to_cstr(v->data[i]);
        strcat(buf, strp);
        free(strp); strp = NULL;

        if (i != v->size - 1)
        strcat(buf, ", ");

    }
    strcat(buf, "]");
    strcpy(result, buf);
}
