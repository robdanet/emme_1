#ifndef EDISASM
#define EDISASM

#include "ebytecode.h"

void dis_bcode(eBytecode* bytecode, const char* name);

#endif // EDISASM

