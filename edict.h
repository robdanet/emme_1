#ifndef EMME_DICT_H
#define EMME_DICT_H

#include "eobject.h"

#define EMME_DICT_SIZE 1


struct dcell
{
    int key;
    bool empty;
    char* str;
    eObject obj;
};

typedef struct dcell dcell;

struct edict_t
{
    eGcObject gcobj;
    dcell** data;
    esize_t size;
    esize_t space;
    esize_t collisions;
};

typedef struct edict_t edict_t;


esize_t hash_it(const char *s, esize_t size);

edict_t* edict_new(esize_t space);

void edict_delete(edict_t* d);

void edict_init(edict_t* dict, esize_t space);

void edict_erase(edict_t* dict);

int query_dict(edict_t* dict, int key);

int edict_insert(edict_t* dict, char* key_string, eObject obj);

void rehash(edict_t* dict);

eObject edict_find(edict_t* dict, const char* key_string);

void edict_copy(edict_t* dst, edict_t* src);

void edict_copy_all(edict_t* dst, edict_t* src);

void edict_sortkeys(edict_t* d);

int edict_sortvalues(edict_t* d);

void edict_print(edict_t* d);

void edict_to_cstring(edict_t* d, char* result);


#endif
