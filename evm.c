#include "evm.h"
#include "ecompiler.h"
#include "edisasm.h"
#include "elibs.h"


#define fnhasvararg(f) (OBJ_AS_FUNC(f)->has_vargs)
#define clhasvararg(c) (OBJ_AS_CLOSURE(c)->f->has_vargs)


double eobj_to_cnum(eVm* vm, eObject obj)
{
    if (objisnumber(obj))
        return OBJ_AS_FLOAT(obj);
    if (objisstring(obj) && isnumeric(OBJ_AS_CSTR(obj)))
    {
        char* str = OBJ_AS_CSTR(obj);
        double num = atof(str);
        return num;
    }
    runtime_error(vm, "Object cannot be converted to number\n");
    return -1;
}


int line_index(eVm* vm)
{
    return (vm->ip - vm->bcode->raw - 1) >= 0 ? (vm->ip - vm->bcode->raw - 1) : 0;
}

void evm_init(eVm* vm, eBytecode* bcode, const char* filename)
{
//    memset(vm->stack, 0, sizeof(eObject) * STACK_MAX);
//    memset(vm->cstack, 0, sizeof(eObject) * STACK_MAX);
//    memset(vm->call_stack, 0, sizeof(eFrame) * CALL_STACK_MAX);
//    memset(vm->callers_stack, 0, sizeof(eObject) * CALL_STACK_MAX);
//    memset(vm->block_stack, 0, sizeof(LoopBlock) * BLOCK_STACK_MAX);
    vm->bcode = bcode;
    vm->sp = vm->stack;
    vm->csp = vm->cstack;
    vm->bsp = vm->block_stack;
    vm->cp = vm->callers_stack;
    vm->ip = vm->bcode->raw;
    vm->fp = vm->call_stack;
    vm->run = true;
    vm->filename = filename;
    edict_init(&vm->globals, EMME_DICT_SIZE);
    init_libs(vm);
    make_bltins_type();
}

void evm_delete(eVm* vm)
{
    edict_erase(&vm->globals);
}

ErrorType runtime_error(eVm* vm, const char* msg)
{
    fprintf(stderr, FILE_LINE_FMT("Error! %s.\n"),
            vm->filename, vm->bcode->lines[line_index(vm)], msg);
    longjmp(go, RuntimeError);
}

ErrorType eval_impl(const char* source, const char* filename)
{
    ErrorType error = NoError;
    eLexer lexer;
    eVm vm;
    eCompiler compiler;

    lexer_init(&lexer, source, filename);
    compiler.lexer = &lexer;

    if(!setjmp(go))
    {
        error = compile_source(&compiler);
    }
    else
    {
        return print_comperr(&compiler);
    }

    eBytecode* prev_bytes = vm.bcode;
    ebyte_t* tmp_ip = vm.ip;

    if (!setjmp(go))
    {
        evm_init(&vm, OBJ_AS_FUNC(compiler.curr_fn)->code, filename);

        error = run_bcode(&vm);

    }
    else
    {
        return RuntimeError;
    }

//    dis_bcode(OBJ2FUNC(compiler.curr_fn)->code, "eval");
//    dump_code(&vm, &compiler);
    vm.bcode = prev_bytes;
    vm.ip = tmp_ip;


    evm_delete(&vm);
    return error;
}

eObject eval_str(const char* source)
{
    eObject result;
    eLexer lexer;
    eVm vm;
    eCompiler compiler;

    lexer_init(&lexer, source, "<stdin>");
    compiler.lexer = &lexer;

    if(!setjmp(go))
    {
        /*result = */compile_source(&compiler);
    }
    else
    {
        print_comperr(&compiler);
    }

    eBytecode* prev_bytes = vm.bcode;
    ebyte_t* tmp_ip = vm.ip;


    if (!setjmp(go))
    {
        evm_init(&vm, OBJ_AS_FUNC(compiler.curr_fn)->code, "<stdin>");

        run_bcode(&vm);

    }
    else
    {
        return NIL;
    }
    //evm_init(&vm, OBJ_AS_FUNC(compiler.curr_fn)->code, "<stdin>");
    /*result = *///run_bcode(&vm);
    result = tos(&vm);
//    dis_bcode(OBJ2FUNC(compiler.curr_fn)->code, "eval");
//    dump_code(&vm, &compiler);
//    vm.bcode = prev_bytes;
//    vm.ip = tmp_ip;

    evm_delete(&vm);

    return result;
}

static void dump_stack(eVm* vm)
{
#ifdef TRACE_STACK
    printf("\n[");
    for (eObject* e=vm->stack; e<vm->sp; e++)
    {
        print_object(*e);
    }
    printf("]\n");
#endif
#ifdef TRACE_CSTACK
    printf("\nC[");
    for (eObject* e=vm->cstack; e<vm->csp; e++)
    {
        print_object(*e);
    }
    printf("]\n");
#endif
#ifdef TRACE_CALLERS_STACK
    printf("\n:[");
    for (eObject* e=vm->callers_stack; e<vm->cp; e++)
    {
        print_object(*e);
    }
    printf("]\n");
#endif
#ifdef TRACE_LOOP_STACK
    printf("\nB[");
    for (LoopBlock* b=vm->block_stack; b<vm->bsp; b++)
    {
        printf("(%d, %d) ", b->start, b->end);
    }
    printf("]\n");
#endif
}

#define is_above(ptr, array, size) (ptr >= array + size)
#define is_below(ptr, array) (ptr < (array - 1))

eObject pop(eVm* vm)
{
//    if (is_below(vm->sp, vm->stack))
//    {
//        fprintf(stderr, "Stack underflow !\n");
//        exit(EXIT_FAILURE);
//    }
    vm->sp--;
    return *vm->sp;
}

void push(eVm* vm, eObject value)
{
    if (is_above(vm->sp, vm->stack, STACK_MAX))
    {
        fprintf(stderr, "Stack overflow !\n");
        exit(EXIT_FAILURE);
    }
    *vm->sp = value;
    vm->sp++;
}

eObject cpop(eVm* vm)
{
    if (is_below(vm->csp, vm->cstack))
    {
        fprintf(stderr, "Stack underflow !\n");
        exit(EXIT_FAILURE);
    }
    vm->csp--;
    return *vm->csp;
}

void cpush(eVm* vm, eObject value)
{
    if (is_above(vm->csp, vm->cstack, STACK_MAX))
    {
        fprintf(stderr, "Stack overflow !\n");
        exit(EXIT_FAILURE);
    }
    *vm->csp = value;
    vm->csp++;
}

LoopBlock block_pop(eVm* vm)
{
    vm->bsp--;
    return *vm->bsp;
}

LoopBlock block_top(eVm* vm)
{
    return *(vm->bsp-1);
}

void block_push(eVm* vm, LoopBlock b)
{
    *vm->bsp = b;
    vm->bsp++;
}

eObject tos(eVm* vm)
{
    return *(vm->sp-1);
}

eObject tos1(eVm* vm)
{
    return *(vm->sp-2);
}

eObject tos2(eVm* vm)
{
    return *(vm->sp-3);
}

eObject tosN(eVm* vm, int n)
{
    return *(vm->sp-n-1);
}

#define apply_op(a,b,op) (NUMBER(((a) op (b))))
#define apply_cmp_op(a,b,op) (BOOL(((a) op (b))))

#define operation(vm, left, right, op) {\
  double l, r;\
  l = eobj_to_cnum(vm, left);\
  r = eobj_to_cnum(vm, right);\
  PUSH(apply_op(l, r, op));\
}

#define binary_op(vm, op) {\
    eObject right = POP();\
    eObject left = POP();\
    operation(vm, left,right,op)\
}

/* operations on integer */
#define ioperation(vm, left, right, op) {\
  long l, r;\
  l = (long)eobj_to_cnum(vm, left);\
  r = (long)eobj_to_cnum(vm, right);\
  PUSH(apply_op(l, r, op));\
}

#define ibinary_op(vm, op) {\
    eObject right = POP();\
    eObject left = POP();\
    ioperation(vm, left,right,op)\
}

/* comparative and logic operations */
#define cmp_operation(vm, left, right, op) {\
    double l, r;\
    l = eobj_to_cnum(vm, left);\
    r = eobj_to_cnum(vm, right);\
    PUSH(apply_cmp_op(l, r, op));\
}

#define cmp_binary_op(vm, op) {\
    eObject right = POP();\
    eObject left = POP();\
    cmp_operation(vm, left,right,op)\
}

void make_vector_slice(eVm* vm, esize_t size)
{
    eVector* a = evector_new();
    if (size > 0)
    {
        for(esize_t i=size; i>0; i--)
        {
            evector_push(a, *(vm->sp - i));
        }
        vm->sp -= size;
    }
    eObject array = eobj_vector(a);
    PUSH(array);
}

eObject find_field(eVm* vm, eClass* the_class, const char* name)
{
    eObject field = edict_find(&the_class->env->fields, name);
    if (eobj_isequal(NONE, field))
    {
        if (the_class->has_parent)
        {
            elog("search parent...");
            field = find_field(vm, the_class->parent, name);
        }
        else
        {
            return NONE;
        }
    }
    return field;
}

ErrorType run_bcode(eVm* vm)
{
    register eFrame* fp = vm->fp;
    do
    {
        dump_stack(vm);
        switch (FETCH()) {
        case OP_HALT:
            return NoError;
            break;
        case OP_CONST:
//            push(vm, vm->bcode->locals.constants[FETCH()]);
            push(vm, vm->bcode->locals->data[FETCH()]);
            break;
        case OP_POP_VAL:
            vm->sp--;
            break;
        case OP_ADD:
            binary_op(vm, +);
            break;
        case OP_SUB:
            binary_op(vm, -);
            break;
        case OP_MUL:
            binary_op(vm, *);
            break;
        case OP_DIV:
            binary_op(vm, /);
            break;
        case OP_MODULO:
            ibinary_op(vm, %);
            break;
        case OP_LSHFT:
            ibinary_op(vm, <<);
            break;
        case OP_RSHFT:
            ibinary_op(vm, >>);
            break;
        case OP_COMPADD:
            binary_op(vm, +);
            break;
        case OP_COMPSUB:
            binary_op(vm, -);
            break;
        case OP_COMPMUL:
            binary_op(vm, *);
            break;
        case OP_COMPDIV:
            binary_op(vm, /);
            break;
        case OP_GT:
            cmp_binary_op(vm, >);
            break;
        case OP_LT:
            cmp_binary_op(vm, <);
            break;
        case OP_GE:
            cmp_binary_op(vm, >=);
            break;
        case OP_LE:
            cmp_binary_op(vm, <=);
            break;
        case OP_EQ:
        {
            eObject right = POP();
            eObject left = POP();
            bool r = eobj_isequal(left, right);
            PUSH(BOOL(r));
        }
            break;
        case OP_NE:
        {
            eObject right = POP();
            eObject left = POP();
            bool r = eobj_isequal(left, right);
            PUSH(BOOL(!r));
        }
            break;
        case OP_BAND:
            ibinary_op(vm, &);
            break;
        case OP_BXOR:
            ibinary_op(vm, ^);
            break;
        case OP_BOR:
            ibinary_op(vm, |);
            break;
        case OP_AND:
        {
            eObject right = POP();
            eObject left = POP();
            if(!eobj_istrue(left))PUSH(left);
            else PUSH(right);
        }
            break;
        case OP_OR:
        {
            eObject right = POP();
            eObject left = POP();
            if(eobj_istrue(left))PUSH(left);
            else PUSH(right);
        }
            break;
        case OP_NOT:
        {
            eObject obj = POP();
            if (obj.type == EBOOL)
            {
                bool r = OBJ_AS_BOOL(obj);
                PUSH(BOOL(!r));
            }
            else
            {
                PUSH(BOOL(!eobj_istrue(obj)));
            }
        }
            break;
        case OP_NEG:
        {
            eObject obj = POP();
            if (obj.type == ENUMBER)
            {
                double r = OBJ_AS_FLOAT(obj);
                PUSH(NUMBER(-r));
            }
        }
            break;
        case OP_CONCAT:
        {
            eObject o1 = POP();
            eObject o2 = POP();
            eString* s1 = NULL, *s2 = NULL;

            if (!objisstring(o1))
            {
                char* str = eobj_to_cstr(o1);
                s1 = estring_new(str);
            }
            else
            {
                s1 = OBJ_AS_STR(o1);
            }
            if (!objisstring(o2))
            {
                char* str = eobj_to_cstr(o2);
                s2 = estring_new(str);
            }
            else
            {
                s2 = OBJ_AS_STR(o2);
            }
            esize_t len = s1->size + s2->size + 1;
            char* buf = emalloc(sizeof(char) * len);
            strcpy(buf, s2->text);
            strcat(buf, s1->text);
            eObject r = eobj_string(buf); free(buf);
            PUSH(r);
        }
            break;
        case OP_GSTORE:
        {
            eObject name = vm->bcode->globals->data[FETCH()];
            elog("\nGStore %s\n", OBJ_AS_CSTR(name));
            eObject value = POP();
            edict_insert(&vm->globals, OBJ_AS_CSTR(name), value);
        }
            break;
        case OP_GLOAD:
        {
            eObject name = vm->bcode->globals->data[FETCH()];
            eObject res = edict_find(&vm->globals, OBJ_AS_CSTR(name));

            if (objisnone(res))
            {
                fmt_error(vm, "Symbol named %% does not exist.\n", name);
                return RuntimeError;
            }
            elog("\nGLoad %s\n(%p)\n", OBJ_AS_CSTR(name), res.value.object);
            PUSH(res);
        }
            break;
        case OP_STORE:
        {
            eObject name = vm->bcode->locals->data[FETCH()];
            eObject value = POP();
            //char* str = eobj_to_cstr(value);
            elog("\nStore %s val %s\n", OBJ_AS_CSTR(name), str);
            edict_insert(&vm->fp->locals, OBJ_AS_CSTR(name), value);
            //free(str);
        }
            break;
        case OP_LOAD:
        {
            eObject name = vm->bcode->locals->data[FETCH()];

            elog("\nLoad %s\n", OBJ_AS_CSTR(name));
            eObject res = edict_find(&vm->fp->locals, OBJ_AS_CSTR(name));
            PUSH(res);
        }
            break;
        case OP_FSTORE:
        {
            eObject name = vm->bcode->locals->data[FETCH()];
            eObject value = POP();
            eObject klass = POP();
            if (objisclass(klass))
            {
                eClass* the_class = OBJ_AS_CLASS(klass);
                char* str = eobj_to_cstr(value);
                elog("\nFStore %s val %s\n", OBJ_AS_CSTR(name), str);
                edict_insert(&the_class->env->fields, OBJ_AS_CSTR(name), value);
                free(str);
            }
            else
            {
                 fmt_error(vm, "Object %s is not an instance\n", eobj_to_cstr(klass));
                 return RuntimeError;
            }
        }
            break;
        case OP_FLOAD:
        {
            eObject name = vm->bcode->locals->data[FETCH()];
           // printf("\nFLoad %s\n", OBJ_AS_CSTR(name));
            eObject obj;
            eObject field;
            eClass* the_class;
            obj = POP();

            //if (OBJ_AS_CLASS(obj)->is_inst)
            {
                if (objisbuiltin(obj))
                {

                    the_class = get_builtin_instance(obj.type);
                    //-------------------------------------------
                    field = edict_find(&the_class->env->fields, OBJ_AS_CSTR(name));
                    if (eobj_isequal(NONE, field))
                    {
                        fmt_error(vm, "'%s' is not a field of '%s' class\n",
                                  OBJ_AS_CSTR(name), OBJ_AS_CSTR(the_class->name));
                        return RuntimeError;
                    }
                    PUSH(field);
                    if (objisfunction(field))
                    {
                        CPUSH(obj);
                    }
                    break;
                    //-------------------------------------------
                }
                else
                {
                    the_class = OBJ_AS_CLASS(obj);
                }

                if (eobj_isequal(NONE, obj))
                {
                    const char* str = eobj_type(obj);
                    fmt_error(vm, "'%s' has no '%s' method\n", str, OBJ_AS_CSTR(name));
                    return RuntimeError;
                }
                const char *str  = OBJ_AS_CSTR(name);
                field = find_field(vm, the_class, str);

                if (eobj_isequal(NONE, field))
                {
                    fmt_error(vm, "'%s' is not a field of '%s' class\n",
                              OBJ_AS_CSTR(name), OBJ_AS_CSTR(the_class->name));
                    return RuntimeError;
                }
                if (OBJ_AS_CLASS(obj)->is_c)
                    CPUSH(obj);

                PUSH(field);
                if (objisclass(obj) && objisfunction(field) /*&& !OBJ2FUNC(field)->is_ctor*/)
                {
                    OBJ_AS_FUNC(field)->lself = obj;
                    //*vm->cp = obj;
                    printf("ADD_SELF(%s)\n", OBJ_AS_CSTR(the_class->name));
                    //vm->cp++;
                }
            }
//            else
//            {

//            }
        }
            break;
        case OP_LLOAD:
        {
            ebyte_t i = FETCH();
            PUSH(vm->stack[i]);
        }
            break;
        case OP_LSTORE:
        {
           ebyte_t i = FETCH();
           vm->stack[i] = POP();
        }
            break;
        case OP_LSELF:
//            PUSH(*(vm->cp - 1));
//            printf("LOAD_SELF %s, %p)\n",
//                   OBJ_AS_CSTR(OBJ_AS_CLASS(*(vm->cp - 1))->name),
//                    (vm->cp - 1)->value.object);
            PUSH(vm->fp->lself);
            break;
        case OP_USTORE:
        {
//            eObject name = vm->bcode->locals.upvalues->data[FETCH()];
            eObject name = vm->bcode->upvalues->data[FETCH()];
            //elog("\nUstore %s\n", OBJ2CSTR(name));
            eObject value = POP();
            edict_insert(&vm->fp->cl_envp->upvalues, OBJ_AS_CSTR(name), value);
        }
            break;
        case OP_ULOAD:
        {
            eObject name = vm->bcode->upvalues->data[FETCH()];
            elog("\nUload %s\n", OBJ_AS_CSTR(name));

            // picks up fields in methods with missing self.
            if (vm->fp->cl_envp == NULL)
            {
                fmt_error(vm, "Symbol named '%%' does not exist.\n", name);
                return RuntimeError;
            }

            eObject obj = edict_find(&vm->fp->cl_envp->upvalues, OBJ_AS_CSTR(name));
            if (eobj_isequal(NONE, obj))
            {
                fmt_error(vm, "Symbol named '%%' does not exist.\n", name);
                return RuntimeError;
            }
            PUSH(obj);
        }
            break;
        case OP_MAKE_VECTOR:
        {
            esize_t size = FETCH();
            eVector* v = evector_new();

            for(esize_t i=size; i>0; i--)
            {
                evector_push(v, *(vm->sp - i));
            }
            /* pops values */
            vm->sp -= size;

            PUSH(OBJ_VECTOR(v));
        }
            break;
        case OP_MAKE_DICT:
        {
            esize_t size = FETCH();
            edict_t* d = edict_new(EMME_DICT_SIZE);

            for (esize_t i=size; i>0; i-=2)
            {
                char* key = OBJ_AS_CSTR(*(vm->sp - i));
                eObject value = *(vm->sp - i + 1);
                edict_insert(d, key, value);
            }
            /* pops values */
            vm->sp -= size;

            PUSH(OBJ_DICT(d));
        }
            break;
        case OP_ARRAY_LOOKUP:
            if (!objisvector(TOS1) && !objisdictionary(TOS1) && !objisstring(TOS1))
            {
                fmt_error(vm, "Object '%s' is not iterable", eobj_type(TOS1));
                return RuntimeError;
            }
            if (objisnumber(TOS))
            {
                if (objisvector(TOS1))
                {
                    int k = OBJ_AS_NUM(POP());
                    eVector* v = OBJ_AS_VEC(POP());
                    if (k < 0){ k = v->size + k; }
                    if (k >= signedi(v->size) || k < 0)
                        runtime_error(vm, "Trying to read out of vector bounds");
                    eObject e = v->data[k];
                    PUSH(e);
                }
                else if (objisstring(TOS1))
                {
                    int k = OBJ_AS_NUM(POP());
                    eString* s = OBJ_AS_STR(POP());
                    if (k < 0){ k = s->size + k; }
                    if (k >= signedi(s->size) || k < 0)
                        runtime_error(vm, "Trying to read out of string bounds");
                    char str[2] = {s->text[k], '\0'};
                    eObject e = NEW_STRING(str);
                    PUSH(e);
                }
            }
            else if (objisstring(TOS))
            {
                if (objisdictionary(TOS1))
                {
                    eString* k = OBJ_AS_STR(POP());
                    edict_t* d = OBJ_AS_DICT(POP());
                    elog("Searching dict for %s\n", k->text);

                    eObject obj = edict_find(d, k->text);
                    if (eobj_isequal(NONE, obj))
                    {
                        fmt_error(vm, "key '%s' not in 'dict'", k->text);
                        return RuntimeError;
                    }
                    PUSH(obj);
                }
            }
            break;
        case OP_ARRAY_UPDATE:
            if (!objisvector(TOS2) && !objisdictionary(TOS2) && !objisstring(TOS2))
            {
                fmt_error(vm, "Object '%s' is not iterable.", eobj_type(TOS2));
                return RuntimeError;
            }
            if (objisnumber(TOS1))
            {
                if (objisvector(TOS2))
                {
                    eObject u = POP();
                    int k = OBJ_AS_NUM(POP());
                    eVector* v = OBJ_AS_VEC(POP());
                    if (k >= signedi(v->size) || k < 0)
                        return runtime_error(vm, "Trying to read out of vector bounds");
                    v->data[k] = u;
                    PUSH(OBJ_VECTOR(v));
                }
                else if (objisstring(TOS2))
                {
                    eObject c = POP();
                    int k = OBJ_AS_NUM(POP());
                    eString* s = OBJ_AS_STR(POP());
                    if (k >= signedi(s->size) || k < 0)
                        return runtime_error(vm, "Trying to read out of string bounds");
                    s->text[k] = OBJ_AS_CSTR(c)[0];
                    PUSH(NEW_STRING(s->text));
                }
            }
            else if (objisstring(TOS1))
            {
                if (objisdictionary(TOS2))
                {
                    eObject u = POP();
                    eString* k = OBJ_AS_STR(POP());
                    edict_t* d = OBJ_AS_DICT(POP());
                    eObject res = edict_find(d, k->text);
                    if (eobj_isequal(NONE, res))
                    {
                        fmt_error(vm, "key '%s' not in 'dict'", k->text);
                        return RuntimeError;
                    }
                    edict_insert(d, k->text, u);
                    PUSH(OBJ_DICT(d));
                }
            }
            break;
        case OP_SLICEA:
        {
            if (!objisvector(TOS2) && !objisstring(TOS2))
            {
                fmt_error(vm, "Cannot use slice operator with '%s'.", eobj_type(TOS2));
                return RuntimeError;
            }
            eObject right = POP(), left = POP();
            eObject a = POP();

            int e =  !objisnil(right) ? OBJ_AS_NUM(right) :
            (objisvector(a) ? OBJ_AS_VEC(a)->size : OBJ_AS_STR(a)->size);

            int s =  !objisnil(left)  ? OBJ_AS_NUM(left) : 0;

            if (objisvector(a))
            {
                eVector* v = OBJ_AS_VEC(a);
                if (e < 0) e = v->size + e;
                if (s < 0) s = v->size + s;

                if (e >= signedi(v->size) || e < 0 || s >= signedi(v->size) || s < 0)
                    return runtime_error(vm, "Trying to read out of vector bounds");

                int size = 0;
                if (e > s)
                {
                    size = e - s;
                    while (s < e)
                    {
                        PUSH(v->data[s++]);
                    }
                }
                else if (e < s)
                {
                    size = s - e;
                    while (e < s)
                    {
                        PUSH(v->data[e++]);
                    }
                }
                make_vector_slice(vm, size);
            }
            if (objisstring(a))
            {
                eString* str = OBJ_AS_STR(a);
                if (e < 0) e = str->size + e;
                if (s < 0) s = str->size + s;

                if (e > signedi(str->size) || e < 0 || s > signedi(str->size) || s < 0)
                    return runtime_error(vm, "Trying to read out of string bounds");

                char* p = NULL;
                char* buf = NULL;
                eObject string;

                if (e > s)
                {
                    buf = emalloc(e - s + 1);

                    p = memset(buf, 0, e - s + 1);

                    for(int i=s; i<e; i++)
                    {
                        *p++ = str->text[i];
                    }
                }
                else
                {
                    buf = emalloc(2);

                    p = memset(buf, 0, 2);
                }
                *p = '\0';
                string = eobj_string(buf);
                PUSH(string);
                free(buf);
            }
        }
            break;
        case OP_SLICEA_UPDATE:
        {
            if (!objisvector(TOSN(3)) && !objisstring(TOSN(3)))
            {
                fmt_error(vm, "Cannot use slice operator with '%s'.", eobj_type(TOSN(3)));
                return RuntimeError;
            }
            eObject new_val = POP();
            eObject right = POP(), left = POP();
            eObject a = POP();

            int e =  !objisnil(right) ? OBJ_AS_NUM(right) :
            (objisvector(a) ? OBJ_AS_VEC(a)->size : OBJ_AS_STR(a)->size);

            int s =  !objisnil(left)  ? OBJ_AS_NUM(left) : 0;

            if (objisvector(a))
            {
                eVector* v = OBJ_AS_VEC(a);
                if (e < 0) e = v->size + e;
                if (s < 0) s = v->size + s;

                if (e >= signedi(v->size) || e < 0 || s >= signedi(v->size) || s < 0)
                    return runtime_error(vm, "Trying to read out of vector bounds");

                eVector* tmp_v = evector_new();

                for (int i=0; i<s; i++)
                {
                    evector_push(tmp_v, v->data[i]);
                }
                if (!objisvector(new_val))
                {
                    evector_push(tmp_v, new_val);
                }
                else
                {
                    for (esize_t i=0; i<OBJ_AS_VEC(new_val)->size; i++)
                    {
                        evector_push(tmp_v, OBJ_AS_VEC(new_val)->data[i]);
                    }
                }
                for (esize_t i=e; i<v->size; i++)
                {
                    evector_push(tmp_v, v->data[i]);
                }

                v->data = erealloc(v->data, sizeof(eObject) * (tmp_v->space));
                v->size = tmp_v->size;
                v->space = tmp_v->space;

                for (esize_t i=0; i<tmp_v->size; i++)
                {
                   v->data[i] = tmp_v->data[i];
                }
            }
            if (objisstring(a))
            {
                eString* str = OBJ_AS_STR(a);
                if (e < 0) e = str->size + e;
                if (s < 0) s = str->size + s;

                if (e > signedi(str->size) || e < 0 || s > signedi(str->size) || s < 0)
                    return runtime_error(vm, "Trying to read out of string bounds");

                eString* tmp_s = estring_new("");

                for (int i=0; i<s; i++)
                {
                    estring_append(tmp_s, str->text[i]);
                }
                for (esize_t i=0; i<OBJ_AS_STR(new_val)->size; i++)
                {
                    estring_append(tmp_s, OBJ_AS_STR(new_val)->text[i]);
                }
                for (esize_t i=e; i<str->size; i++)
                {
                    estring_append(tmp_s, str->text[i]);
                }
                for (esize_t i=0; i<tmp_s->size; i++)
                {
                    str->text[i] = tmp_s->text[i];
                }

                str->text = erealloc(str->text, sizeof(char) * (tmp_s->size + 1));
                str->text[tmp_s->size] = '\0';
                str->size = tmp_s->size;
            }
        }
            break;
        case OP_JF:
        {
            ebyte_t jmp0 = FETCH();
            ebyte_t jmp1 = FETCH();
            int r = jmp0 << 8 | jmp1;
            eObject obj = POP();
            if (!obj.value.number && !obj.value.boolean)
                vm->ip = (vm->bcode->raw + r);
        }
            break;
        case OP_JMP:
        {
            ebyte_t jmp0 = FETCH();
            ebyte_t jmp1 = FETCH();
            int r = jmp0 << 8 | jmp1;
            vm->ip = (vm->bcode->raw + r);
        }
            break;
        case OP_JMP_BACK:
        {
            ebyte_t jmp0 = FETCH();
            ebyte_t jmp1 = FETCH();
            int r = jmp0 << 8 | jmp1;
            vm->ip = (vm->bcode->raw + r);

        }
            break;
        case OP_BREAK:
        {
            LoopBlock b = block_top(vm);
            vm->ip = (vm->bcode->raw + b.end);
         }
            break;
        case OP_CONTINUE:
        {
            LoopBlock b = block_top(vm);
            vm->ip = (vm->bcode->raw + (b.start + 4));
         }
            break;
        case OP_GET_ITER:
        {
            array_iterator* it = make_iter(vm->sp - 1, has_next);
            elog("GET_ITER %p\n", it);
            PUSH(eobj_iterator(it));
        }
            break;
        case OP_FOR_ITER:
        {
            array_iterator* it = OBJ_AS_ITER(POP());
            elog("FOR_ITER %p\n", it);
            ebyte_t jmp0 = FETCH();
            ebyte_t jmp1 = FETCH();

            if (it->has_next(vm->sp - 1, it))
            {
                elog("HAS_NEXT");
                PUSH(OBJ_ITERATOR(it));
                PUSH(iter(it));
            }
            else
            {
                POP();
                int r = jmp0 << 8 | jmp1;
                vm->ip = (vm->bcode->raw + r);
            }
        }
            break;
        case OP_SETUP_LOOP:
        {
            ebyte_t s0 = FETCH();
            ebyte_t s1 = FETCH();
            int start = s0 << 8 | s1;
            ebyte_t s2 = FETCH();
            ebyte_t s3 = FETCH();
            int end = s2 << 8 | s3;
            LoopBlock b;
            b.start = start;
            b.end = end;
            block_push(vm, b);
            elog("start %d end %d \n",  start,  end);
         }
            break;
        case OP_POP_LOOP:
            block_pop(vm);
            break;
        case OP_CALL:
        {
            ebyte_t argc = FETCH();
            eObject func = tosN(vm, argc);


            if (!objisfunction(func) && !objisclass(func) && !objisclosure(func))
            {
                fmt_error(vm, "Object type '%s' <@%p> is not a callable object.\n",
                          eobj_type(func), OBJ_AS_FUNC(func));
                return RuntimeError;
            }

            int expected = 0;
            int toskip = 0;

            if (objisfunction(func))
            {
                expected = OBJ_AS_FUNC(func)->argc - OBJ_AS_FUNC(func)->dargc;
                toskip = argc - expected;
            }
            if (objisclosure(func))
            {
                expected = OBJ_AS_CLOSURE(func)->f->argc - OBJ_AS_CLOSURE(func)->f->dargc;
                toskip = argc - expected;
            }

            if (argc < expected)
            {
                fmt_error(vm, "Expected %d arguments, %d passed\n", expected, argc);
                return RuntimeError;
            }

            if (objisfunction(func) && objisnativefunc(func))
            {
                elog("%s", OBJ_AS_CSTR(OBJ_AS_FUNC(func)->name));
                vm->r1[0] = argc;
                if (!OBJ_AS_FUNC(func)->fp(vm))
                    return RuntimeError;
                else
                    break;
            }


            vm->fp++;
            vm->fp->ip = vm->ip;/* store ip */
            vm->fp->bcode = vm->bcode;/* store bytecode pointer */
            vm->fp->func = func;
            if (objismethod(func))
            {
                vm->fp->lself = OBJ_AS_FUNC(func)->lself;
                elog("IDX_SELF(%s)\n", OBJ_AS_CSTR(OBJ_AS_FUNC(func)->name));
            }

            edict_init(&vm->fp->locals, EMME_DICT_SIZE);/* init symtab */

//            if (objismethod(func))
//            {
//                const char* klassname = OBJ_AS_CSTR(OBJ_AS_CLASS(OBJ_AS_FUNC(func)->klass)->name);
//                elog("CALLER_CLASS(%s)\n", klassname);
//            }

            if (objisclosure(func))
            {
                elog("IS_CLOSURE");
                vm->fp->cl_envp = OBJ_AS_CLOSURE(func)->cl_env;
            }


            if (objisclass(func))
            {
                vm->r1[0] = argc;
                vm->bcode = OBJ_AS_CLASS(func)->code;
                vm->ip = vm->bcode->raw;
            }
            else if (objisclosure(func))
            {
                elog("LOAD SELF");
                if (OBJ_AS_CLOSURE(func)->f->outer->is_method)
                    vm->fp->lself = OBJ_AS_CLOSURE(func)->f->lself;
                vm->bcode = OBJ_AS_CLOSURE(func)->f->code;/* grab function bytecode */
                vm->ip = OBJ_AS_CLOSURE(func)->f->code->raw;/* set ip */
            }
            else
            {
                vm->bcode = OBJ_AS_FUNC(func)->code;/* grab function bytecode */
                vm->ip = OBJ_AS_FUNC(func)->code->raw;/* set ip */
                elog("skip at %d - toskip %d\n", OBJ_AS_FUNC(func)->skip_addr[toskip-1], toskip);
            }

            int vargc = 0;

            if (objisfunction(func) && fnhasvararg(func))
            {
                vargc = argc - signedi(OBJ_AS_FUNC(func)->argc);
                vargc = vargc > 0 ? vargc : 0;
                elog("vargc %d\n",vargc);
                if (vargc > 0)
                {
                    eVector* pack = evector_new();
                    for (int i=vargc-1; i>=0; i--)
                    {
                        evector_push(pack, TOSN(i));
                    }
                    vm->sp -= vargc;
                    char* vargname = OBJ_AS_CSTR(OBJ_AS_FUNC(func)->vargname);
                    edict_insert(&vm->fp->locals, vargname, OBJ_VECTOR(pack));
                }

            }
            if (objisclosure(func) && clhasvararg(func))
            {

                vargc = argc - signedi(OBJ_AS_CLOSURE(func)->f->argc);
                vargc = vargc > 0 ? vargc : 0;
                elog("vargc %d\n",vargc);
                if (vargc > 0)
                {
                    eVector* pack = evector_new();
                    for (int i=vargc-1; i>=0; i--)
                    {
                        evector_push(pack, TOSN(i));
                    }
                    vm->sp -= vargc;
                    char* vargname = OBJ_AS_CSTR(OBJ_AS_CLOSURE(func)->f->vargname);
                    edict_insert(&vm->fp->locals, vargname, OBJ_VECTOR(pack));
                }
            }

            for (esize_t i=0; i<vm->bcode->locals->size; i++)
            {
                char* str = eobj_to_cstr(vm->bcode->locals->data[i]);
                printf("const %lu %s \n", i, str);
                free(str);
            }

            int final_argc = 0;

            if (objisfunction(func))
            {
                final_argc = fnhasvararg(func) && OBJ_AS_FUNC(func)->is_child ?
                            argc - vargc - 1:
                            fnhasvararg(func) ? argc - vargc - 1 :
                            OBJ_AS_FUNC(func)->is_child ? argc - 1 : argc - 1;

                int end =  OBJ_AS_FUNC(func)->is_child ? 0    /* skip super */: 0;
                for (int i=final_argc; i>=end; i--)
                {
                    eObject name;
                    int idx = i;
                    /* If the previous parameter has a default value */
                    /* here we will find that value. So, to get the next */
                    /* parameter's name we increment the index */

                    if (!objisstring(vm->bcode->locals->data[idx]))idx++;
                    //printf("param in %s\n",OBJ_AS_CSTR(vm->bcode->locals->data[idx]));

                    name = vm->bcode->locals->data[idx];

                    eObject arg_val = POP();
                    edict_insert(&vm->fp->locals, OBJ_AS_CSTR(name), arg_val);
                }
            }
            else if (objisclosure(func))
            {
                final_argc =  clhasvararg(func) ? argc - vargc - 1 : argc - 1;
printf("//////////////////////////////////////////////////////////////////////////////");
                for (int i=final_argc; i>=0; i--)
                {
                    eObject name;
                    int idx = i;
                    /* If the previous parameter has a default value */
                    /* here we will find that value. So, to get the next */
                    /* parameter's name we increment the index */
//                    if (!objisstring(vm->bcode->locals.constants[idx]))idx++;
                    if (!objisstring(vm->bcode->locals->data[idx]))idx++;
                    elog("param in %s\n",OBJ_AS_CSTR(vm->bcode->locals->data[idx]));
                    //                    name = vm->bcode->locals.constants[idx];
                    name = vm->bcode->locals->data[idx];
                    eObject arg_val = POP();
                    edict_insert(&vm->fp->locals, OBJ_AS_CSTR(name), arg_val);
                }
            }

//            if (objisclosure(func))
//            {
//                *vm->cp = OBJ_AS_CLOSURE(func)->lself;
//                vm->cp++;
//            }

            int toskip_idx = (toskip - 1) <= 0 ? 0 : (toskip - 1);
            toskip -= vargc;

            if (objisfunction(func))
            {
                elog("fn goto address %d toskip(%d)\n", (OBJ_AS_FUNC(func)->skip_addr[toskip - 1]), toskip - 1);
                if ((toskip - 1) >= 0)
                    vm->ip += (OBJ_AS_FUNC(func)->skip_addr[toskip - 1]);
                dis_bcode(OBJ_AS_FUNC(func)->code, OBJ_AS_CSTR(OBJ_AS_FUNC(func)->name));
                POP();
            }
            else if (objisclosure(func))
            {
                elog("cl goto address %d toskip(%d)\n", (OBJ_AS_CLOSURE(func)->f->skip_addr[toskip - 1]), toskip - 1);
                if ((toskip - 1) >= 0)
                    vm->ip += (OBJ_AS_CLOSURE(func)->f->skip_addr[toskip - 1]);
                dis_bcode(OBJ_AS_CLOSURE(func)->f->code, OBJ_AS_CSTR(OBJ_AS_CLOSURE(func)->f->name));
                POP();
            }
            else
            {
                if (!OBJ_AS_CLASS(func)->has_parent)
                    POP();
                dis_bcode(OBJ_AS_CLASS(func)->code, OBJ_AS_CSTR(OBJ_AS_CLASS(func)->name));
            }
        }
            break;
        case OP_CALLINIT:
        {
            ebyte_t argc = FETCH();
            eObject func = tosN(vm, argc);/* function */

            int expected = 0;
            int toskip = 0;

            expected = OBJ_AS_FUNC(func)->argc - OBJ_AS_FUNC(func)->dargc;
            toskip = argc - expected;

            if (argc < expected)
            {
                fmt_error(vm, "Expected %d arguments, %d passed\n", expected, argc);
                return RuntimeError;
            }

            vm->fp++;
            vm->fp->ip = vm->ip;/* store ip */
            vm->fp->bcode = vm->bcode;/* store bytecode pointer */
            vm->fp->func = func;
            vm->fp->lself = OBJ_AS_FUNC(func)->lself;

            OBJ_AS_FUNC(vm->fp->func)->klass = OBJ_AS_FUNC(func)->klass;

            edict_init(&vm->fp->locals, EMME_DICT_SIZE);/* init symtab */


//            elog("CALLER(%s)\n", eobj_to_cstr(OBJ2FUNC(func)->klass));
//            eObject obj = OBJ2FUNC(func)->klass;
//            const char* classname = OBJ_AS_CSTR(OBJ2CLASS(obj)->name);
//            vm->self = edict_find(&vm->self_table, (char*)classname);
//            *vm->cp++ = vm->self;

            vm->bcode = OBJ_AS_FUNC(func)->code;/* grab function bytecode */
            vm->ip = OBJ_AS_FUNC(func)->code->raw;/* set ip */

            int vargc = 0;

            if (objisfunction(func) && fnhasvararg(func))
            {
                vargc = argc - signedi(OBJ_AS_FUNC(func)->argc);
                vargc = vargc > 0 ? vargc : 0;
                elog("vargc %d\n",vargc);
                if (vargc > 0)
                {
                    eVector* pack = evector_new();
                    for (int i=vargc-1; i>=0; i--)
                    {
                        evector_push(pack, TOSN(i));
                    }
                    vm->sp -= vargc;
                    char* vargname = OBJ_AS_CSTR(OBJ_AS_FUNC(func)->vargname);
                    edict_insert(&vm->fp->locals, vargname, OBJ_VECTOR(pack));
                }
                else
                {
                    char* vargname = OBJ_AS_CSTR(OBJ_AS_FUNC(func)->vargname);
                    edict_insert(&vm->fp->locals, vargname, NIL);
                }
            }

            for (esize_t i=0; i<vm->bcode->locals->size; i++)
            {
                char* str = eobj_to_cstr(vm->bcode->locals->data[i]);
                elog("const %lu %s \n", i, str);
                free(str);
            }

            int final_argc = OBJ_AS_CLASS(OBJ_AS_FUNC(func)->klass)->has_parent && fnhasvararg(func) ? argc - vargc - 1:
            fnhasvararg(func) ? argc - vargc - 1 :
            OBJ_AS_CLASS(OBJ_AS_FUNC(func)->klass)->has_parent ? argc - 1/* skip super */: argc - 1;
            int end = OBJ_AS_CLASS(OBJ_AS_FUNC(func)->klass)->has_parent ? 0   /* skip super */: 0;
            for (int i=final_argc; i>=end; i--)
            {
                eObject name;
                int idx = i;
                /* If the previous parameter has a default value */
                /* here we'll find that value. So, to get the next */
                /* parameter's name we increment the index */
//                if (!objisstring(vm->bcode->locals.constants[idx]))idx++;
                if (!objisstring(vm->bcode->locals->data[idx]))idx++;
                elog("param in %s\n",OBJ_AS_CSTR(vm->bcode->locals->data[idx]));
//                name = vm->bcode->locals.constants[idx];
                name = vm->bcode->locals->data[idx];
//                name = *vm->bcode->argnames->data++;
                eObject p = POP();
                edict_insert(&vm->fp->locals, OBJ_AS_CSTR(name), p);
            }
            int toskip_idx = (toskip - 1) < 0 ? 0 : (toskip - 1);
            toskip -= vargc;

//            for (esize_t i=0; i<FUNC_ARITY; i++)
//                printf("%u - %lu\n",(OBJ_AS_FUNC(func)->skip_addr[i]),i);

            elog("fn goto address %lu toskip(%d)\n", (OBJ_AS_FUNC(func)->skip_addr[toskip - 1]), toskip - 1);
            if ((toskip - 1) >= 0)
                vm->ip += /*vm->bcode->raw +*/ (OBJ_AS_FUNC(func)->skip_addr[toskip - 1]);
            dis_bcode(OBJ_AS_FUNC(func)->code, OBJ_AS_CSTR(OBJ_AS_FUNC(func)->name));
            POP();
        }
            break;
        case OP_RET:

            vm->ip = vm->fp->ip;
            vm->bcode = vm->fp->bcode;
            edict_erase(&vm->fp->locals);

//            if (objismethod(vm->fp->func) || (objisclosure(vm->fp->func) && OBJ_AS_CLOSURE(vm->fp->func)->f->outer->is_method))
//            {
//                eFunction* f = OBJ_AS_FUNC(vm->fp->func);
//                elog("func name %s\n", OBJ_AS_CSTR(OBJ2FUNC(vm->fp->func)->name));
//                eClass* c = OBJ_AS_CLASS(f->klass);
////                const char* klassname = OBJ_AS_CSTR(c->name);
////                elog("POP_SELF(%s)\n", klassname);
//                //vm->cp--;
//            }
            --vm->fp;
            break;
        case OP_MAKE_CLOSURE:
        {
            eObject func = POP();

            ClosureEnv* cl_env = emalloc(sizeof(ClosureEnv));

            edict_init(&cl_env->upvalues, vm->fp->locals.space);

            if (OBJ_AS_FUNC(func)->outer->is_closure)
            {
                elog("size %lu\n", OBJ_AS_FUNC(func)->outer->cl_env->upvalues.size);
                if (OBJ_AS_FUNC(func)->outer->cl_env->upvalues.size)
                {
                    elog("copy outer upvals");
                    edict_copy(&cl_env->upvalues, &OBJ_AS_FUNC(func)->outer->cl_env->upvalues);
                }
            }
            elog("copy upvals");
            edict_copy(&cl_env->upvalues, &vm->fp->locals);
            OBJ_AS_FUNC(func)->cl_env = cl_env;

            eObject closure = eobj_closure_func(func, cl_env);
            if (OBJ_AS_CLOSURE(closure)->f->outer->is_method)
                OBJ_AS_CLOSURE(closure)->f->lself = vm->fp->lself;//*(vm->cp - 1);//pass in 'self' if is closure
            elog("size %lu\n", cl_env->upvalues.size);
            elog("size %lu\n", vm->fp->locals.size);

            PUSH(closure);
        }
            break;
        case OP_MAKE_INSTANCE:
        {
            elog("---------------------------------------------------------\n");
            eObject instance = eobj_clone(POP());

            eClass* c = OBJ_AS_CLASS(instance);
            c->env = emalloc(sizeof(ClassEnv));
            edict_init(&c->env->fields, vm->fp->locals.space);

            c->is_inst = true;
            elog("################COPY INSTANCE####################");
            /* copy local fields */
            edict_copy_all(&c->env->fields, &vm->fp->locals);
            elog("#################################################");

            if (c->has_parent)
            {
                POP();
                eClass* the_parent = c->parent;
                the_parent->child = c;
            }

            PUSH(instance);
        }
            break;
        case OP_INERIT:
            break;
        case OP_MAKE_SUBCLASS:
        {
            elog("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

            eObject parent = POP();

            eObject klass = POP();
            OBJ_AS_CLASS(parent)->is_parent = true;
            OBJ_AS_CLASS(klass)->has_parent = true;
            OBJ_AS_CLASS(klass)->parent = OBJ_AS_CLASS(parent);

            elog("parent");
//            print_object(parent);
            //PUSH(klass);//??
            PUSH(parent);
        }
            break;
        case OP_PRINT:
        {
            const char* fmt;
            eObject obj = POP();
            if (objisstring(obj))
                fmt = QUOTE_STR_FMT;
            else
                fmt = STR_FMT;

            char* ptr = eobj_to_cstr(obj);
            printf(fmt, ptr);
            free(ptr);
        }
            break;
        case OP_FIELD_GET:
            break;
        case OP_IMPORT_LIB:
        {
//            eObject name = vm->bcode->globals.constants[FETCH()];
            eObject name = vm->bcode->globals->data[FETCH()];

            if (!load_dynamic_ctor(vm, OBJ_AS_CSTR(name)))
            { return RuntimeError; }
        }
            break;
        case OP_DUP_TOS:
            PUSH(TOS);
            break;
        case OP_SWAP:
        {
            eObject tmp = TOS;
            *(vm->sp - 1) = TOS1;
            *(vm->sp - 2) = tmp;
        }
            break;
        default:
            elog("opcode error\n");
            break;
        }
    }while (vm->run);

    return NoError;
}
