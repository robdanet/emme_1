#include "elexer.h"


void lexer_init(eLexer* lexer, const char* source, const char* filename)
{
    lexer->filename = filename;
    lexer->textp = source;
    lexer->ln_start = source;
    lexer->tk_start = source;
    lexer->line = 1;
}

eToken token_error(eLexer* lexer, const char* msg)
{
    eToken token;
    token.start = strdup(msg);
    token.length = strlen(msg);
    token.line = lexer->line;
    token.type = ETOKEN_ERROR;

    return token;
}

void print_line_with_error(eLexer* lexer, const char* msg)
{
    int i=0;
    char code_line[SMALL_BUFFER];
    int currtk_len = (int)(lexer->textp - lexer->tk_start);
    int column = (int)(lexer->textp - lexer->ln_start - currtk_len);
    /* if lexer->ln_start[i] is NULL we are at EOF */
    if (lexer->ln_start[i])
    {
        for(i=0; (lexer->ln_start[i] != '\n'/* || lexer->ln_start[i] != '\0'*/) /*&& i<255*/; i++)
        {
            code_line[i] = lexer->ln_start[i];
        }
        code_line[i] = '\0';

        fprintf(stderr, "\n[%s][%lu:%d]: %s\n\n%s\n",
                lexer->filename,
                lexer->line,
                column + 1,
                msg,
                code_line);

        for (i=0; i<column && lexer->ln_start[i] != '\n' ;i++)
            fprintf(stderr, "%s", " ");
        fprintf(stderr,"%s","^\n");
    }
    else
    {
        fprintf(stderr, "\n[%s][%lu:%d]: %s\n",
                lexer->filename,
                lexer->line,
                column + 1,
                msg);
    }
}

bool is_eos(eLexer* lexer){ return *lexer->textp == '\0'; }

static bool eisalpha(char c)
{
    return (c >= 'a' && c <= 'z') ||
           (c >= 'A' && c <= 'Z') ||
           (c == '_');
}
static bool eisdigit(char c)
{
    return ((c >= '0') && (c <= '9'));
}

static bool eispunct(char c)
{
    return (c == '+') || (c == '-') || (c == '*') || (c == '/') ||
           (c == '.') || (c == '!') || (c == '=') || (c == '{') ||
           (c == '}') || (c == '(') || (c == ')') || (c == ',') ||
           (c == '>') || (c == '<') || (c == '!') || (c == '[') ||
           (c == ']') || (c == ';') || (c == ':') || (c == '%') ||
           (c == '&') || (c == '|') || (c == '^');
}

static bool eisstring(char c)
{
    return (c == '\"') || (c == '\'');
}


static void consume(eLexer* lexer){ lexer->textp++; }
static char peek(eLexer* lexer){ return *lexer->textp; }
static void reset(eLexer* lexer){ lexer->ln_start = lexer->textp + 1; lexer->line++; }

static eToken make_token(eLexer* lexer, TokenType type)
{
    eToken t;
    t.type = type;
    t.length = (esize_t)(lexer->textp - lexer->tk_start);
    t.start = lexer->tk_start;
    t.line = lexer->line;

    return t;
}

static void spaces(eLexer* lexer)
{
    lexer->tk_start = lexer->textp;
    while (1)
    {
        switch (peek(lexer)) {
        case '\n':
            consume(lexer);
            reset(lexer);
            break;
        case ' ':
        case '\t':
        case '\r':
            consume(lexer);
            break;
        default:
           return;
        }
    }
}

static void do_escape(eLexer* lexer)
{
    switch (peek(lexer)) {
    case '\\':
        consume(lexer);
        break;
    }
}

static eToken estring(eLexer* lexer)
{
    char type = peek(lexer);
    consume(lexer);
    lexer->tk_start = lexer->textp;

    switch(type)
    {
    case '\'':
        while (peek(lexer) != '\'')
        {
            if (is_eos(lexer))
            { goto error; }
            do_escape(lexer);
            consume(lexer);
        }
        consume(lexer);
        return make_token(lexer, ETOKEN_STRING);
    case '\"':
        while (peek(lexer) != '\"')
        {
            if (is_eos(lexer))
            { goto error; }
            do_escape(lexer);
            consume(lexer);
        }
        consume(lexer);
        return make_token(lexer, ETOKEN_STRING);
    default:
        break;
    }
error:
    return token_error(lexer, "EOS");
}

static char* copy_text(eLexer* lexer)
{
    esize_t len = (esize_t)(lexer->textp - lexer->tk_start);
    char* p = emalloc(sizeof(char) * (len + 1));
    memcpy(p, lexer->tk_start, len);
    p[len]='\0';
    return p;
}

static bool is_kwrd(const char* str, const char* kw, esize_t size)
{
    if (memcmp(str, kw, size) == 0 && strlen(str) == size) return true;
    return false;
}

static eToken symbol(eLexer* lexer)
{
    const char* kws[] =
    {
        "nd"/*and*/,
        "reak",/*break*/
        "lass",/*class*/
        "ontinue",/*continue*/
        "lse",/*else*/
        "lif",/*elif*/

        "alse",/*false*/
        "or"/*for*/,
        "unc"/*func*/,

        "f"/*if*/,
        "mport",/*import*/
//        "nit",/*init*/
        "n",/*in*/

        "ambda",/*lambda*/
        "et",/*let*/

        "ew",/*new*/
        "il"/*nil*/,
        "ot"/*not*/,

        "r"/*or*/,
        "rint",/*print*/
        "eturn",/*return*/
        "elf",/*self*/
        "rue"/*true*/,
        "se",/*use*/
        "ar"/*var*/,
        "hile"/*while*/
    };

    while (eisalpha(peek(lexer)) || eisdigit(peek(lexer)))
    {
        consume(lexer);
    }
    char* str = copy_text(lexer);

    if (lexer->tk_start[0] == 'a')
    {
        if (is_kwrd(str+1, kws[0], 2))
        {
            free(str); return make_token(lexer, ETOKEN_AND);
        }
    }
    if (lexer->tk_start[0] == 'b')
    {
        if (is_kwrd(str+1, kws[1], 4))
        {
            free(str); return make_token(lexer, ETOKEN_BREAK);
        }
    }
    if (lexer->tk_start[0] == 'c')
    {
        if (is_kwrd(str+1, kws[2], 4))
        {
            free(str); return make_token(lexer, ETOKEN_CLASS);
        }
        else if (is_kwrd(str+1, kws[3], 7))
        {
            free(str); return make_token(lexer, ETOKEN_SKIP);
        }
    }
    if (lexer->tk_start[0] == 'e')
    {
        if(is_kwrd(str+1, kws[4], 3))
        {
            free(str); return make_token(lexer, ETOKEN_ELSE);
        }
        else if(is_kwrd(str+1, kws[5], 3))
        {
            free(str); return make_token(lexer, ETOKEN_ELIF);
        }
    }
    if (lexer->tk_start[0] == 'f')
    {
        if(is_kwrd(str+1, kws[6], 4))
        {
            free(str); return make_token(lexer, ETOKEN_FALSE);
        }
        else if(is_kwrd(str+1, kws[7], 2))
        {
            free(str); return make_token(lexer, ETOKEN_FOR);
        }
        else if(is_kwrd(str+1, kws[8], 3))
        {
           free(str); return make_token(lexer, ETOKEN_FUNC);
        }
    }
    if (lexer->tk_start[0] == 'i')
    {
        if(is_kwrd(str+1, kws[9], 1))
        {
            free(str); return make_token(lexer, ETOKEN_IF);
        }
        else if(is_kwrd(str+1, kws[10], 5))
        {
            free(str); return make_token(lexer, ETOKEN_IMPORT);
        }
//        else if(is_kwrd(str+1, kws[9], 3))
//        {
//            free(str); return Token(lexer, ETOKEN_INIT);
//        }
        else if(is_kwrd(str+1, kws[11], 1))
        {
            free(str); return make_token(lexer, ETOKEN_IN);
        }
    }
    if (lexer->tk_start[0] == 'l')
    {
        if(is_kwrd(str+1, kws[12], 5))
        {
            free(str); return make_token(lexer, ETOKEN_LAMBDA);
        }
        else if (is_kwrd(str+1, kws[13], 2))
        {
            free(str); return make_token(lexer, ETOKEN_LET);
        }
    }
    if (lexer->tk_start[0] == 'n')
    {
        if(is_kwrd(str+1, kws[14], 2))
        {
            free(str); return make_token(lexer, ETOKEN_NEW);
        }
        else if(is_kwrd(str+1, kws[15], 2))
        {
            free(str); return make_token(lexer, ETOKEN_NIL);
        }
        else if(is_kwrd(str+1, kws[16], 2))
        {
            free(str); return make_token(lexer, ETOKEN_NOT);
        }
    }
    if (lexer->tk_start[0] == 'o')
    {
        if(is_kwrd(str+1, kws[17], 1))
        {
            free(str); return make_token(lexer, ETOKEN_OR);
        }
    }
    if (lexer->tk_start[0] == 'p')
    {
        if(is_kwrd(str+1, kws[18], 4))
        {
            free(str); return make_token(lexer, ETOKEN_PRINT);
        }
    }
    if (lexer->tk_start[0] == 'r')
    {
        if(is_kwrd(str+1, kws[19], 5))
        {
            free(str); return make_token(lexer, ETOKEN_RET);
        }
    }
    if (lexer->tk_start[0] == 's')
    {
        if(is_kwrd(str+1, kws[20], 3))
        {
            free(str); return make_token(lexer, ETOKEN_SELF);
        }
    }
    if (lexer->tk_start[0] == 't')
    {
        if(is_kwrd(str+1, kws[21], 3))
        {
            free(str); return make_token(lexer, ETOKEN_TRUE);
        }
    }
    if (lexer->tk_start[0] == 'u')
    {
        if(is_kwrd(str+1, kws[22], 2))
        {
            free(str); return make_token(lexer, ETOKEN_USE);
        }
    }
    if (lexer->tk_start[0] == 'v')
    {
        if(is_kwrd(str+1, kws[23], 2))
        {
            free(str); return make_token(lexer, ETOKEN_VAR);
        }
    }
    if (lexer->tk_start[0] == 'w')
    {
        if(is_kwrd(str+1, kws[24], 4))
        {
            free(str); return make_token(lexer, ETOKEN_WHILE);
        }
    }
    free(str);
    return make_token(lexer, ETOKEN_SYM);
}

static eToken scient_num(eLexer* lexer);
static eToken scient_num_exp(eLexer* lexer);

static eToken float_num(eLexer* lexer)
{
    consume(lexer);
    while (eisdigit(peek(lexer)) ||  (peek(lexer) == 'e') || (peek(lexer) == 'E'))
    {
        switch(peek(lexer)){
        case 'e':
        case 'E':
            return scient_num(lexer);
            break;
        }
        consume(lexer);
    }
    return make_token(lexer, ETOKEN_NUMBER);
}

static eToken scient_num(eLexer* lexer)
{
    consume(lexer);
    while (eisdigit(peek(lexer)) || (peek(lexer) == '-') || (peek(lexer) == '+'))
    {
        switch(peek(lexer)){
        case '+':
        case '-':
            return scient_num_exp(lexer);
            break;
        }
        consume(lexer);
    }
    return make_token(lexer, ETOKEN_NUMBER);
}

static eToken scient_num_exp(eLexer* lexer)
{
    consume(lexer);
    while (eisdigit(peek(lexer)))
    {
        consume(lexer);
    }
    return make_token(lexer, ETOKEN_NUMBER);
}

static eToken hex_num(eLexer* lexer)
{
    consume(lexer);
    while (eisdigit(peek(lexer)) ||
          (peek(lexer) >= 'a' && peek(lexer) <= 'z') ||
          (peek(lexer) >= 'A' && peek(lexer) <= 'Z'))
    {
        consume(lexer);
    }
    return make_token(lexer, ETOKEN_XNUMBER);
}

static eToken number(eLexer* lexer)
{
    while (1)
    {
        if (eisdigit(peek(lexer)) || (peek(lexer) == '.') ||
                (peek(lexer) == 'E') || (peek(lexer) == 'e') ||
                (peek(lexer) == 'X') || (peek(lexer) == 'x'))
        {
            switch(peek(lexer)){
            case '.':
                return float_num(lexer);
                break;
            case 'e':
            case 'E':
                return scient_num(lexer);
                break;
            case 'x':
            case 'X':
                return hex_num(lexer);
                break;
            }
            consume(lexer);
        }
        else if (eisalpha(peek((lexer))))
        { return token_error(lexer, "Invalid symbol"); }
        else
        { break; }
    }

    return make_token(lexer, ETOKEN_NUMBER);
}

#define caseof2(a, ta, tb)\
    switch(peek(lexer)){\
    case a:consume(lexer);return make_token(lexer, ta);\
    default :return make_token(lexer, tb);\
        break;\
    }

#define caseof3(a, b, ta, tb, tc)\
    switch(peek(lexer)){\
    case a:consume(lexer);return make_token(lexer, ta);\
    case b:consume(lexer);return make_token(lexer, tb);\
    default :return make_token(lexer, tc);\
        break;\
    }

#define caseof4(a, b, c, ta, tb, tc, td)\
    switch(peek(lexer)){\
    case a:consume(lexer);return make_token(lexer, ta);\
    case b:consume(lexer);return make_token(lexer, tb);\
    case c:consume(lexer);return make_token(lexer, tc);\
    default :return make_token(lexer, td);\
        break;\
    }


#define line_comment \
if (c == '/' && peek(lexer) == '/')\
    {\
        while (peek(lexer) != '\n')\
                 consume(lexer);\
            return token_get(lexer);\
    }

#define begin_multiline (c == '/' && peek(lexer) == '*')
#define end_multiline (c == '*' && peek(lexer) == '/')

eToken multiline_comment(eLexer* lexer, char c)
{
    consume(lexer);
    int depth = 0;
    while ((!end_multiline && !is_eos(lexer)) || depth != -1)
    {
        c = peek(lexer);
        if (c == '\n')reset(lexer);//lexer->line++;
        consume(lexer);
        if (begin_multiline)
        {
            depth++;
        }
        if (end_multiline)
        {
            consume(lexer);
            if (peek(lexer) == '*')//deals with two attached */*/
            {
                //lexer->ln_start = lexer->textp;
                return make_token(lexer, ETOKEN_COMMERR);
//                return token_error(lexer, "Error");
//                fprintf(stderr, "[%s][%lu]Expected expression or space before '*'\n",
//                        lexer->filename, lexer->line);
//                exit(EXIT_FAILURE);
            }
            else
            { lexer->textp--; }

            depth--;
        }
        if (is_eos(lexer))
        {
            fprintf(stderr,  "[%s][%lu]'EOS' token inside multiline comment.\n",
                  lexer->filename, lexer->line);
            exit(EXIT_FAILURE);
        }
    }
    consume(lexer);//consume last '/'
    return token_get(lexer);
}


static eToken punctuation(eLexer* lexer)
{
    char c = peek(lexer);
    consume(lexer);
    line_comment;

    if (begin_multiline)
    {
        return multiline_comment(lexer, c);
    }

    switch (c) {
    case '+':
        caseof3('+', '=', ETOKEN_PLUS_PLUS, ETOKEN_PLUS_EQUAL, ETOKEN_PLUS);
        break;
    case '-':
        caseof4('>', '-', '=', ETOKEN_POINTER, ETOKEN_MINUS_MINUS, ETOKEN_MINUS_EQUAL, ETOKEN_MINUS);
        break;
    case '>':
        caseof3('=', '>', ETOKEN_GE, ETOKEN_R_SHIFT, ETOKEN_GT);
        break;
    case '<':
        caseof3('=', '<', ETOKEN_LE, ETOKEN_L_SHIFT, ETOKEN_LT);
        break;
    case '*':
        caseof2('=', ETOKEN_MULT_EQUAL, ETOKEN_MULT);
        break;
    case '/':
        caseof2('=', ETOKEN_DIV_EQUAL, ETOKEN_DIV);
        break;
    case '=':
        caseof2('=', ETOKEN_EQ, ETOKEN_EQUAL);
        break;
    case '!':
        caseof2('=', ETOKEN_NE, ETOKEN_NOT);
        break;
    case '{':
        return make_token(lexer, ETOKEN_L_CURLY);
        break;
    case '}':
        return make_token(lexer, ETOKEN_R_CURLY);
        break;
    case '(':
        return make_token(lexer, ETOKEN_L_PAREN);
        break;
    case ')':
        return make_token(lexer, ETOKEN_R_PAREN);
        break;
    case '[':
        return make_token(lexer, ETOKEN_L_BRACK);
        break;
    case ']':
        return make_token(lexer, ETOKEN_R_BRACK);
        break;
    case ',':
        return make_token(lexer, ETOKEN_COMMA);
        break;
    case ';':
        return make_token(lexer, ETOKEN_S_COLON);
        break;
    case ':':
        return make_token(lexer, ETOKEN_COLON);
        break;
    case '%':
        return make_token(lexer, ETOKEN_MODULO);
        break;
    case '&':
        return make_token(lexer, ETOKEN_B_AND);
        break;
    case '|':
        return make_token(lexer, ETOKEN_B_OR);
        break;
    case '^':
        return make_token(lexer, ETOKEN_B_XOR);
        break;
    case '.':
    {
            if (eisdigit(peek(lexer)))return number(lexer);
            if (peek(lexer) == '.')
            {
                consume(lexer);
                if (peek(lexer) == '.')
                {consume(lexer); return make_token(lexer, ETOKEN_ELLIPSIS);}
                else
                { return make_token(lexer, ETOKEN_DOT_DOT); }
            }
            else
            {
                return make_token(lexer, ETOKEN_DOT);
            }
    }
        break;
    default:
        break;
    }
    return token_error(lexer, "Unrecognized token.");
}

eToken token_get(eLexer* lexer)
{
    spaces(lexer);
    lexer->tk_start = lexer->textp;

    if (is_eos(lexer)) return make_token(lexer, ETOKEN_EOS);

    if (eisdigit(peek(lexer)))
        return number(lexer);
    if (eisalpha(peek(lexer)))
        return symbol(lexer);
    if (eisstring(peek(lexer)))
        return estring(lexer);
    if (eispunct(peek(lexer)))
        return punctuation(lexer);

    return token_error(lexer, "Unrecognized token.");

}

