#ifndef ELIBS
#define ELIBS


#include "emme.h"


/* stream */
int wr_iostream(eVm* vm);
int wr_stream_open(eVm* vm);
int wr_stream_close(eVm* vm);
int wr_stream_setfile(eVm* vm);
int wr_stream_setmode(eVm* vm);
int wr_stream_read(eVm* vm);
int wr_stream_write(eVm* vm);

extern void sset_mode(eStream* s, const char* mode);
extern void sset_filename(eStream* s, const char* filename);
extern int sopen(eStream* s);
extern char* sread(eStream* s);
extern void sclose(eStream* s);


/* vector */
int wr_push(eVm* vm);
int wr_pop(eVm* vm);
int wr_size(eVm* vm);
int wr_splice(eVm* vm);

/* dict */
int wr_dict_size(eVm* vm);
int wr_insert(eVm* vm);
int wr_sortk(eVm* vm);
int wr_sortv(eVm* vm);
int wr_first(eVm* vm);
int wr_second(eVm* vm);

/* string */
int wr_string_size(eVm* vm);
int wr_append(eVm* vm);
int wr_format(eVm* vm);
int wr_indexof(eVm* vm);
int wr_isspace(eVm* vm);
int wr_isdigit(eVm* vm);
int wr_toupper(eVm* vm);
int wr_tolower(eVm* vm);
int wr_toascii(eVm* vm);

/* number */
int wr_num_ctor(eVm* vm);
int wr_add(eVm* vm);
int wr_sub(eVm* vm);
int wr_mul(eVm* vm);

/* time */
int wr_time_ctor(eVm* vm);
int wr_elapsed(eVm* vm);
int wr_nanosec(eVm* vm);
int wr_seconds(eVm* vm);
int wr_minutes(eVm* vm);
int wr_hour(eVm* vm);
int wr_strftime(eVm* vm);

/* math */
int wr_math_ctor(eVm* vm);
int wr_sin(eVm* vm);
int wr_sqrt(eVm* vm);
int wr_pow(eVm* vm);


//extern void make_bltins_types();

//extern void fmt_error(eVm* vm, char* fmt, ...);
int wr_printf(eVm* vm);
int wr_eval(eVm* vm);
int wr_exit(eVm* vm);
int wr_len(eVm* vm);
int wr_make_iter(eVm* vm);
int wr_iter(eVm* vm);
int wr_next(eVm* vm);
int wr_has_next(eVm* vm);
int wr_typename(eVm* vm);
int wr_instanceof(eVm* vm);
int wr_range(eVm* vm);
int wr_input(eVm* vm);
int wr_dis(eVm* vm);
int wr_tochar(eVm* vm);
int wr_toint(eVm* vm);
int wr_test_print(eVm* vm);
int wr_tofloat(eVm* vm);


void* open_shared_lib(char* libname);
void* load_shared_sym(const char* symname, void* so_library);
extern int load_dynamic_ctor(eVm* vm, char* name);

eClass *make_type(char *name, struct Library *library, struct LibMethod *methods);

#endif // ELIBS
