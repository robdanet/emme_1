﻿#ifndef EOBJECT_H
#define EOBJECT_H

#include "etypes.h"

struct eFunction;
struct eClass;



#define NIL                     ((eObject){ENIL, .value.number=0 })
#define NONE                    ((eObject){ENONE, .value.number=-1 })
#define BOOL(b)                 ((eObject){EBOOL, .value.boolean=b })
#define NUMBER(n)               ((eObject){ENUMBER, .value.number=n })

#define NEW_STRING(text)        ((eObject){ESTRING, .value.object=estring_new(text) })
#define OBJ_VECTOR(v)           ((eObject){EVECTOR, .value.object=v })
#define OBJ_DICT(d)             ((eObject){EDICT, .value.object=d })
#define OBJ_ITERATOR(it)        ((eObject){EITER, .value.object=it })
#define OBJ_STRING(estring)     ((eObject){ESTRING, .value.object=estring })
#define OBJ_CLASS(klass)        ((eObject){ECLASS, .value.object=klass })
#define OBJ_FUNCTION(f)         ((eObject){EFUNCTION, .value.object=f })


#define OBJ_AS_NIL(obj)         ((obj).value.number)
#define OBJ_AS_BOOL(obj)        ((obj).value.boolean)
#define OBJ_AS_NUM(obj)         ((obj).value.number)
#define OBJ_AS_FLOAT(obj)       ((obj).value.number)
#define OBJ_AS_INT(obj)         ((long)(obj).value.number)
#define OBJ_AS_STR(obj)         ((eString*)(obj).value.object)
#define OBJ_AS_CSTR(obj)        (OBJ_AS_STR(obj)->text)
#define OBJ_AS_VEC(obj)         ((eVector*)(obj).value.object)
#define OBJ_AS_DICT(obj)        ((edict_t*)(obj).value.object)
#define OBJ_AS_ITER(obj)        ((array_iterator*)(obj).value.object)


#define OBJ_AS_TIME(obj)  ((eTime*)(obj).value.object)
#define OBJ_AS_FUNC(obj) ((struct eFunction*)(obj).value.object)
#define OBJ_AS_CLASS(obj) ((struct eClass*)(obj).value.object)
#define OBJ_AS_CLOSURE(obj) ((struct ClosureFunc*)(obj).value.object)
#define OBJP2GCOBJP(objp) ((eGcObject*)objp)
#define OBJDREF(ref) (*((eObject*)ref))

#define objisnone(obj) ((obj).type == ENONE)
#define objisnil(obj) ((obj).type == ENIL)
#define objisbool(obj) ((obj).type == EBOOL)
#define objisnumber(obj) ((obj).type == ENUMBER)
#define objisstring(obj) ((obj).type == ESTRING)
#define objisvector(obj) ((obj).type == EVECTOR)
#define objisdictionary(obj) ((obj).type == EDICT)
#define objisfunction(obj) ((obj).type == EFUNCTION)
#define objisclass(obj) ((obj).type == ECLASS)
#define objisnativeclass ((obj).type == EUCLASS)
#define objisclosure(obj) ((obj).type == ECLOSURE)
#define objisnativefunc(obj) (((OBJ_AS_FUNC(obj))->is_native))
#define objisiterator(obj) ((obj).type == EITER)
#define objistime(obj) ((obj).type == ETTIME)
#define objismath(obj) ((obj).type == ETTMATH)
#define objisstream(obj) ((obj).type == ESTREAM)
#define objismethod(obj) (objisfunction(obj) && OBJ_AS_FUNC(obj)->is_method)
#define objisbuiltin(obj) \
    (objisvector(obj) || objisstring(obj) || objisdictionary(obj) ||\
     objisnumber(obj) || objisbool(obj) || objisstream(obj))



#define OBJ_AS_STREAM(obj) ((eStream*)(obj).value.object)

#define funcname(f) OBJ_AS_CSTR(OBJ_AS_FUNC(f)->name)
#define classname(c) OBJ_AS_CSTR(OBJ_AS_CLASS(c)->name)
#define closurename(c) OBJ_AS_CSTR(OBJ_AS_CLOSURE(c)->f->name)

typedef enum
{
    ENIL,
    EBOOL,
    ENUMBER,
    ESTRING,
    EVECTOR,
    EDICT,
    EFUNCTION,
    ECLOSURE,
    ECLASS,
    EITER,
    ESTREAM,
    ETTIME,
    ETTMATH,
    EBCODE,
    ENONE
}ValueType;



typedef union Value
{
    bool boolean;
    double number;
    void* object;
}Value;

typedef struct eGc
{
    struct eGcObject* objects;
    esize_t obj_count;
    esize_t obj_max;
}eGc;

typedef struct eGcObject
{
    struct eGcObject* next;
    ValueType type;
    char marked;
}eGcObject;

#define GCOBJ(t,m) ((eGcObject){ NULL, .type=t, .marked=m })

typedef struct eObject
{
    ValueType type;
    Value value;
}eObject;


void run_gc(eGc *gc);
void collect(eGcObject* obj);
void gc_init(eGc* gc);

void gc_mark(eObject* objp);
int gc_sweep(eGc* gc);

eGcObject* egcobject_new(ValueType type, esize_t size);


typedef struct eString
{
    eGcObject gcobj;
    esize_t size;
    char* text;
}eString;


eString* estring_new(const char* text);

char* copy_buffer(const char* ptr, int len);

eObject estring_fromtext(const char* text, int len);

eObject efmtstring_fromtext(const char* text, int len);

eObject eobj_string(const char* text);

void estring_delete(eString* s);

void estring_append(eString* s, int ch);

bool eobj_isequal(eObject a, eObject b);

bool eobj_istrue(eObject obj);

char* eobj_to_cstr(eObject obj);

void print_object(eObject obj);

char* eobj_type(eObject obj);

void eobj_delete(eObject obj);

typedef struct array_iterator
{
    eGcObject gcobj;
    esize_t idx;
    bool (*has_next)(eObject*, struct array_iterator*);
    eObject (*next)(struct array_iterator*);
    eObject* ary;
}array_iterator;

array_iterator* make_iter(eObject* ary, bool (*has_next)(eObject*, array_iterator*));
eObject eobj_iterator(array_iterator* it);

void delete_iter(array_iterator* it);
bool has_next(eObject* ary, array_iterator* it);
eObject next(array_iterator* it);
eObject iter(array_iterator* it);

typedef struct eStream
{
    eGcObject gcobj;
    esize_t size;
    const char* name;
    const char* mode;
    FILE* fp;
}eStream;

eStream* estream_new();
void estream_delete(eStream* stream);
eObject eobj_stream(eStream* stream);



typedef struct eTime
{
    eGcObject gcobj;
    time_t rawtime;
    struct tm * timeinfo;
}eTime;

eTime* etime_new();
void etime_delete(eTime* time);
eObject eobj_time(eTime* time);


typedef struct eMath
{
    eGcObject gcobj;
}eMath;

eMath* emath_new();
void emath_delete(eMath* math);
eObject eobj_math(eMath* math);
eObject eobj_clone(eObject obj);
bool obj_isnumeric(eObject obj);


#endif // EOBJECT_H

