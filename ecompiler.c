#include "ecompiler.h"


#define curr_code_get (OBJ_AS_FUNC(compiler->curr_fn)->code)
#define const_add(constant) bytecode_constadd(curr_code_get, constant)
#define byte_emit(byte) \
    bytecode_push(curr_code_get, byte, compiler->last.line)
#define curr_fn_get \
    (OBJ_AS_FUNC(compiler->curr_fn))


void definition(eCompiler* compiler);
void field_def(eCompiler* compiler);
void class_def(eCompiler *compiler);
void constructor_def(eCompiler* compiler, eObject klass, ebyte_t* num, ebyte_t* argc, bool has_parent, ebyte_t supnum);
void method_def(eCompiler* compiler, eObject caller, /*bool has_name, */bool has_parent, ebyte_t supnum);
void parameters(eCompiler* compiler);
void function_def(eCompiler* compiler);
void statement(eCompiler* compiler);
void block_stmt(eCompiler* compiler);
void return_stmt(eCompiler* compiler);
void orterm(eCompiler* compiler);
void assignment(eCompiler* compiler);
void compound_op(eCompiler* compiler);
void comparation(eCompiler* compiler);
void concat(eCompiler* compiler);
void slice_right_empty(eCompiler* compiler);
void slice_left_empty(eCompiler* compiler);
void slice(eCompiler* compiler);
void dot_sym(eCompiler* compiler);
void call(eCompiler* compiler);
void parse_param(eCompiler* compiler);
static void bind_symbol(eCompiler* compiler);
static void push_scope(eCompiler* compiler);
static void pop_scope(eCompiler* compiler);
int parse_source(eCompiler* compiler);
ErrorType eval_source(const char* source, const char* filename);

static void syntax_error(eCompiler* compiler, const char* msg)
{
    compiler->err_msg = strdup(msg);
    compiler->err = SyntaxError;
    longjmp(go,1);
}

static void unexp_token_error(eCompiler* compiler)
{
    char* tok_text = copy_buffer(compiler->current.start, compiler->current.length);
    if (strlen(tok_text) == 0)tok_text = " ";
    char buf[80];
    strcpy(buf, "Unexpected token \'");
    strcat(buf, tok_text);
    strcat(buf, "\'");
    syntax_error(compiler, buf);
    free(tok_text);
}


void compiler_init(eCompiler* compiler)
{
    compiler->current = token_get(compiler->lexer);
    compiler->last = compiler->current;
    compiler->depth = 0;
    compiler->fn_scope = 0;
    compiler->err = NoError;
    compiler->err_msg = NULL;
    compiler->curr_fn = eobj_function("", bytecode_new());
    compiler->external = false;

    compiler->can_return = false;
    compiler->inside_classbdy = 0;
    compiler->inside_loop = 0;
}

bool match(eCompiler* compiler, TokenType type)
{
    if (compiler->current.type == type)
    {
        //elog("['%.*s']", (int)compiler->current.length, compiler->current.start);
        compiler->last = compiler->current;
        compiler->current = token_get(compiler->lexer);
        return true;
    }
    return false;
}

bool check(eCompiler* compiler, TokenType type)
{
    if (compiler->current.type == type)
    {
        return true;
    }
    return false;
}

bool expect(eCompiler* compiler, TokenType type, const char* msg)
{
    if (compiler->current.type == type)
    {
//        elog("['%.*s']", (int)compiler->current.length, compiler->current.start);
        compiler->last = compiler->current;
        compiler->current = token_get(compiler->lexer);
        return true;
    }
    char* tok_text = copy_buffer(compiler->current.start, compiler->current.length);
    char buf[80];
    strcpy(buf, msg);
    strcat(buf, "before \'");
    strcat(buf, tok_text);
    strcat(buf, "\'");
    syntax_error(compiler, buf);
    free(tok_text);

    return false;
}

static esize_t initializer_list(eCompiler* compiler)
{
    esize_t count = 0;
    if (!match(compiler, ETOKEN_R_BRACK))
    {
        do
        {
            compound_op(compiler);
            count++;
        }while (match(compiler, ETOKEN_COMMA));
        expect(compiler, ETOKEN_R_BRACK, "Expect ']' ");
    }
    return count;
}

static esize_t dictionary_list(eCompiler* compiler)
{
    esize_t count = 0;
    if (!match(compiler, ETOKEN_R_CURLY))
    {
        do
        {
            compound_op(compiler);

            match(compiler, ETOKEN_COLON);

            compound_op(compiler); count++;
        }
        while (match(compiler, ETOKEN_COMMA));
        expect(compiler, ETOKEN_R_CURLY, "Expct '}'");
    }
    return count*2;
}

static eObject sym_name(eCompiler* compiler, ebyte_t* n)
{
    eObject name = estring_fromtext(compiler->last.start, (int)compiler->last.length);
    if (compiler->fn_scope == 0)
        *n = bytecode_constaddg(curr_code_get, name);
    else
        *n = bytecode_constadd(curr_code_get, name);

    return name;
}

void lambda_decl(eCompiler* compiler)
{
    compiler->can_return = true;
    /* store current function */
    eObject prev_fn = compiler->curr_fn;
    /* create this function object */
    compiler->curr_fn = eobj_function("lambda", bytecode_new());
    /* and assign the previous to it's outer pointer */
    curr_fn_get->outer = OBJ_AS_FUNC(prev_fn);

    if (compiler->fn_scope > 0)
        curr_fn_get->is_closure = true;/* scope > 0 is a closure */
    /* grab outer or main function */
    eObject func = compiler->curr_fn;
    /* and set the current bytecode pointer to that of this function */
    curr_fn_get->code = OBJ_AS_FUNC(func)->code;

    parameters(compiler);
    expect(compiler, ETOKEN_L_CURLY, "Expect '{' ");
    /* increment scope number */
    push_scope(compiler);
    compiler->depth++;

    return_stmt(compiler);

    compiler->depth--;
    expect(compiler, ETOKEN_R_CURLY, "Expect '}' ");

    if (compiler->err != NoError)
        return;

    pop_scope(compiler);

    compiler->curr_fn = prev_fn;

    ebyte_t fnum = const_add(func);//

    byte_emit(OP_CONST);
    byte_emit(fnum);

    if (compiler->fn_scope > 0)
    {
        byte_emit(OP_MAKE_CLOSURE);
    }
}

static int find_symbol(eFunction* f, eObject* name, int* scope_in)
{
    elog("Searching...%s\n", OBJ_AS_STR(*name)->text);
    ebyte_t idx;

    elog("Look in scope %d\n", f->scope);

    if (bytecode_findlocal(f->code, *name, &idx))
    {
        *scope_in = f->scope;
        return idx;
    }
    else if (bytecode_findglobal(f->code, *name, &idx))
    {
        *scope_in = 0;
        return idx;
    }
    else if (f->outer)
    {
        elog("Look in next scope %d\n", f->outer->scope);
        return find_symbol(f->outer, name, scope_in);
    }

    *scope_in = -1;
    return -1;
}

int find_let_local(eCompiler* compiler, const char* name)
{
    for (int i=curr_fn_get->let_count-1; i>=0; i--)
    {
        if (strcmp(curr_fn_get->let_locals[i].name, name) == 0)
        {
            return i;
        }
    }
    return -1;
}

void term(eCompiler* compiler)
{
    if (match(compiler, ETOKEN_NIL))
    {
        byte_emit(OP_CONST);
        byte_emit(const_add(NIL));
    }
    else if (match(compiler, ETOKEN_TRUE))
    {
        byte_emit(OP_CONST);
        byte_emit(const_add(BOOL(true)));
    }
    else if (match(compiler, ETOKEN_FALSE))
    {
        byte_emit(OP_CONST);
        byte_emit(const_add(BOOL(false)));
    }
    else if (match(compiler, ETOKEN_NUMBER))
    {
        char buf[10];
        sprintf(buf, "%.*s", (int)compiler->last.length, compiler->last.start);
        byte_emit(OP_CONST);
        byte_emit(const_add(NUMBER(atof(buf))));
    }
    else if (match(compiler, ETOKEN_XNUMBER))
    {
        char buf[10];
        char* endp;
        sprintf(buf, "%.*s", (int)compiler->last.length, compiler->last.start);
        byte_emit(OP_CONST);
        byte_emit(const_add(NUMBER(strtol(buf, &endp, 16))));
    }
    else if (match(compiler, ETOKEN_STRING))
    {
        eObject fmtstr = efmtstring_fromtext(compiler->last.start,
                                             compiler->last.length - 1);
        byte_emit(OP_CONST);
        byte_emit(const_add(fmtstr));
    }
    else if (match(compiler, ETOKEN_FUNC))
    {
        function_def(compiler);
    }
    else if (match(compiler, ETOKEN_LAMBDA))
    {
        lambda_decl(compiler);
    }
    else if (match(compiler, ETOKEN_SELF))
    {
        byte_emit(OP_LSELF);
    }
    else if(match(compiler, ETOKEN_NEW))
    {
//        if (!check(compiler, ETOKEN_SYM))
//            expect(compiler, ETOKEN_SYM, "Expect type name ");
        dot_sym(compiler);
        byte_emit(OP_CALL);
        byte_emit(0);
        byte_emit(OP_DUP_TOS);

        ebyte_t constructnum = const_add(NEW_STRING("__init"));
        byte_emit(OP_FLOAD);
        byte_emit(constructnum);

        ebyte_t argc = 0;
        if (match(compiler, ETOKEN_L_PAREN))
        {
            if (!check(compiler, ETOKEN_R_PAREN))
            {
                do
                {
                    compound_op(compiler);
                    argc++;
                }
                while (match(compiler, ETOKEN_COMMA));
                expect(compiler, ETOKEN_R_PAREN, "Expect ')' ");
            }
            else
            {
                expect(compiler, ETOKEN_R_PAREN, "Expect ')' ");
            }
        }
        byte_emit(OP_CALLINIT);
        byte_emit(argc);
    }
    else if (match(compiler, ETOKEN_SYM))
    {
        int idx;
        int scope_in;
        eObject name = estring_fromtext(compiler->last.start,
                                        compiler->last.length);

        int lnum;
        if ((lnum = find_let_local(compiler, OBJ_AS_STR(name)->text)) != -1)
        {
            byte_emit(OP_LLOAD);
            byte_emit(lnum);
            return;
        }

        elog("Sym %s, look from scope %d\n", OBJ_AS_CSTR(name),
               OBJ2FUNC(compiler->curr_fn)->scope);

        idx = find_symbol(OBJ_AS_FUNC(compiler->curr_fn), &name, &scope_in);
        elog("has idx %d in scope %d\n", idx, scope_in);

        if (scope_in == 0 || (scope_in == -1 && idx == -1))
        {
            byte_emit(OP_GLOAD);
            ebyte_t num = bytecode_constaddg(OBJ_AS_FUNC(compiler->curr_fn)->code, name);
            elog("GLOAD sym %s\n",OBJ_AS_CSTR(name));
            byte_emit(num);
        }
        else if (scope_in < OBJ_AS_FUNC(compiler->curr_fn)->scope && scope_in != -1)
        {
            byte_emit(OP_ULOAD);
            ebyte_t lnum = bytecode_constadduv(OBJ_AS_FUNC(compiler->curr_fn)->code, name);
            elog("ULOAD sym %s\n",OBJ_AS_CSTR(name));
            byte_emit(lnum);
        }
        else if (scope_in == OBJ_AS_FUNC(compiler->curr_fn)->scope)
        {
            byte_emit(OP_LOAD);
            ebyte_t lnum = bytecode_constadd(OBJ_AS_FUNC(compiler->curr_fn)->code, name);
            elog("LOAD sym %s\n",OBJ_AS_CSTR(name));
            byte_emit(lnum);
        }
    }
    else if (match(compiler, ETOKEN_L_BRACK))
    {
        esize_t size = initializer_list(compiler);
        byte_emit(OP_MAKE_VECTOR);
        byte_emit(size);
    }
    else if (match(compiler, ETOKEN_L_CURLY))
    {
        esize_t size = dictionary_list(compiler);
        byte_emit(OP_MAKE_DICT);
        byte_emit(size);
    }
    else if (match(compiler, ETOKEN_L_PAREN))
    {
        compound_op(compiler);
        expect(compiler, ETOKEN_R_PAREN, "Expect ')' ");
    }
    else
    {
        if (match(compiler, ETOKEN_COMMERR))
        {
            syntax_error(compiler, "Expected expression or space before '*'");
        }
        unexp_token_error(compiler);
    }
}

void slice_right_empty(eCompiler* compiler)
{
    byte_emit(OP_CONST);
    byte_emit(const_add(NIL));
}

void slice_left_empty(eCompiler* compiler)
{
    byte_emit(OP_CONST);
    byte_emit(const_add(NIL));
    slice(compiler);
}

void slice(eCompiler* compiler)
{
     match(compiler, ETOKEN_COLON);

     if (check(compiler, ETOKEN_R_BRACK))
     {
         slice_right_empty(compiler);
     }
     else
     {
         compound_op(compiler);
     }
}

void call(eCompiler* compiler)
{

    dot_sym(compiler);

    while (1)
    {
        char* name = copy_buffer(compiler->last.start, compiler->last.length);
        elog("call -> %s\n", name);
        ebyte_t argc = 0;
        if (match(compiler, ETOKEN_L_PAREN))
        {
            if (!check(compiler, ETOKEN_R_PAREN))
            {
                do
                {
                    compound_op(compiler);
                    argc++;
                }
                while (match(compiler, ETOKEN_COMMA));
                expect(compiler, ETOKEN_R_PAREN, "Expect ')' ");
            }
            else
            {
                expect(compiler, ETOKEN_R_PAREN, "Expect ')' ");
            }
            if (strcmp("__init", name) == 0)byte_emit(OP_CALLINIT); else byte_emit(OP_CALL);
            byte_emit(argc);
        }
        else if (match(compiler, ETOKEN_L_BRACK))
        {
            bool is_slice = false;
            if (check(compiler, ETOKEN_COLON))
            {
                is_slice = true;
                slice_left_empty(compiler);
            }
            else
            {
                compound_op(compiler);

                if (check(compiler, ETOKEN_COLON))
                {
                    is_slice = true;
                    slice(compiler);
                }
            }
            expect(compiler, ETOKEN_R_BRACK, "Expect ']' ");
            if (is_slice && !check(compiler, ETOKEN_EQUAL))
            {
                byte_emit(OP_SLICEA);
            }

            if (match(compiler, ETOKEN_EQUAL))
            {
                compound_op(compiler);
                if (is_slice)
                {
                    byte_emit(OP_SLICEA_UPDATE);
                }
                else
                {
                    byte_emit(OP_ARRAY_UPDATE);
                }
            }
            else if (!is_slice)
            {
                byte_emit(OP_ARRAY_LOOKUP);
            }

        }
        else if (match(compiler, ETOKEN_POINTER))
        {
            expect(compiler, ETOKEN_SYM, "Expect field name ");
            byte_emit(OP_CONST);
            eObject s = estring_fromtext(compiler->last.start, compiler->last.length);
            byte_emit(bytecode_constadd(OBJ_AS_FUNC(compiler->curr_fn)->code, s));

            if (match(compiler, ETOKEN_EQUAL))
            {
                compound_op(compiler);
                byte_emit(OP_ARRAY_UPDATE);
            }
            else
                byte_emit(OP_ARRAY_LOOKUP);
        }
        else if (match(compiler, ETOKEN_DOT))
        {
            match(compiler, ETOKEN_SYM);
            eObject name = estring_fromtext(compiler->last.start, compiler->last.length);
            elog("Sym %s, scope %d\n", OBJ_AS_CSTR(name),
                   OBJ2FUNC(compiler->curr_fn)->scope);
            if (strcmp(OBJ_AS_CSTR(name), "__init") == 0 && curr_fn_get->scope == 0)
                syntax_error(compiler, "Cannot invoke '__init' in global scope.");
            if (strcmp(OBJ_AS_CSTR(name), "super") == 0 && curr_fn_get->scope == 0)
                syntax_error(compiler, "Cannot invoke 'super' in global scope.");

            if (match(compiler, ETOKEN_EQUAL))
            {
                compound_op(compiler);
                byte_emit(OP_FSTORE);
            }
            else
                byte_emit(OP_FLOAD);

            ebyte_t num = bytecode_constadd(OBJ_AS_FUNC(compiler->curr_fn)->code, name);
            elog("FLOAD sym %s\n",OBJ_AS_CSTR(name));
            byte_emit(num);
        }
        else
        {
            free(name);
            break;
        }
        free(name);
    }
}

void dot_sym(eCompiler* compiler)
{
    term(compiler);
    while(1){
        if (match(compiler, ETOKEN_DOT))
        {
            match(compiler, ETOKEN_SYM);
            eObject name = estring_fromtext(compiler->last.start, compiler->last.length);
            elog("Sym %s, scope %d\n", OBJ_AS_CSTR(name),
                 OBJ2FUNC(compiler->curr_fn)->scope);
            if (strcmp(OBJ_AS_CSTR(name), "__init") == 0 && curr_fn_get->scope == 0)
                syntax_error(compiler, "Cannot invoke '__init' in global scope.");
            if (strcmp(OBJ_AS_CSTR(name), "super") == 0 && curr_fn_get->scope == 0)
                syntax_error(compiler, "Cannot invoke 'super' in global scope.");

            if (match(compiler, ETOKEN_EQUAL))
            {
                orterm(compiler);
                byte_emit(OP_FSTORE);
            }
            else
                byte_emit(OP_FLOAD);

            ebyte_t num = bytecode_constadd(OBJ_AS_FUNC(compiler->curr_fn)->code, name);
            elog("FLOAD sym %s\n",OBJ_AS_CSTR(name));
            byte_emit(num);
        }
        else
        {
            break;
        }
    }
}

void notop(eCompiler* compiler)
{
    if (match(compiler, ETOKEN_NOT) ||
        match(compiler, ETOKEN_MINUS))
    {
        ebyte_t type = compiler->last.type;
        compound_op(compiler);
        switch (type)
        {
        case ETOKEN_NOT:
            byte_emit(OP_NOT);
            break;
        case ETOKEN_MINUS:
            byte_emit(OP_NEG);
            break;
        default:
            break;
        }
    }
    else
        call(compiler);
}

void factor(eCompiler* compiler)
{
    notop(compiler);
    while (match(compiler, ETOKEN_MULT) || match(compiler, ETOKEN_DIV) ||
           match(compiler, ETOKEN_MODULO))
    {
        char type = compiler->last.type;
        notop(compiler);
        switch (type) {
        case ETOKEN_MULT:
            byte_emit(OP_MUL);
            break;
        case ETOKEN_DIV:
            byte_emit(OP_DIV);
            break;
        case ETOKEN_MODULO:
            byte_emit(OP_MODULO);
            break;
        default:
            break;
        }
    }
}

void expr(eCompiler* compiler)
{
    factor(compiler);
    while (match(compiler, ETOKEN_PLUS) ||
           match(compiler, ETOKEN_MINUS))
    {
        char type = compiler->last.type;
        factor(compiler);
        switch (type) {
        case ETOKEN_PLUS:
            byte_emit(OP_ADD);
            break;
        case ETOKEN_MINUS:
            byte_emit(OP_SUB);
            break;
        default:
            break;
        }
    }
}

void shift(eCompiler* compiler)
{
    expr(compiler);
    while (match(compiler, ETOKEN_L_SHIFT) ||
           match(compiler, ETOKEN_R_SHIFT))
    {
        char type = compiler->last.type;
        expr(compiler);
        switch (type) {
        case ETOKEN_L_SHIFT:
            byte_emit(OP_LSHFT);
            break;
        case ETOKEN_R_SHIFT:
            byte_emit(OP_RSHFT);
            break;
        default:
            break;
        }
    }
}

void concat(eCompiler* compiler)
{
    shift(compiler);
    while (match(compiler, ETOKEN_DOT_DOT))
    {
        char type = compiler->last.type;
        shift(compiler);
        switch (type) {
        case ETOKEN_DOT_DOT:
            byte_emit(OP_CONCAT);
            break;
        default:
            break;
        }
    }
}

void comparation(eCompiler* compiler)
{
    concat(compiler);
    while (match(compiler, ETOKEN_GT) || match(compiler, ETOKEN_GE) ||
           match(compiler, ETOKEN_LT) || match(compiler, ETOKEN_LE))
    {
        TokenType type = compiler->last.type;
        concat(compiler);
        switch (type) {
        case ETOKEN_GT:
            byte_emit(OP_GT);
            break;
        case ETOKEN_LT:
            byte_emit(OP_LT);
            break;
        case ETOKEN_GE:
            byte_emit(OP_GE);
            break;
        case ETOKEN_LE:
            byte_emit(OP_LE);
            break;
        default:
            break;
        }
    }
}

void equality(eCompiler* compiler)
{
    comparation(compiler);
    while (match(compiler, ETOKEN_EQ) ||
           match(compiler, ETOKEN_NE))
    {
        TokenType type = compiler->last.type;
        comparation(compiler);
        switch (type) {
        case ETOKEN_EQ:
            byte_emit(OP_EQ);
            break;
        case ETOKEN_NE:
            byte_emit(OP_NE);
            break;
        default:
            break;
        }
    }
}

void bw_andterm(eCompiler* compiler)
{
    equality(compiler);
    while (match(compiler, ETOKEN_B_AND))
    {
        equality(compiler);
        byte_emit(OP_BAND);
    }
}

void bw_xorterm(eCompiler* compiler)
{
    bw_andterm(compiler);
    while (match(compiler, ETOKEN_B_XOR))
    {
        bw_andterm(compiler);
        byte_emit(OP_BXOR);
    }
}

void bw_orterm(eCompiler* compiler)
{
    bw_xorterm(compiler);
    while (match(compiler, ETOKEN_B_OR))
    {
        bw_xorterm(compiler);
        byte_emit(OP_BOR);
    }
}

void andterm(eCompiler* compiler)
{
    bw_orterm(compiler);
    while (match(compiler, ETOKEN_AND))
    {
        bw_orterm(compiler);
        byte_emit(OP_AND);
    }
}

void orterm(eCompiler* compiler)
{
    andterm(compiler);
    while (match(compiler, ETOKEN_OR))
    {
        andterm(compiler);
        byte_emit(OP_OR);
    }
}

#define patch_store(x,n) \
    switch(x){\
    case OP_GLOAD:\
        byte_emit(OP_GSTORE);\
        byte_emit(n);\
        break;\
    case OP_LOAD:\
        byte_emit(OP_STORE);\
        byte_emit(n);\
        break;\
    case OP_LSELF:\
        byte_emit(OP_LSELF);\
        byte_emit(OP_SWAP);\
        byte_emit(OP_FSTORE);\
        byte_emit(n);\
        break;\
    case OP_ULOAD:\
        byte_emit(OP_USTORE);\
        byte_emit(n);\
        break;\
    default:\
        break;\
    }

#define store_cmp_op(sz) \
    do{\
    ebyte_t opcode, num;\
    if (curr_code_get->raw[sz] == OP_LSELF)\
{\
    opcode = curr_code_get->raw[sz];\
    num = curr_code_get->raw[sz + 2];\
    }\
    else\
{\
    opcode = curr_code_get->raw[sz];\
    num = curr_code_get->raw[sz + 1];\
    }\
    patch_store(opcode, num);\
    } while (0)

void compound_op(eCompiler* compiler)
{
    esize_t size = curr_code_get->size;

    orterm(compiler);
    if (match(compiler, ETOKEN_PLUS_EQUAL))
    {
        elog("--%d\n",curr_code_get->raw[size]);
        elog("--%d\n",curr_code_get->raw[size+2]);
        orterm(compiler);
        byte_emit(OP_COMPADD);
        store_cmp_op(size);
    }
    else if (match(compiler, ETOKEN_MINUS_EQUAL))
    {
        orterm(compiler);
        byte_emit(OP_COMPSUB);
        store_cmp_op(size);
    }
    else if (match(compiler, ETOKEN_MULT_EQUAL))
    {
        orterm(compiler);
        byte_emit(OP_COMPMUL);
        store_cmp_op(size);
    }
    else if (match(compiler, ETOKEN_DIV_EQUAL))
    {
        orterm(compiler);
        byte_emit(OP_COMPDIV);
        store_cmp_op(size);
    }
}

static esize_t emit_label(eBytecode* bytecode, esize_t line)
{
    bytecode_push(bytecode, 0, line);
    bytecode_push(bytecode, 0, line);

    return bytecode->size - 2;
}

//static void patch_label(eBytecode* bytecode, esize_t sz_at_jump)
//{
//    /* calculate the distance between the jump */
//    /* instruction and the target (current address) */
//    int jmp_dist = bytecode->size - sz_at_jump - 1;

//    if (jmp_dist > UINT16_MAX)
//    {
//        elog("Too much code inside one block.\n");
//    }

//    /* encode in 2 bytes the operand */
//    ebyte_t label_value[2];
//    label_value[0] = (jmp_dist >> 8) & 0xff;
//    label_value[1] = (jmp_dist & 0xff);

//    bytecode_writeat(bytecode, label_value[0], sz_at_jump);
//    bytecode_writeat(bytecode, label_value[1], sz_at_jump + 1);
//}


static void patch_label(eCompiler* compiler, eBytecode* bytecode, esize_t jmp_loc)
{
    int address = curr_code_get->size;


    if ((address - jmp_loc) > UINT16_MAX)
    {
        elog("Too much code inside one block.\n");
    }

    int location[2];
    location[0] = (address >> 8) & 0xff;
    location[1] = (address & 0xff);

    bytecode_writeat(bytecode, location[0], jmp_loc);
    bytecode_writeat(bytecode, location[1], jmp_loc + 1);
}

void if_stmt(eCompiler* compiler)
{
    compound_op(compiler);

    byte_emit(OP_JF);
    int l0 = emit_label(OBJ_AS_FUNC(compiler->curr_fn)->code, compiler->last.line);

    assignment(compiler);

    byte_emit(OP_JMP);
    int l1  = emit_label(OBJ_AS_FUNC(compiler->curr_fn)->code, compiler->last.line);


    patch_label(compiler, OBJ_AS_FUNC(compiler->curr_fn)->code, l0);

    int l3 = -1;
    while (match(compiler, ETOKEN_ELIF))
    {
        compound_op(compiler);

        byte_emit(OP_JF);
        int l2 = emit_label(OBJ_AS_FUNC(compiler->curr_fn)->code, compiler->last.line);

        assignment(compiler);

        byte_emit(OP_JMP);
        l3 = emit_label(OBJ_AS_FUNC(compiler->curr_fn)->code, compiler->last.line);

        patch_label(compiler, OBJ_AS_FUNC(compiler->curr_fn)->code, l2);
    }
    if (match(compiler, ETOKEN_ELSE))
    {
        assignment(compiler);
    }
    patch_label(compiler, OBJ_AS_FUNC(compiler->curr_fn)->code, l1);

    if (l3 != -1)
        patch_label(compiler, OBJ_AS_FUNC(compiler->curr_fn)->code, l3);
}

static int setup_loop(eCompiler* compiler)
{
    int start = curr_code_get->size;

    byte_emit((start >> 8) & 0xff);
    byte_emit((start & 0xff));
    byte_emit(0);
    byte_emit(0);

    return start;
}

static void patch_loop(eCompiler* compiler, int start)
{
    int end = curr_code_get->size;

    bytecode_writeat(curr_code_get, (end >> 8) & 0xff, start + 2);
    bytecode_writeat(curr_code_get, (end & 0xff), start + 3);
}

void foreach_stmt(eCompiler* compiler)
{
    int l0;
    int l1;
    ebyte_t num;
    eObject name;

    match(compiler, ETOKEN_SYM);
    name =  estring_fromtext(compiler->last.start, (int)compiler->last.length);
    expect(compiler, ETOKEN_IN, "expect 'in' token");

    compound_op(compiler);


    byte_emit(OP_GET_ITER);
    byte_emit(OP_SETUP_LOOP);

    int start = setup_loop(compiler);

    l1 = OBJ_AS_FUNC(compiler->curr_fn)->code->size;
    compiler->continue_addr = l1;

    byte_emit(OP_FOR_ITER);
    l0 = emit_label(OBJ_AS_FUNC(compiler->curr_fn)->code, compiler->last.line);

    if (compiler->fn_scope == 0)
    {
        byte_emit(OP_GSTORE);
        num = bytecode_constaddg(OBJ_AS_FUNC(compiler->curr_fn)->code, name);
    }
    else
    {
        byte_emit(OP_STORE);
        num = bytecode_constadd(OBJ_AS_FUNC(compiler->curr_fn)->code, name);
    }
    byte_emit(num);

    compiler->inside_loop++;
    assignment(compiler);
    compiler->inside_loop--;


    byte_emit(OP_JMP);
    byte_emit((l1 >> 8) & 0xff);
    byte_emit(l1 & 0xff);

    patch_label(compiler, OBJ_AS_FUNC(compiler->curr_fn)->code, l0);

    patch_loop(compiler, start);

    byte_emit(OP_POP_LOOP);
}

void while_stmt(eCompiler* compiler)
{
    byte_emit(OP_SETUP_LOOP);


    int start = setup_loop(compiler);


    int l1 = OBJ_AS_FUNC(compiler->curr_fn)->code->size;
    compiler->continue_addr = l1;


    /* condition */
    compound_op(compiler);

    byte_emit(OP_JF);
    int l0 = emit_label(curr_code_get, compiler->last.line);

    compiler->inside_loop++;
    assignment(compiler);
    compiler->inside_loop--;

    byte_emit(OP_JMP);
    byte_emit((l1 >> 8) & 0xff);
    byte_emit(l1 & 0xff);

    patch_label(compiler, OBJ_AS_FUNC(compiler->curr_fn)->code, l0);

    patch_loop(compiler, start);

    byte_emit(OP_POP_LOOP);

}

void continue_stmt(eCompiler* compiler)
{
    if (!compiler->inside_loop)
        syntax_error(compiler, "cannot use \'continue\' outside of a loop");
    byte_emit(OP_CONTINUE);
}

void break_stmt(eCompiler* compiler)
{
    if (!compiler->inside_loop)
        syntax_error(compiler, "cannot use \'break\' outside of a loop");
    byte_emit(OP_BREAK);
}

void block_stmt(eCompiler* compiler)
{
    compiler->depth++;
    curr_fn_get->let_depth++;

    if (!match(compiler, ETOKEN_R_CURLY))
    do
    {
        if (is_eos(compiler->lexer))
        {
            syntax_error(compiler, "Error! End of stream token inside block.\n");
        }
        definition(compiler);
    }
    while (!match(compiler, ETOKEN_R_CURLY));

    curr_fn_get->let_depth--;
    compiler->depth--;

    while (curr_fn_get->let_count > 0 &&
           curr_fn_get->let_locals[curr_fn_get->let_count - 1].depth >
              curr_fn_get->let_depth) {
        byte_emit(OP_POP_VAL);
        curr_fn_get->let_count--;
    }
}

void print_stmt(eCompiler* compiler)
{
    compound_op(compiler);
    byte_emit(OP_PRINT);
}

void return_stmt(eCompiler* compiler)
{
    if (!compiler->can_return)
    {
        syntax_error(compiler, "cannot return from constructor");
    }
    compound_op(compiler);
    byte_emit(OP_RET);
}

void import_stmt(eCompiler* compiler)
{
    ebyte_t num;
    eObject name;

    expect(compiler, ETOKEN_SYM, "Expect symbol ");
    name =  estring_fromtext(compiler->last.start, (int)compiler->last.length);
    num = bytecode_constaddg(OBJ_AS_FUNC(compiler->curr_fn)->code, name);
    byte_emit(OP_IMPORT_LIB);
    byte_emit(num);
    byte_emit(OP_GSTORE);
    byte_emit(num);
}

void use_stmt(eCompiler* compiler)
{
    eObject pathsym;
    /* get the path of the file from the symbol's token */
    expect(compiler, ETOKEN_STRING, "Expect symbol ");
    pathsym =  estring_fromtext(compiler->last.start, (int)compiler->last.length);
    const char* path = OBJ_AS_CSTR(pathsym);

    /* open a new input */
    FILE* fp = NULL;
    Input* in = input_new(fp, path);
	if (in == NULL)
		syntax_error(compiler, "Module not found");

    /* store backtrack data */
    const char* before_sym = compiler->lexer->textp;
    eToken last = compiler->last;
    eToken current = compiler->current;
    eLexer* tmp_lex = compiler->lexer;

    /* parse and compile the file */
    eLexer lexer;
    lexer_init(&lexer, in->text, path);
    compiler->lexer = &lexer;/* use a new lexer object */
    compiler->external = true;/* tells the compiler not to emit HALTH */
    compiler->current = token_get(compiler->lexer);
    parse_source(compiler);

    /* load the backtrack data */
    compiler->external = false;
    compiler->lexer = tmp_lex;
    compiler->lexer->textp = before_sym;
    compiler->last = last;
    compiler->current = current;

    input_delete(in);
}

void statement(eCompiler* compiler)
{    /* store backtrack data */
    const char* before_sym = compiler->lexer->textp;
    eToken last = compiler->last;
    eToken current = compiler->current;
    eLexer* tmp_lex = compiler->lexer;

    if (match(compiler, ETOKEN_PRINT))
    {
        print_stmt(compiler);
    }
    else if (match(compiler, ETOKEN_IF))
    {
        if_stmt(compiler);
    }
    else if (match(compiler, ETOKEN_FOR))
    {
        foreach_stmt(compiler);
    }
    else if (match(compiler, ETOKEN_WHILE))
    {
        while_stmt(compiler);
    }
    else if (match(compiler, ETOKEN_SKIP))
    {
        continue_stmt(compiler);
    }
    else if (match(compiler, ETOKEN_BREAK))
    {
        break_stmt(compiler);
    }
    else if (match(compiler, ETOKEN_L_CURLY))
    {
        if (check(compiler, ETOKEN_STRING))
        {
            if (match(compiler, ETOKEN_STRING) && check(compiler, ETOKEN_COLON))
            {
                /* load the backtrack data */
                compiler->lexer = tmp_lex;
                compiler->lexer->textp = before_sym;
                compiler->last = last;
                compiler->current = current;
                compound_op(compiler);
            }
        }
        else
        {
            block_stmt(compiler);
        }
    }
    else if (match(compiler, ETOKEN_RET))
    {
        return_stmt(compiler);
    }
    else if (match(compiler, ETOKEN_IMPORT))
    {
        import_stmt(compiler);
    }
    else if (match(compiler, ETOKEN_USE))
    {
        use_stmt(compiler);
    }
    else
    {
        compound_op(compiler);
    }
    if (match(compiler, ETOKEN_S_COLON)){;}
}

void push_scope(eCompiler* compiler)
{
    compiler->fn_scope++;
    OBJ_AS_FUNC(compiler->curr_fn)->scope = compiler->fn_scope;
}

void pop_scope(eCompiler* compiler)
{
    compiler->fn_scope--;
    OBJ_AS_FUNC(compiler->curr_fn)->scope = compiler->fn_scope;
}

static void bind_param(eCompiler* compiler, int num)
{
    match(compiler, ETOKEN_EQUAL);
    compound_op(compiler);

    byte_emit(OP_STORE);
    byte_emit(num);

    OBJ_AS_FUNC(compiler->curr_fn)->skip_addr[OBJ_AS_FUNC(compiler->curr_fn)->dargc] =
            OBJ_AS_FUNC(compiler->curr_fn)->code->size;

    OBJ_AS_FUNC(compiler->curr_fn)->dargc++;
}

void parse_param(eCompiler* compiler)
{
    if (match(compiler, ETOKEN_ELLIPSIS))
    {
        OBJ_AS_FUNC(compiler->curr_fn)->has_vargs = true;
        expect(compiler, ETOKEN_SYM, "Expect param name ");
        eObject name =  estring_fromtext(compiler->last.start, (int)compiler->last.length);
        bytecode_constadd(OBJ_AS_FUNC(compiler->curr_fn)->code, name);
//        eVector* pack = evector_new();
//        pack->data = emalloc(sizeof(eObject));
        OBJ_AS_FUNC(compiler->curr_fn)->vargname = name;

        return;
    }
    expect(compiler, ETOKEN_SYM, "Expect param name ");
    eObject name =  estring_fromtext(compiler->last.start, (int)compiler->last.length);
    ebyte_t num = bytecode_constadd(curr_code_get, name);
//    eObject name2 =  estring_fromtext(compiler->last.start, (int)compiler->last.length);
//    evector_push(curr_code_get->argnames, name);

    if (check(compiler, ETOKEN_EQUAL))
    {
        bind_param(compiler, num);
        elog("param %d %s\n",num, OBJ_AS_CSTR(name));
    }

    curr_fn_get->argc++;
}

void parameters(eCompiler* compiler)
{
    /* parse/compile parameters */
    if (match(compiler, ETOKEN_L_PAREN))
    {
        if (!check(compiler, ETOKEN_R_PAREN))
        {
            do
            {
                parse_param(compiler);

            } while (match(compiler, ETOKEN_COMMA));

            expect(compiler, ETOKEN_R_PAREN, "Expect ')' ");
        }
        else
        {
            expect(compiler, ETOKEN_R_PAREN, "Expect ')' ");
        }
    }
}

static void default_constructor(eCompiler* compiler, eObject klass)
{
    eBytecode* ctor_code = bytecode_new();
    eObject ctor = eobj_function("__init", ctor_code);
    OBJ_AS_FUNC(ctor)->argc = 0;
    OBJ_AS_FUNC(ctor)->dargc = 0;
    OBJ_AS_FUNC(ctor)->klass = klass;

    eObject prev_fn = compiler->curr_fn;
    compiler->curr_fn = ctor;

    byte_emit(OP_RET);

    compiler->curr_fn = prev_fn;

    ebyte_t constructnum = const_add(NEW_STRING("__init"));
    ebyte_t ctnum = const_add(ctor);
    byte_emit(OP_CONST);
    byte_emit(ctnum);
    byte_emit(OP_STORE);
    byte_emit(constructnum);
}

void class_def(eCompiler* compiler)
{
    eObject name;
    ebyte_t gnum;


    expect(compiler, ETOKEN_SYM, "Expect class name ");
    name = sym_name(compiler, &gnum);

    bool has_parent = false;
    /* create Class object template to create instances at runtime */
    eBytecode* bytecode = bytecode_new();
    const char* str = OBJ_AS_CSTR(name);
    eObject klass = eobj_class(str, bytecode);

    eClass* the_class = OBJ_AS_CLASS(klass);
    if (compiler->inside_classbdy)
        the_class->is_innerc = true;

    /* store current function */
    eObject prev_fn = compiler->curr_fn;
    compiler->curr_fn = eobj_function(str, bytecode);
    //free(str);

    ebyte_t supnum = 0;

    if (match(compiler, ETOKEN_COLON))
    {
        term(compiler);

        byte_emit(OP_CALL);
        byte_emit(0);

        supnum = bytecode_constadd(curr_code_get, NEW_STRING("super"));

        byte_emit(OP_MAKE_SUBCLASS);

        OBJ_AS_CLASS(klass)->has_parent = true;

        has_parent = true;

        byte_emit(OP_STORE);
        byte_emit(supnum);
    }

    push_scope(compiler);
    compiler->depth++;
    compiler->inside_classbdy++;

    expect(compiler, ETOKEN_L_CURLY, "Expect '{' ");

    ebyte_t constructnum = 0, argnum = 0;
    while (1)
    {
        if (is_eos(compiler->lexer))
        {
            syntax_error(compiler, "Error! End of file inside class block.\n");
        }

        if (match(compiler, ETOKEN_FUNC))
        {
            char* sym_str = copy_buffer(compiler->current.start, compiler->current.length);
            if (strcmp(sym_str, "__init") == 0)
            {
                OBJ_AS_CLASS(klass)->has_init = true;
                constructor_def(compiler,  klass, &constructnum, &argnum, has_parent, supnum);
                elog("PARSE CTOR");
            }
            else
            method_def(compiler, klass, /*true, */has_parent, supnum);
//            constructor_def(compiler,  klass, &constructnum, &argnum, has_parent, supnum);
            free(sym_str);
        }
        else if (match(compiler, ETOKEN_VAR))
        {
            field_def(compiler);
            if (match(compiler, ETOKEN_COMMA))
            {
                do{
                    field_def(compiler);
                }while(match(compiler, ETOKEN_COMMA));
            }
        }
//        else if (match(compiler, ETOKEN_SYM))
//        {
//            char* sym_str = copy_buffer(compiler->last.start, compiler->last.length);
//            if (strcmp(sym_str, "__init") == 0)
//            {
//                OBJ_AS_CLASS(klass)->has_init = true;
//                constructor_def(compiler,  klass, &constructnum, &argnum, has_parent, supnum);
//                elog("PARSE CTOR");
//                free(sym_str);
//            }
//            else
//            {
//                syntax_error(compiler, "missing 'var' or 'func' in field or method declaration.");
//            }
//        }
        else if (match(compiler, ETOKEN_CLASS))
        {
            class_def(compiler);
        }
        else
        {
            break;
        }
        if (match(compiler, ETOKEN_S_COLON)){;}
    }
    expect(compiler, ETOKEN_R_CURLY, "Expect '}' ");

    //default constructor

    if (!OBJ_AS_CLASS(klass)->has_init)
    {
        default_constructor(compiler, klass);
    }

    pop_scope(compiler);
    compiler->depth--;
    compiler->inside_classbdy--;

    if (has_parent)
    {
        OBJ_AS_CLASS(klass)->has_parent = has_parent;
    }

    ebyte_t selfnum = bytecode_constadd(OBJ_AS_FUNC(compiler->curr_fn)->code,  klass);
    byte_emit(OP_CONST);
    byte_emit(selfnum);
    byte_emit(OP_MAKE_INSTANCE);

    byte_emit(OP_RET);


    compiler->curr_fn = prev_fn;

    ebyte_t knum = bytecode_constadd(OBJ_AS_FUNC(compiler->curr_fn)->code, klass);

    byte_emit(OP_CONST);
    byte_emit(knum);
    if (compiler->fn_scope == 0)
        byte_emit(OP_GSTORE);
    else
        byte_emit(OP_STORE);
    byte_emit(gnum);

}

static void auto_return(eCompiler* compiler, esize_t block_start)
{
    esize_t block_len = block_start - curr_fn_get->code->size;

    if (block_len == 0 || curr_code_get->raw[curr_code_get->size - 1] != OP_RET)
    {
        byte_emit(OP_CONST);
        byte_emit(const_add(NIL));
        byte_emit(OP_RET);
    }
}

void function_def(eCompiler* compiler)
{
    compiler->can_return = true;

    eObject name;
    ebyte_t fnum;
    bool has_name = false;

    if (check(compiler, ETOKEN_SYM))
    {
        match(compiler, ETOKEN_SYM);
        name = sym_name(compiler, &fnum);
        has_name = true;
    }
    /* store current function */
    eObject prev_fn = compiler->curr_fn;
    const char* str = has_name ? OBJ_AS_CSTR(name) : "anonymous";
    /* create this function object */
    compiler->curr_fn = eobj_function(str, bytecode_new());
    /* scope > 0 is a closure */
    if (compiler->fn_scope > 0)
    {
        /* assign the previous to this function's 'outer' pointer */
        curr_fn_get->outer = OBJ_AS_FUNC(prev_fn);
        curr_fn_get->is_closure = true;
    }
    /* grab this function */
    eObject func = compiler->curr_fn;

    parameters(compiler);

    expect(compiler, ETOKEN_L_CURLY, "Expect '{' ");

    push_scope(compiler);

    esize_t block_start = curr_fn_get->code->size;

    block_stmt(compiler);

    auto_return(compiler, block_start);

    pop_scope(compiler);

    compiler->curr_fn = prev_fn;

    ebyte_t knum = const_add(func);
    byte_emit(OP_CONST);
    byte_emit(knum);

    if (has_name)
    {
        if (compiler->fn_scope > 0)
        {
            byte_emit(OP_MAKE_CLOSURE);
            byte_emit(OP_STORE);
        }
        else
        {
            byte_emit(OP_GSTORE);
        }
        byte_emit(fnum);
    }
    else if (compiler->fn_scope > 0)
    {
        byte_emit(OP_MAKE_CLOSURE);
    }
}

void constructor_def(eCompiler* compiler, eObject klass, ebyte_t* num, ebyte_t* argc, bool has_parent, ebyte_t supnum)
{
    compiler->can_return = false;

    match(compiler, ETOKEN_SYM);
    eObject name = estring_fromtext(compiler->last.start, (int)compiler->last.length);
    ebyte_t constructnum = bytecode_constadd(curr_code_get, name);
    *num = constructnum;

    eObject prev_fn = compiler->curr_fn;/* store current function */

    eObject func = eobj_function(OBJ_AS_CSTR(name), bytecode_new());/* create this function object */

    OBJ_AS_FUNC(func)->outer = OBJ_AS_FUNC(prev_fn);/* assign the previous to it's outer pointer */

    compiler->curr_fn = func;/* grab this function */

    parameters(compiler);

    if (has_parent)
    {
        curr_fn_get->is_child = true;
        byte_emit(OP_LSELF);
        byte_emit(OP_FLOAD);
        byte_emit(curr_fn_get->argc + curr_fn_get->dargc + ((curr_fn_get->has_vargs)?1:0));
        //byte_emit(curr_fn_get->outer->code->size);
        ebyte_t supnum = bytecode_constadd(curr_code_get, NEW_STRING("super"));
        byte_emit(OP_STORE);
        byte_emit(supnum);
    }

    //*argc = OBJ_AS_FUNC(compiler->curr_fn)->argc;

    expect(compiler, ETOKEN_L_CURLY, "Expect '{' ");


    push_scope(compiler);

    block_stmt(compiler);

    byte_emit(OP_RET);

    pop_scope(compiler);


    compiler->curr_fn = prev_fn;

    eFunction* f = OBJ_AS_FUNC(func);
    f->is_ctor = true;
    f->is_method = true;
    f->klass = klass;
    f->true_argc = *argc;

    ebyte_t fnum = bytecode_constadd(curr_code_get, func);

    byte_emit(OP_CONST);
    byte_emit(fnum);
    byte_emit(OP_STORE);
    byte_emit(constructnum);
}

void method_def(eCompiler* compiler, eObject klass, bool has_parent, ebyte_t supnum)
{

    compiler->can_return = true;

    eObject name;
    ebyte_t gnum;

    expect(compiler, ETOKEN_SYM, "Expect method name ");
    name = sym_name(compiler, &gnum);


    eObject prev_fn = compiler->curr_fn;

    char* str = OBJ_AS_CSTR(name);

    eObject func = eobj_function(str, bytecode_new());

    OBJ_AS_FUNC(func)->outer = OBJ_AS_FUNC(prev_fn);

    compiler->curr_fn = func;

    if (curr_fn_get->outer->is_method)
    {
        curr_fn_get->is_closure = true;
    }

    parameters(compiler);

    if (has_parent)
    {
        curr_fn_get->is_child = true;
        byte_emit(OP_LSELF);
        byte_emit(OP_FLOAD);
        byte_emit(curr_fn_get->argc + curr_fn_get->dargc + ((curr_fn_get->has_vargs)?1:0));
        //byte_emit(curr_fn_get->outer->code->size);
        ebyte_t supnum = bytecode_constadd(curr_code_get, NEW_STRING("super"));
        byte_emit(OP_STORE);
        byte_emit(supnum);
    }

    expect(compiler, ETOKEN_L_CURLY, "Expect '{' ");

    push_scope(compiler);/* increment scope count */

    esize_t block_start = curr_fn_get->code->size;

    block_stmt(compiler);

    auto_return(compiler, block_start);

    pop_scope(compiler);

    compiler->curr_fn = prev_fn;

    eFunction* f = OBJ_AS_FUNC(func);
    f->is_method = true;
    f->klass = klass;

    ebyte_t fnum = const_add(func);//

    byte_emit(OP_CONST);
    byte_emit(fnum);

//    if (has_name)
//    {
//        if (compiler->fn_scope > 1 && !OBJ_AS_CLASS(klass)->is_innerc)
    if (curr_fn_get->is_closure)
    {
        byte_emit(OP_MAKE_CLOSURE);
        byte_emit(OP_STORE);
    }
    else
    {
        byte_emit(OP_STORE);
    }
    byte_emit(gnum);
//    }
//    else if (compiler->fn_scope > 1)
//    {
//        byte_emit(OP_MAKE_CLOSURE);
//    }
}


void var_decl(eCompiler* compiler)
{
    expect(compiler, ETOKEN_SYM, "Expect variable name ");
    eObject name = estring_fromtext(compiler->last.start, (int)compiler->last.length);
    ebyte_t num;
    if (curr_fn_get->scope > 0)
    {
        num = bytecode_constadd(OBJ_AS_FUNC(compiler->curr_fn)->code, name);
        expect(compiler, ETOKEN_EQUAL, "Expect '=' ");
        compound_op(compiler);
        elog("Define local %u\n", num);
        byte_emit(OP_STORE);
        byte_emit(num);
    }
    else
    {
        num = bytecode_constaddg(OBJ_AS_FUNC(compiler->curr_fn)->code, name);
        expect(compiler, ETOKEN_EQUAL, "Expect '=' ");
        compound_op(compiler);
        elog("Define global %u\n", num);
        byte_emit(OP_GSTORE);
        byte_emit(num);
    }
}

void field_def(eCompiler* compiler)
{
    expect(compiler, ETOKEN_SYM, "Expect variable name ");
    eObject name = estring_fromtext(compiler->last.start, (int)compiler->last.length);
    ebyte_t lnum = const_add(name);
    expect(compiler, ETOKEN_EQUAL, "Expect '=' ");

//    if (check(compiler, ETOKEN_FUNC) || check(compiler, ETOKEN_LAMBDA))
//        syntax_error(compiler, "Cannot use an anonymous function inside a class main scope.");

    compound_op(compiler);
    elog("Define local %u\n", lnum);
    byte_emit(OP_STORE);
    byte_emit(lnum);

}

static void bind_symbol(eCompiler* compiler)
{
    ebyte_t num;
    eObject name = estring_fromtext(compiler->last.start, (int)compiler->last.length);

    match(compiler, ETOKEN_EQUAL);
    compound_op(compiler);

    int lnum;
    if ((lnum = find_let_local(compiler, OBJ_AS_STR(name)->text)) != -1)
    {
        byte_emit(OP_LSTORE);
        byte_emit(lnum);
        return;
    }

    int scope_in;
    find_symbol(OBJ_AS_FUNC(compiler->curr_fn), &name, &scope_in);
    elog("Scope in %d \n", scope_in);

    if (scope_in == 0 || scope_in == -1)
    {
        elog("bind global");
        byte_emit(OP_GSTORE);
        num = bytecode_constaddg(OBJ_AS_FUNC(compiler->curr_fn)->code, name);
    }
    else if (compiler->fn_scope > 1 && scope_in < compiler->fn_scope)
    {
        elog("bind upvalue");
        byte_emit(OP_USTORE);
        num = bytecode_constadduv(OBJ_AS_FUNC(compiler->curr_fn)->code, name);
    }
    else
    {
        elog("bind local");
        byte_emit(OP_STORE);
        num = bytecode_constadd(OBJ_AS_FUNC(compiler->curr_fn)->code, name);
    }
    byte_emit(num);
}

void definition(eCompiler* compiler)
{
//    const char* before_sym = compiler->lexer->textp;
//    eToken last = compiler->last;
//    eToken current = compiler->current;

    if (match(compiler, ETOKEN_CLASS))
    {
        class_def(compiler);
    }
    else if (match(compiler, ETOKEN_FUNC))
    {
//        bool has_name = false;
//        if (check(compiler, ETOKEN_SYM))has_name = true;
        function_def(compiler);
    }
    else if (match(compiler, ETOKEN_VAR))
    {
        var_decl(compiler);
        if (match(compiler, ETOKEN_COMMA))
        {
            do{
                var_decl(compiler);
            }while(match(compiler, ETOKEN_COMMA));
        }
    }
    else if (match(compiler, ETOKEN_LET))
    {
        match(compiler, ETOKEN_SYM);
        LetLocal* let_val = &curr_fn_get->let_locals[curr_fn_get->let_count++];
        let_val->name = copy_buffer(compiler->last.start, (int)compiler->last.length);
        let_val->depth = curr_fn_get->let_depth;
        printf("Let store %s\n", let_val->name);
        match(compiler, ETOKEN_EQUAL);
        compound_op(compiler);
    }
//    else if (match(compiler, ETOKEN_SYM))
//    {
//        if (check(compiler, ETOKEN_EQUAL))
//        {
//            bind_symbol(compiler);
//            if (match(compiler, ETOKEN_COMMA))
//            {
//                if (expect(compiler, ETOKEN_SYM, "Expect symbol name "))
//                do{
//                    bind_symbol(compiler);
//                }while(match(compiler, ETOKEN_COMMA));
//            }
//        }
//        else
//        {
//            compiler->lexer->textp = before_sym;
//            compiler->last = last;
//            compiler->current = current;
//            compound_op(compiler);
//        }
//    }
    else
    {
        assignment(compiler);
    }

    if (match(compiler, ETOKEN_S_COLON)){;}
}

void assignment(eCompiler* compiler)
{
//    const char* before_sym = compiler->lexer->textp;
//    eToken last = compiler->last;
//    eToken current = compiler->current;
    statement(compiler);
//    if (match(compiler, ETOKEN_SYM))
//    {
    if (check(compiler, ETOKEN_EQUAL))
    {
            /* remove load opcode and argument */
        curr_code_get->size -= 2;

        bind_symbol(compiler);
        if (match(compiler, ETOKEN_COMMA))
        {
//                if (expect(compiler, ETOKEN_SYM, "Expect symbol name "))
            do{
                if (expect(compiler, ETOKEN_SYM, "Expect symbol name "))
                    bind_symbol(compiler);
            }while(match(compiler, ETOKEN_COMMA));
        }
    }
//        else
//        {
//            compiler->lexer->textp = before_sym;
//            compiler->last = last;
//            compiler->current = current;
//            compound_op(compiler);
//        }
//    }
//    else
//    {
//        statement(compiler);
//    }
}

int parse_source(eCompiler* compiler)
{
    while (!match(compiler, ETOKEN_EOS))
    {
        definition(compiler);
    }
    if (!compiler->external)
        byte_emit(OP_HALT);
    return NoError;
}

ErrorType compile_source(eCompiler* compiler)
{
    ErrorType error = NoError;
    compiler_init(compiler);
    error = parse_source(compiler);
    return error;
}

ErrorType print_comperr(eCompiler* compiler)
{
    print_line_with_error(compiler->lexer, compiler->err_msg);
    fprintf(stderr,"%s\n", "Syntax Error");
    free((char*)compiler->err_msg);
    return SyntaxError;
}

ErrorType eval_source(const char* source, const char* filename)
{
    ErrorType error = NoError;
    eLexer lexer;
    eCompiler compiler;

    lexer_init(&lexer, source, filename);
    compiler.lexer = &lexer;

    if (!setjmp(go))
    {
        error = compile_source(&compiler);
    }
    else
    {
        return print_comperr(&compiler);
    }

    if (!setjmp(go))
    {
        evm_init(&vm, OBJ_AS_FUNC(compiler.curr_fn)->code, filename);
#ifdef EMME_EXEC
        error = run_bcode(&vm);
#endif
        dis_bcode(OBJ_AS_FUNC(compiler.curr_fn)->code, "emme dis");
//            dump_code(&vm, &compiler);
    }
    else
    {
        return RuntimeError;
    }


    evm_delete(&vm);

    return error;
}

ErrorType eval_script(const char* path)
{
    FILE* fp = NULL;
    Input* in = input_new(fp, path);
    ErrorType error_type = NoError;
    error_type = eval_source(in->text, path);
    input_delete(in);
    printf("\nEXIT %d\n",error_type);

    return error_type;
}

void init_front(eLexer* lexer, eCompiler* compiler, const char* source, const char* filename)
{
    lexer_init(lexer, source, filename);
    compiler->lexer = lexer;
}

ErrorType compile_line(eCompiler* compiler)
{
    ErrorType error = NoError;
    if (!setjmp(go))
    {
        error = compile_source(compiler);
    }
    else
    {
        return print_comperr(compiler);
    }
    return error;
}
