#include "edisasm.h"
#include "etypes.h"
#include "eobject.h"
#include "edict.h"
#include "efunction.h"

#define DIS_FRMT             "%3d\t%s\t\t%d\t%3lu\t(%s)\n"
#define DIS_SIMPLE_FRMT      "%3d\t%s\t\t\t%3lu\n"
#define DIS_SIMPLEL_FRMT     "%3d\t%s\t\t%3lu\n"
#define DIS_ARRAY_FRMT       "%3d\t%s\t\t%3lu\n"
#define DIS_GETITER_FRMT     "%3d\t%s\t\t%3lu\n"
#define DIS_IMPORT           "%3d\t%s\t%d\t%3lu\t(%s)\n"
#define DIS_CALL_FRMT        "%3d\t%s\t\t%d\t%3lu\n"
#define DIS_CALLI_FRMT       "%3d\t%s\t%d\t%3lu\n"
#define DIS_SUBCLASS         "%3d\t%s\t%d\t%3lu\n"
#define DIS_INERIT           "%3d\t%s\t\t%d\t%3lu\n"
#define DIS_JUMP_FRMT        "%3d\t%s\t\t%u\t%3lu\n"
#define DIS_FORIT            "%3d\t%s\t%d\t%3lu\n"
#define DIS_JUMPBACK_FRMT    "%3d\t%s\t%d\t%3lu\n"
#define DIS_SETLOOP_FRMT     "%3d\t%s\t%d-%d-%d-%d\t%3lu\n"
#define DIS_CLOSURE_FRMT     "%3d\t%s\t\t%3lu\n"
#define DIS_VEC_FRMT         "%3d\t%s\t%d\t%3lu\t\n"
#define DIS_NAME_FRMT        "\n---|%s|---\n"

static char* eobj_repr(eObject obj)
{
    char* buf = emalloc(LARGE_BUFFER);
    memset(buf, 0, LARGE_BUFFER);
    switch (obj.type)
    {
    case ENIL:
        sprintf(buf, "%s", "nil");
        break;
    case EBOOL:
        sprintf(buf,"%s", OBJ_AS_BOOL(obj) ? "true" : "false");
        break;
    case ENUMBER:
        sprintf(buf, NUM_FRMT, OBJ_AS_FLOAT(obj));
        break;
    case ESTRING:
        sprintf(buf, "%s", OBJ_AS_STR(obj)->text);
        break;
    case EVECTOR:
        evector_to_cstring(OBJ_AS_VEC(obj), buf);
        break;
    case EDICT:
        edict_to_cstring(OBJ_AS_DICT(obj), buf);
        break;
    case EITER:
        sprintf(buf, PTR_FMT, "iterator", OBJ_AS_ITER(obj));
        break;
    case ETTIME:
        sprintf(buf, PTR_FMT, "time", OBJ_AS_TIME(obj));
        break;
    case EFUNCTION:
        sprintf(buf, "<func, '%s'>",  OBJ_AS_CSTR(OBJ_AS_FUNC(obj)->name));
        break;
    case ECLOSURE:
        sprintf(buf, "<closure, '%s'> ",  OBJ_AS_CSTR(OBJ_AS_CLOSURE(obj)->f->name));
        break;
    case ECLASS:
        sprintf(buf, "<%s, '%s'>", OBJ_AS_CLASS(obj)->is_inst ? "instance" : "class",
                OBJ_AS_CSTR(OBJ_AS_CLASS(obj)->name));
        break;
    default:
        break;
    }

    return buf;
}

static void dis_simple(const char* name, eBytecode* bytecode, int ip)
{
    fprintf(stdout, DIS_SIMPLE_FRMT, ip, name, bytecode->lines[ip]);
}

static void dis_simplel(const char* name, eBytecode* bytecode, int ip)
{
    fprintf(stdout, DIS_SIMPLEL_FRMT, ip, name, bytecode->lines[ip]);
}

static void dis_array(const char* name, eBytecode* bytecode, int ip)
{
    fprintf(stdout, DIS_ARRAY_FRMT, ip, name, bytecode->lines[ip]);
}

static void dis_getiter(const char* name, eBytecode* bytecode, int ip)
{
    fprintf(stdout, DIS_GETITER_FRMT, ip, name, bytecode->lines[ip]);
}

static void dis_closure(const char* name, eBytecode* bytecode, int ip)
{
    fprintf(stdout, DIS_CLOSURE_FRMT, ip, name, bytecode->lines[ip]);
}

static void dis_inst(const char* name, eBytecode* bytecode, int ip)
{
    int n = bytecode->raw[ip + 1];
//    eObject value = bytecode->locals.constants[n];
    eObject value = bytecode->locals->data[n];
    const char* str = eobj_repr(value);
    fprintf(stdout, DIS_FRMT, ip, name, n, bytecode->lines[ip], str);
    free(str);
}

//static void dis_inst(const char* name, eBytecode* bytecode, int ip)
//{
//    int n = bytecode->raw[ip + 1];
//    eObject value = bytecode->locals.constants[n];
//    const char* str = eobj_repr(value);
//    char* buf;
//    if (objisstring(value))buf = quotes(str);else buf = str;
//    fprintf(stdout, DIS_FRMT, ip, name, n, bytecode->lines[ip], buf);
//}

static void dis_subclass(const char* name, eBytecode* bytecode, int ip)
{
    int n = bytecode->raw[ip + 1];
    fprintf(stdout, DIS_SUBCLASS, ip, name, n, bytecode->lines[ip] );

}

static void dis_inerit(const char* name, eBytecode* bytecode, int ip)
{
    int n = bytecode->raw[ip + 1];
    fprintf(stdout, DIS_INERIT, ip, name, n, bytecode->lines[ip] );

}

static void dis_global(const char* name, eBytecode* bytecode, int ip)
{
    int n = bytecode->raw[ip + 1];
//    eObject value = bytecode->globals.constants[n];
    eObject value = bytecode->globals->data[n];
    char* str = eobj_repr(value);
    fprintf(stdout, DIS_FRMT, ip, name, n, bytecode->lines[ip], str);
    free(str);
}

static void dis_import(const char* name, eBytecode* bytecode, int ip)
{
    int n = bytecode->raw[ip + 1];
//    eObject value = bytecode->globals.constants[n];
    eObject value = bytecode->globals->data[n];
    char* str = eobj_repr(value);
    fprintf(stdout, DIS_IMPORT, ip, name, n, bytecode->lines[ip], str);
    free(str);
}

static void dis_uval(const char* name, eBytecode* bytecode, int ip)
{
    int n = bytecode->raw[ip + 1];
    eObject value = bytecode->upvalues->data[n];
    char* str = eobj_repr(value);
    fprintf(stdout, DIS_FRMT, ip, name, n, bytecode->lines[ip], str);
    free(str);
}

static void dis_call(const char* name, eBytecode* bytecode, int ip)
{
    int n = bytecode->raw[ip + 1];
    fprintf(stdout, DIS_CALL_FRMT, ip, name, n, bytecode->lines[ip]);
}

static void dis_calli(const char* name, eBytecode* bytecode, int ip)
{
    int n = bytecode->raw[ip + 1];
    fprintf(stdout, DIS_CALLI_FRMT, ip, name, n, bytecode->lines[ip]);
}

static void dis_vec(const char* name, eBytecode* bytecode, int ip)
{
    int n = bytecode->raw[ip + 1];
    fprintf(stdout, DIS_VEC_FRMT, ip, name, n, bytecode->lines[ip]);
}

static void dis_jump(const char* name, eBytecode* bytecode, int ip)
{
    int n = bytecode->raw[ip + 1] << 8 | bytecode->raw[ip + 2];
    fprintf(stdout, DIS_JUMP_FRMT, ip, name, n, bytecode->lines[ip]);
}

static void dis_forit(const char* name, eBytecode* bytecode, int ip)
{
    int n = bytecode->raw[ip + 1] << 8 | bytecode->raw[ip + 2];
    fprintf(stdout, DIS_FORIT, ip, name, n, bytecode->lines[ip]);
}


static void dis_jumpback(const char* name, eBytecode* bytecode, int ip)
{
    int n = bytecode->raw[ip + 1] << 8 | bytecode->raw[ip + 2];
    fprintf(stdout, DIS_JUMPBACK_FRMT, ip, name, n, bytecode->lines[ip]);
}

static void dis_setloop(const char* name, eBytecode* bytecode, int ip)
{
    int a = bytecode->raw[ip + 1];
    int b = bytecode->raw[ip + 2];
    int c = bytecode->raw[ip + 3];
    int d = bytecode->raw[ip + 4];
    fprintf(stdout, DIS_SETLOOP_FRMT, ip, name, a, b, c, d,  bytecode->lines[ip]);
}

void dis_bcode(eBytecode* bytecode, const char* name)
{
#ifdef TRACE_ASM
    fprintf(stdout, DIS_NAME_FRMT, name);
    int ip = -1;
    do
    {
        switch (bytecode->raw[++ip]) {
        case OP_HALT:
            dis_simple("HALT",bytecode,ip);
            return;
            break;
        case OP_CONST:
            dis_inst("CONST", bytecode, ip);
            ++ip;
            break;
        case OP_POP_VAL:
            dis_simple("POP_VAL", bytecode, ip);
            break;
        case OP_ADD:
            dis_simple("ADD", bytecode, ip);
            break;
        case OP_SUB:
            dis_simple("SUB", bytecode, ip);
            break;
        case OP_MUL:
            dis_simple("MUL", bytecode, ip);
            break;
        case OP_DIV:
            dis_simple("DIV", bytecode, ip);
            break;
        case OP_MODULO:
            dis_simple("MODULO", bytecode, ip);
            break;
        case OP_LSHFT:
            dis_simple("LSHFT", bytecode, ip);
            break;
        case OP_RSHFT:
            dis_simple("RSHFT", bytecode, ip);
            break;
        case OP_COMPADD:
            dis_simple("COMPADD", bytecode, ip);
            break;
        case OP_COMPSUB:
            dis_simple("COMPSUB", bytecode, ip);
            break;
        case OP_COMPMUL:
            dis_simple("COMPMUL", bytecode, ip);
            break;
        case OP_COMPDIV:
            dis_simple("COMPDIV", bytecode, ip);
            break;
        case OP_GT:
            dis_simple("GT", bytecode, ip);
            break;
        case OP_LT:
            dis_simple("LT", bytecode, ip);
            break;
        case OP_EQ:
            dis_simple("EQ", bytecode, ip);
            break;
        case OP_NE:
            dis_simple("NE", bytecode, ip);
            break;
        case OP_AND:
            dis_simple("AND", bytecode, ip);
            break;
        case OP_OR:
            dis_simple("OR", bytecode, ip);
            break;
        case OP_NOT:
            dis_simple("NOT", bytecode, ip);
            break;
        case OP_NEG:
            dis_simple("NEG", bytecode, ip);
            break;
        case OP_CONCAT:
            dis_simple("CONCAT", bytecode, ip);
            break;
        case OP_GSTORE:
            dis_global("GSTORE", bytecode, ip);
            ++ip;
            break;
        case OP_GLOAD:
            dis_global("GLOAD", bytecode, ip);
            ++ip;
            break;
        case OP_STORE:
            dis_inst("STORE", bytecode, ip);
            ++ip;
            break;
        case OP_LOAD:
            dis_inst("LOAD", bytecode, ip);
            ++ip;
            break;
        case OP_USTORE:
            dis_uval("USTORE", bytecode, ip);
            ++ip;
            break;
        case OP_ULOAD:
            dis_uval("ULOAD", bytecode, ip);
            ++ip;
            break;
        case OP_FSTORE:
            dis_inst("FSTORE", bytecode, ip);
            ++ip;
            break;
        case OP_FLOAD:
            dis_inst("FLOAD", bytecode, ip);
            ++ip;
            break;
        case OP_LLOAD:
            dis_inst("LLOAD", bytecode, ip);
            ++ip;
            break;
        case OP_LSTORE:
            dis_inst("LSTORE", bytecode, ip);
            ++ip;
            break;
        case OP_LSELF:
            dis_simple("LSELF", bytecode, ip);
            break;
        case OP_MAKE_VECTOR:
            dis_vec("MAKE_VECTOR", bytecode, ip);
            ++ip;
            break;
        case OP_MAKE_DICT:
            dis_vec("MAKE_DICT", bytecode, ip);
            ++ip;
            break;
        case OP_ARRAY_LOOKUP:
            dis_array("ARRAY_LOOKUP", bytecode, ip);
            break;
        case OP_ARRAY_UPDATE:
            dis_array("ARRAY_UPDATE", bytecode, ip);
            break;
        case OP_SLICEA:
            dis_simple("SLICE", bytecode, ip);
            break;
        case OP_SLICEA_UPDATE:
            dis_simplel("SLICE_UPDATE", bytecode, ip);
            break;
        case OP_JF:
            dis_jump("JF", bytecode, ip);
            ++ip;++ip;
            break;
        case OP_JMP:
            dis_jump("JMP", bytecode, ip);
            ++ip;++ip;
            break;
        case OP_JMP_BACK:
            dis_jumpback("JMP_BACK", bytecode, ip);
            ++ip;++ip;
            break;
        case OP_BREAK:
            dis_simple("BREAK", bytecode, ip);
            break;
        case OP_CONTINUE:
            dis_simple("CONTINUE", bytecode, ip);
            break;
        case OP_GET_ITER:
            dis_getiter("GET_ITER", bytecode, ip);
            break;
        case OP_FOR_ITER:
            dis_forit("FOR_ITER", bytecode, ip);
            ++ip;++ip;
            break;
        case  OP_SETUP_LOOP:
            dis_setloop("SETUP_LOOP", bytecode, ip);
            ++ip;++ip;++ip;++ip;
            break;
        case OP_POP_LOOP:
            dis_simple("POP_LOOP", bytecode, ip);
            break;
        case OP_CALL:
            dis_call("CALL", bytecode, ip);
            ++ip;
            break;
        case OP_CALLINIT:
            dis_calli("CALLINIT", bytecode, ip);
            ++ip;
            break;
        case OP_CALLMETH:
            dis_call("CALLMETH", bytecode, ip);
            ++ip;
            break;
        case OP_RET:
            dis_simple("RET", bytecode, ip);
            break;
        case OP_MAKE_CLOSURE:
            dis_closure("MAKE_CLOSURE", bytecode, ip);
            break;
        case OP_MAKE_INSTANCE:
            dis_closure("MAKE_INSTANCE", bytecode, ip);
            break;
        case OP_MAKE_SUBCLASS:
            dis_simple("MAKE_SUBCLASS", bytecode, ip);
            break;
        case OP_INERIT:
            dis_inerit("INERIT", bytecode, ip);
            break;
        case OP_PRINT:
            dis_simple("PRINT", bytecode, ip);
            break;
        case OP_FIELD_GET:
            dis_simple("FIELD_GET", bytecode, ip);
            break;
        case OP_IMPORT_LIB:
            dis_import("IMPORT_LIB", bytecode, ip);
            ++ip;
            break;
        case OP_DUP_TOS    :
            dis_simple("DUP_TOS", bytecode, ip);
            break;
        case OP_SWAP    :
            dis_simple("SWAP", bytecode, ip);
            break;
        default:
            break;
        }

    } while (ip < (int)bytecode->size-1);
#endif
}
