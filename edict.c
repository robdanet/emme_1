﻿#include "edict.h"



/* sdbm lib hash function */
/* hash(i) = hash(i - 1) * 65599 + str[i]; */
esize_t hash_it(const char *s, esize_t size)
{
    esize_t hash = 0;
    int c;

    while ((c = *s++)>0)
        hash = c + (hash << 6) + (hash << 16) - hash;

    return hash % size;
}

edict_t* edict_new(esize_t space)
{
    edict_t* d = (edict_t*)egcobject_new(EDICT, sizeof(edict_t));
    edict_init(d, space);
    return d;
}

void edict_delete(edict_t* d)
{
    edict_erase(d);
    free(d);
}

void edict_init(edict_t* dict, esize_t space)
{
    dict->space = space;
    dict->data = emalloc(sizeof(dcell*) * (dict->space));
    dict->size = 0;
    dict->collisions = 0;

    for (esize_t i=0; i<dict->space; i++)
    {
        dict->data[i] = emalloc(sizeof(dcell));
        dict->data[i]->key = -1;
        dict->data[i]->obj = NONE;
        dict->data[i]->empty = true;
        dict->data[i]->str = NULL;
    }
}

void edict_erase(edict_t* dict)
{
    for (esize_t i=0; i<dict->space; i++)
    {
        free(dict->data[i]);
        dict->data[i] = NULL;
    }
    free(dict->data);
    dict->data = NULL;
}

int query_dict(edict_t* dict, int key)
{
    if (dict->data[key]->empty)
        return 0;

    return 1;
}

int edict_insert(edict_t* dict, char* key_string, eObject obj)
{

    int key = hash_it(key_string, dict->space);
    int query = query_dict(dict, key);

    if (query == 0)
    {
        dict->data[key]->key = key;
        dict->data[key]->empty = false;
        dict->data[key]->obj = obj;
        dict->data[key]->str = key_string;
        dict->size++;
//        printf("INSERT {%s : %s} \n", dict->data[key]->str, eobj_to_cstr(dict->data[key]->obj));
        return 1;
    }
    else if (query == 1)
    {
        if (strcmp(dict->data[key]->str, key_string) == 0)
        {
//            fprintf(stdout, "{Key(%s):} pair already in this map, replace value.\n", key_string);
            dict->data[key]->obj = obj;
            return 0;
        }
        else
        {
//            printf("Need reash, key %d collide, size %lu, space %lu\n", key, dict->size, dict->space);
            dict->collisions++;//printf("collision# %lu size %lu\n", dict->collisions, dict->size);
            rehash(dict);
//            edict_print(dict);puts("");
//            char * strp =  OBJ2CSTR(obj);
//            printf("Try to insert next key %s obj %s\n", key_string, strp);
//            free(strp);

            return edict_insert(dict, key_string, obj);
        }
    }

    /* something wrong happened */
    return -1;
}

//void rehash(edict_t* dict)
//{
//    edict_t tmp;
//    edict_init(&tmp, dict->space + EMME_DICT_SIZE);

//    edict_copy(&tmp, dict);

//    edict_erase(dict);

//    edict_init(dict, tmp.space);

//    edict_copy(dict, &tmp);

//    edict_erase(&tmp);
//}

void rehash(edict_t* dict)
{
    dict->space += EMME_DICT_SIZE;

    edict_t tmp;
    edict_init(&tmp, dict->space);
    tmp.collisions = dict->collisions;

    for (esize_t i=0; i<dict->space - EMME_DICT_SIZE; i++)
    {
        if (!dict->data[i]->empty)
        {
//            printf("reash {%s : %s} \n", dict->data[i]->str, eobj_to_cstr(dict->data[i]->obj));
            edict_insert(&tmp, dict->data[i]->str, dict->data[i]->obj);
        }
    }

    dict->space = dict->space - EMME_DICT_SIZE;

    edict_erase(dict);
    edict_init(dict, tmp.space);

    dict->collisions = tmp.collisions;
    edict_copy(dict, &tmp);

    edict_erase(&tmp);
}

eObject  edict_find(edict_t* dict, const char* key_string)
{
    //if (!dict)return NONE;
    int key = hash_it(key_string, dict->space);

    int query = query_dict(dict, key);

    if (query == 1)
    {
        if (strcmp(dict->data[key]->str, key_string) == 0)
            return dict->data[key]->obj;
        //else return NONE;
    }
    if (query == 0)
    {
//        fprintf(stderr, "{Key(%s):value} pair NOT in this map.\n", key_string);
    }
    return NONE;
}

void edict_copy(edict_t* dst, edict_t* src)
{
//    puts("start copy");
    for (esize_t i=0; i<src->space; i++)
    {
        if (src->data[i]->empty == false)
        {
//            char* str = eobj_to_cstr(src->data[i]->obj);
            //printf("copy {%s: %s} \n", src->data[i]->str, str);
            edict_insert(dst, src->data[i]->str, src->data[i]->obj);
//            free(str);
        }
    }
//    puts("end copy");
}

void edict_copy_all(edict_t* dst, edict_t* src)
{
    elog("start copy");
    for (esize_t i=0; i<src->space; i++)
    {
        if (src->data[i]->empty == false)
        {
            char* str = eobj_to_cstr(src->data[i]->obj);
            elog("copy {%s: %s} \n", src->data[i]->str, str);

            dst->data[i]->key = src->data[i]->key;
            dst->data[i]->obj = src->data[i]->obj;
            dst->data[i]->str = src->data[i]->str;
            dst->data[i]->empty = false;
            dst->size++;
            free(str);
        }
        else
        {
            dst->data[i]->empty = true;
        }
    }
    elog("end copy");
}

void edict_sortkeys(edict_t* d)
{
    esize_t size = d->size;
    dcell* tmp_cells = malloc(sizeof(dcell) * size);
    for (esize_t i=0, j=0; i<d->space; i++)
    {
        if (!d->data[i]->empty)
        {
            tmp_cells[j] = *d->data[i];
            j++;
        }
    }
    char* k; int j;
    for (esize_t i=0; i<size; i++)
    {
        dcell current = tmp_cells[i];

        k = current.str;
        j = i - 1;

        while (j >= 0 && strcmp(tmp_cells[j].str, k) > 0)
        {
            tmp_cells[j + 1] = tmp_cells[j];
            j = j - 1;
        }
        tmp_cells[j + 1] = current;
    }

    edict_erase(d);
    edict_init(d, size);

    for (esize_t i=0; i<size; i++)
    {
        *d->data[i] = tmp_cells[i];
    }
    d->size = size;
    free(tmp_cells);
}

int edict_sortvalues(edict_t* d)
{
    esize_t size = d->size;
    dcell* tmp_cells = emalloc(sizeof(dcell) * size);
    for (esize_t i=0, j=0; i<d->space; i++)
    {
        if (!d->data[i]->empty)
        {
            if (objisnumber(d->data[i]->obj))
            {
                tmp_cells[j] = *d->data[i];
                j++;
            }
            else return 0;
        }
    }
    double k; int j;
    for (esize_t i=0; i<size; i++)
    {
        dcell current = tmp_cells[i];

        k = OBJ_AS_NUM(current.obj);
        j = i - 1;

        while (j >= 0 && OBJ_AS_NUM(tmp_cells[j].obj) > k)
        {
            tmp_cells[j + 1] = tmp_cells[j];
            j = j - 1;
        }
        tmp_cells[j + 1] = current;
    }

    edict_erase(d);
    edict_init(d, size);

    for (esize_t i=0; i<size; i++)
    {
        *d->data[i] = tmp_cells[i];
    }
    d->size = size;
    free(tmp_cells);

    return 1;
}

void edict_print(edict_t* d)
{
    esize_t cnt = 0;
    printf("{");
    for (esize_t i=0; i<d->space; i++)
    {
        if (d->data[i]->empty == false)
        {
            cnt++;
            printf("%s", d->data[i]->str);
            printf("%s", ": ");
            char* strp = eobj_to_cstr(d->data[i]->obj);
            printf("%s", strp);
            free(strp); strp = NULL;

            if (cnt != d->size)
                printf("%s", ", ");
        }
    }
    printf("%s", "}");
}

void edict_to_cstring(edict_t* d, char* result)
{
    char buf[LARGE_BUFFER];
    memset(buf, 0, LARGE_BUFFER);
    esize_t cnt = 0;
    strcpy(buf, "{");
    for (esize_t i=0; i<d->space; i++)
    {
        if (d->data[i]->empty == false)
        {
            cnt++;
            strcat(buf, "'");
            strcat(buf, d->data[i]->str);
            strcat(buf, "'");
            strcat(buf, ": ");
            char* strp = eobj_to_cstr(d->data[i]->obj);
            strcat(buf, strp);
            free(strp); strp = NULL;

            if (cnt != d->size)
                strcat(buf, ", ");
        }
    }
    strcat(buf, "}");
    strcpy(result, buf);
}
