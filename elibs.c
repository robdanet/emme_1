#include <stdarg.h>
#include <ctype.h>
#include <time.h>
#include <math.h>

#if defined(_WIN32)
#include <windows.h>
#else
#include <dlfcn.h>
#endif

//#include "./modtest/mylib.h"
#include "elibs.h"


//
// STREAM LIB
//

int wr_iostream(eVm* vm)
{
    POP();
    PUSH(eobj_stream(estream_new()));
    return 1;
}

int wr_stream_open(eVm* vm)
{
    POP();
    eStream* s = OBJ_AS_STREAM(CPOP());

    s->fp = fopen(s->name, s->mode);
    if (!s->fp){ runtime_error(vm, "Error opening file"); }
    else
    {
        fseek(s->fp, 0L, SEEK_END);
        s->size = ftell(s->fp);
        rewind(s->fp);
    }
    return 1;
}

int wr_stream_close(eVm* vm)
{
    POP();
    eStream* s = OBJ_AS_STREAM(CPOP());
    sclose(s);
    return 1;
}

int wr_stream_setfile(eVm* vm)
{
    const char* filename = OBJ_AS_CSTR(POP());
    POP();
    eStream* s = OBJ_AS_STREAM(CPOP());
    s->name = filename;
    return 1;
}

int wr_stream_setmode(eVm* vm)
{
    const char* mode = OBJ_AS_CSTR(POP());
    POP();
    eStream* s = OBJ_AS_STREAM(CPOP());
    s->mode = mode;
    return 1;
}

int wr_stream_read(eVm* vm)
{
    POP();
    eStream* s = OBJ_AS_STREAM(CPOP());
    char* buf = emalloc(s->size + 1U);
    memset(buf, 0, s->size + 1U);
    esize_t check = fread(buf, sizeof(char), s->size, s->fp);
    rewind(s->fp);
    if(check != s->size){ runtime_error(vm, "Error reading file"); }
    PUSH(eobj_string(buf));
    free(buf);
    return 1;
}

int wr_stream_write(eVm* vm)
{
    const char* str = OBJ_AS_CSTR(POP());
    eStream* s = OBJ_AS_STREAM(CPOP());
    fputs(str, s->fp);
    if (ferror(s->fp))
    { runtime_error(vm, "Error writing to file"); }
    return 1;
}


//
// VECTOR LIB
//

int wr_push(eVm* vm)
{
    eObject value = POP();
    POP();
    eObject obj = CPOP();
    evector_push(OBJ_AS_VEC(obj), value);
    return 1;
}

int wr_pop(eVm* vm)
{
    POP();
    eVector* v = OBJ_AS_VEC(POP());
    eObject obj = evector_pop(v);
    PUSH(obj);
    return 1;
}

int wr_size(eVm* vm)
{
    POP();
    eObject v = CPOP();
    v = NUMBER(OBJ_AS_VEC(v)->size);
    PUSH(v);
    return 1;
}

int wr_splice(eVm* vm)
{
    eObject num = POP();
    eObject idx = POP();
    POP();
    eObject v = CPOP();
    eObject res = evector_splice(OBJ_AS_VEC(v),
                                 OBJ_AS_INT(idx),
                                 OBJ_AS_INT(num));
    PUSH(res);
    return 1;
}

//
// DICT LIB
//

int wr_dict_size(eVm* vm)
{
    POP();
    eObject d = CPOP();
    d = NUMBER(OBJ_AS_DICT(d)->size);
    PUSH(d);
    return 1;
}

int wr_insert(eVm* vm)
{
    if (vm->r1[0] == 1)
    {
        eObject value = POP();
        if (!objisdictionary(value))
        { runtime_error(vm, "Argument must be a dict"); }
        POP();
        eObject dict = CPOP();

        edict_copy(OBJ_AS_DICT(dict),OBJ_AS_DICT(value));
    }
    else if (vm->r1[0] == 2)
    {
        eObject value = POP();
        eObject key = POP();
        if (!objisstring(key))
        { runtime_error(vm, "Key is not a string"); }
        POP();
        eObject dict = CPOP();
        edict_insert(OBJ_AS_DICT(dict), OBJ_AS_CSTR(key), value);
    }
    else
    {
        runtime_error(vm, "Wrong number of arguments");
    }
    return 1;
}

int wr_sortk(eVm* vm)
{
    POP();
    eObject d = CPOP();
    edict_sortkeys(OBJ_AS_DICT(d));
    return 1;
}

int wr_sortv(eVm* vm)
{
    POP();
    eObject d = CPOP();
    int result = edict_sortvalues(OBJ_AS_DICT(d));
    if (!result)
        runtime_error(vm, "Dict contains non-numeric values and cannot be sorted");
    return 1;
}

int wr_first(eVm* vm)
{
    long index = OBJ_AS_NUM(POP());
    vm->sp--;
    edict_t* dict = OBJ_AS_DICT(CPOP());
    int count = -1;
    char* str = NULL;

    for (esize_t i=0; i<dict->space; i++)
    {
        if (!dict->data[i]->empty)
        {
            count++;
            if (count == index)
            {
                str =  dict->data[i]->str;
                break;
            }
        }
    }
    PUSH(NEW_STRING(str));
    return 1;
}

int wr_second(eVm* vm)
{
    long index = OBJ_AS_NUM(POP());
    vm->sp--;
    edict_t* dict = OBJ_AS_DICT(CPOP());
    int count = -1;
    eObject obj;
    for (esize_t i=0; i<dict->space; i++)
    {
        if (!dict->data[i]->empty)
        {
            count++;
            if (count == index)
            {
                obj =  dict->data[i]->obj;
                break;
            }
        }
    }
    PUSH(obj);
    return 1;
}


//
// STRING LIB
//

int wr_string_size(eVm* vm)
{
    POP();
    eObject d = CPOP();
    d = NUMBER(OBJ_AS_STR(d)->size);
    PUSH(d);
    return 1;
}

int wr_append(eVm* vm)
{
    eObject arg = POP();
    if (!objisstring(arg))
    { runtime_error(vm, "append:Argument must be a string"); }
    POP();
    eString* estr = OBJ_AS_STR(CPOP());
    estring_append(estr, OBJ_AS_STR(arg)->text[0]);

    return 1;
}

static bool contains_intspec(const char* fmt)
{
    return strpbrk(fmt, "dioxXuc");
}

static bool contains_floatspec(const char* fmt)
{
    return strpbrk(fmt, "feEgG");
}

static void charcat(char* buf, int c)
{
    char tb[2];
    sprintf(tb,"%c",c);
    strcat(buf, tb);
}

/*
    :d :i     Decimal signed integer.
    :o	      Octal integer.
    :x :X     Hex integer.
    :u	      Unsigned integer.
    :c	      Character.
    :s	      String.
    :f	      double
    :e :E     double.
    :g :G     double.
    :p        pointer.
*/

int wr_format(eVm* vm)
{
    eVector* v = stack_getargs(vm, 0);
    POP();
    eObject efmt = CPOP();
    const char* fmt = OBJ_AS_CSTR(efmt);
    const char* p = fmt;
    char* buf = emalloc(LARGE_BUFFER);
    strcpy(buf,"");
    while (*p != '\0')
    {
        if (p[0] == '%')
        {
            switch(p[1])
            {
            default:
            {
                eObject obj = evector_pop(v);
                char* str = eobj_to_cstr(obj);
                strcat(buf, str);
                free(str);
            }
                break;
            }
        }
        else if (p[0] == '{' && p[1] == '}')
        {
            p++;
            eObject obj = evector_pop(v);
            char* str = eobj_to_cstr(obj);
            strcat(buf,str);
            free(str);
        }
        else if (p[0] == '{')
        {
            p++;
            int i = 0;
            while (p[i] != '}')i++;
            char* fmt = copy_buffer(p, i); elog("%s", fmt);
            p += i;

            int j = 0;
            char* fmtp = fmt;
            while (fmtp[j]){ if (fmtp[j] == ':')fmtp[j] = '%'; j++; }

            char locbuf[LARGE_BUFFER];
            eObject obj = evector_pop(v);
            switch (obj.type)
            {
            case ESTRING:
            {
                char* str = OBJ_AS_CSTR(obj);
                sprintf(locbuf, fmt, str);
                strcat(buf, locbuf);
                free(str);
            }
                break;
            case ENUMBER:
            {
                if (contains_intspec(fmt))
                {
                    long num = OBJ_AS_INT(obj);
                    sprintf(locbuf, fmt, num);
                    strcat(buf, locbuf);
                }
                else if (contains_floatspec(fmt))
                {
                    double num = OBJ_AS_NUM(obj);
                    sprintf(locbuf, fmt, num);
                    strcat(buf, locbuf);
                }
                else
                {
                    runtime_error(vm, "Wrong format specifier");
                }
            }
                break;
            default:// nothing
                break;
            }
        }
        else
        {
            charcat(buf, *p);
        }
        p++;
    }
    PUSH(eobj_string(buf));
    free(buf);
    return 1;
}


int wr_indexof(eVm* vm)
{
    eObject pos = NIL;
    if (vm->r1[0] == 2)
    {
        pos = POP();
        if (!objisnumber(pos))
        {
            fmt_error(vm, "Function 'indexof': expect 'string' and 'number'. Got '%s'.\n",
                      eobj_type(pos));
            longjmp(go, RuntimeError);
        }
    }
    eObject substr = POP();
    if (!objisstring(substr))
    {
        fmt_error(vm, "Function 'indexof': expect 'string' and 'number'. Got '%s'.\n",
                  eobj_type(substr));
        longjmp(go, RuntimeError);
    }
    const char* csubstr = OBJ_AS_CSTR(substr);
    long int cpos = OBJ_AS_INT(pos);
    POP();
    eObject estr = CPOP();

    const char* str = OBJ_AS_CSTR(estr);
    str = vm->r1[0] == 2 ? (str + cpos) : str;

    if (str != NULL && cpos >= 0)
    {
        char* ptr = strstr(str, csubstr);
        int idx = ptr - str;
        if (idx >= 0)
        {
            PUSH(NUMBER(idx + cpos));
            return 1;
        }
        else
        {
            PUSH(NIL);
        }
    }
    else
    {
        PUSH(NIL);
    }
    return 0;
}

int wr_isspace(eVm* vm)
{
    POP();
    const char* s = OBJ_AS_CSTR(CPOP());
    while (*s)
    {
        if (isspace((int)*s) == 0)
        {
            PUSH(BOOL(false));
            return;
        }
        s++;
    }
    PUSH(BOOL(true));
    return 1;
}

int wr_isdigit(eVm* vm)
{
    POP();
    const char* s = OBJ_AS_CSTR(CPOP());
    while (*s)
    {
        if (isdigit((int)*s) == 0)
        {
            PUSH(BOOL(false));
            return 1;
        }
        s++;
    }
    PUSH(BOOL(true));
    return 1;
}

int wr_toupper(eVm* vm)
{
    eString* result = estring_new("");

    POP();
    const char* s = OBJ_AS_CSTR(CPOP());
    while (*s)
    {
        estring_append(result,toupper((int)*s));
        s++;
    }
    PUSH(OBJ_STRING(result));
    return 1;
}

int wr_tolower(eVm* vm)
{
    eString* result = estring_new("");

    POP();
    const char* s = OBJ_AS_CSTR(CPOP());
    while (*s)
    {
        estring_append(result,tolower((int)*s));
        s++;
    }
    PUSH(OBJ_STRING(result));
    return 1;
}

int wr_toascii(eVm* vm)
{
    int result;

    POP();
    const char* s = OBJ_AS_CSTR(CPOP());
    if (strlen(s) > 1){"error";}
    result = (int)s[0];
    PUSH(NUMBER(result));
    return 1;
}

//
// NUMBER LIB
//

int wr_num_ctor(eVm* vm)
{
    eObject num = POP();
    POP();
    PUSH(NUMBER(eobj_to_cnum(vm, num)));
    return 1;
}

int wr_add(eVm* vm)
{
    eObject r = POP();
    POP();
    eObject l = CPOP();
    PUSH(NUMBER(OBJ_AS_NUM(l) + OBJ_AS_NUM(r)));
    return 1;
}

int wr_sub(eVm* vm)
{
    eObject r =  (POP());
    POP();
    eObject l = CPOP();
    PUSH(NUMBER(OBJ_AS_NUM(l) - OBJ_AS_NUM(r)));
    return 1;
}

int wr_mul(eVm* vm)
{
    eObject r = POP();
    POP();
    eObject l = POP();
    PUSH(NUMBER(OBJ_AS_NUM(l) * OBJ_AS_NUM(r)));
    return 1;
}

//
// TIME LIB
//

int wr_time_ctor(eVm* vm)
{
    //POP();
    PUSH(eobj_time(etime_new()));
    return 1;
}

/*

  Time 	            In nanoseconds
  1 second 	        1.000.000.000 ns
  1 milli second 	1.000.000 ns
  1 micro second 	1.000 ns

*/

int wr_nanosec(eVm* vm)
{
    POP();
    CPOP();
    PUSH(NUMBER(eraw_time()));
    return 1;
}

int wr_elapsed(eVm* vm)
{
    POP();
    CPOP();
    PUSH(NUMBER(((float)clock()/CLOCKS_PER_SEC)));
    return 1;
}

int wr_seconds(eVm* vm)
{
    POP();
    CPOP();
    eTime* t = etime_new();
    time(&t->rawtime);
    t->timeinfo = localtime(&t->rawtime);
    PUSH(NUMBER(t->timeinfo->tm_sec));
    return 1;
}

int wr_minutes(eVm* vm)
{
    POP();
    CPOP();
    eTime* t = etime_new();
    time(&t->rawtime);
    t->timeinfo = localtime(&t->rawtime);
    PUSH(NUMBER(t->timeinfo->tm_min));
    return 1;
}

int wr_hour(eVm* vm)
{
    POP();
    CPOP();
    eTime* t = etime_new();
    time(&t->rawtime);
    t->timeinfo = localtime(&t->rawtime);
    PUSH(NUMBER(t->timeinfo->tm_hour));
    return 1;
}

int wr_strftime(eVm* vm)
{
    const char* s = OBJ_AS_CSTR(POP());
    POP();

    eTime* t = etime_new();

    time(&t->rawtime);
    t->timeinfo = localtime(&t->rawtime);
    char buffer [SMALL_BUFFER];
    strftime (buffer, SMALL_BUFFER, s, t->timeinfo);
    PUSH(NEW_STRING(buffer));
    return 1;
}

//
// MATH LIB
//

int wr_math_ctor(eVm* vm)
{
    eMath* math = emath_new();
    PUSH(eobj_math(math));
    return 1;
}

int wr_sin(eVm* vm)
{

}

int wr_sqrt(eVm* vm)
{
    eObject arg = POP();
    POP();
    //POP();
    PUSH(NUMBER(sqrt(OBJ_AS_NUM(arg))));

    return 1;
}

int wr_pow(eVm* vm)
{
    eObject e = POP();
    eObject n = POP();
    POP();
    //POP();
    PUSH(NUMBER(pow(eobj_to_cnum(vm, n), eobj_to_cnum(vm, e))));

    return 1;
}

//
// library's helpers functions
//

int scan_argvec(eVm* vm, eVector* argv, char* fmt, ...)
{
    char* p = fmt;

    va_list args;
    va_start(args, fmt);

    while (*p != '\0')
    {
        switch(p[0]){
        case 'i':
        {
            p++;
            long int* val = va_arg(args, long int*);
            *val = OBJ_AS_INT(evector_pop(argv));
        }
            break;
        case 'f':
        {
            p++;
            double* val = va_arg(args, double*);
            *val = OBJ_AS_NUM(evector_pop(argv));
        }
            break;
        case 's':
        {
            p++;
            char* val = va_arg(args, char*);
            strcpy(val, OBJ_AS_CSTR(evector_pop(argv)));
        }
            break;
        case 'v':
            break;
        case 'd':
            break;
        }
    }

     va_end(args);
     return 1;
}

eVector* stack_getargs(eVm* vm, int leaven)
{
    /* collect args from the stack */
    eVector* v = evector_new();
    ebyte_t argc = vm->r1[0] - leaven;
    for (int i=argc-1; i>=0; i--)
        evector_push(v, POP());

    return v;
}

int wr_dis(eVm* vm)
{
    eObject efunc = edict_find(&vm->globals, funcname(POP()));
    eFunction* f = OBJ_AS_FUNC(efunc);
    dis_bcode(f->code, OBJ_AS_CSTR(f->name));
    POP();

    return 1;
}

int wr_tochar(eVm* vm)
{
    char s[2];
    eObject arg = POP();
    POP();
    if (!obj_isnumeric(arg))
    { runtime_error(vm, "char(): Argument must be a number"); }
    if (objisnumber(arg))
    {
        s[0] = (int)OBJ_AS_INT(arg);
    }
    else if (objisstring(arg))
    {
        char* str = OBJ_AS_CSTR(arg);
        s[0] = (long int)strtof(str,NULL);
    }

    s[1] = '\0';
    PUSH(NEW_STRING(s));

    return 1;
}

int wr_toint(eVm* vm)
{
    long int val;
    eObject arg = POP();
    POP();
    if (!obj_isnumeric(arg))
    { runtime_error(vm, "int(): Argument must be a number"); }
    if (objisnumber(arg))
    {
        val = OBJ_AS_INT(arg);
    }
    else if (objisstring(arg))
    {
        char* str = OBJ_AS_CSTR(arg);
        val = (long int)strtof(str,NULL);
    }
    PUSH(NUMBER(val));

    return 1;
}

int wr_tofloat(eVm* vm)
{
    double val;
    eObject arg = POP();
    POP();
    if (!obj_isnumeric(arg))
    { runtime_error(vm, "float(): Argument must be a number"); }
    if (objisnumber(arg))
    {
        val = OBJ_AS_NUM(arg);
    }
    else if (objisstring(arg))
    {
        char* str = OBJ_AS_CSTR(arg);
        val = strtod(str,NULL);
    }
    PUSH(NUMBER(val));

    return 1;
}


// variable arguments test
int wr_test_print(eVm* vm)
{
    int a, b;
    char buf[1024];

    eVector* v = stack_getargs(vm, 0);
    POP();



//    scan_argvec(vm, v, "~", retvals);

    for (esize_t i=v->size - 1; signedi(i)>=0; i--)
        print_object(v->data[i]);

    //PUSH(NONE);

}

void fmt_error(eVm* vm, char* fmt, ...)
{
    char* p = fmt;

    va_list args;
    va_start(args, fmt);

    fprintf(stderr, "[%s][Line %lu]\n", vm->filename,
            vm->bcode->lines[line_index(vm)]);
    while (*p != '\0')
    {
        if (p[0] == '%')
        {
            switch(p[1])
            {
            case '%':
            {
                p++;
                eObject obj = va_arg(args, eObject);
                char* str = eobj_to_cstr(obj);
                printf("%s", str);
                free(str);
            }
                break;
            case 's':
            {
                p++;
                char* str = va_arg(args, char*);
                printf("%s", str);
            }
                break;
            case 'l':
            {
                p++;
                long val = va_arg(args, long);
                printf("%lu", val);
            }
                break;
            case 'd':
            {
                p++;
                int val = va_arg(args, int);
                printf("%d", val);
            }
                break;
            case 'p':
            {
                p++;
                void* ptr = va_arg(args, void*);
                printf("%p", ptr);
            }
                break;
            default:
                break;
            }
        }
        else
        {
            putchar(*p);
        }
        p++;
    }
    va_end(args);
}

//
// API's FUNCTIONS
//

int wr_printf(eVm *vm)
{
    /* collect args and pop function */
    eVector* v = stack_getargs(vm, 1);
    eObject efmt = POP();
    POP();

    const char* p = OBJ_AS_CSTR(efmt);
    while (*p != '\0')
    {
        if (p[0] == '%')
        {
            switch(p[1])
            {
            default:
            {
                eObject obj = evector_pop(v);
                char* str = eobj_to_cstr(obj);
                printf("%s", str);
                free(str);
            }
                break;
            }
        }
        else if (p[0] == '{' && p[1] == '}')
        {
            p++;
            eObject obj = evector_pop(v);
            char* str = eobj_to_cstr(obj);
            printf("%s", str);
            free(str);
        }
        else if (p[0] == '{')
        {
            p++;
            int i = 0;
            while (p[i] != '}')i++;
            char* fmt = copy_buffer(p, i); elog("%s", fmt);
            p += i;

            int j = 0;
            char* fmtp = fmt;
            while (fmtp[j]){ if (fmtp[j] == ':')fmtp[j] = '%'; j++; }

            eObject obj = evector_pop(v);
            switch (obj.type)
            {
            case ESTRING:
            {
                char* str = OBJ_AS_CSTR(obj);
                printf(fmt, str);
            }
                break;
            case ENUMBER:
            {
                if (contains_intspec(fmt))
                {
                    long num = OBJ_AS_INT(obj);
                    printf(fmt, num);
                }
                else if (contains_floatspec(fmt))
                {
                    double num = OBJ_AS_NUM(obj);
                    printf(fmt, num);
                }
                else
                {
                    runtime_error(vm, "Wrong format specifier");
                }
            }
                break;
            default:// nothing
                break;
            }
            free(fmt);
        }
        else
        {
            putchar(*p);
        }
        p++;
    }
    return 1;
}

int wr_eval(eVm* vm)
{
    eObject arg = POP();
    char* str = eobj_to_cstr(arg);
    eval_impl(str, "<stdio>");
    free(str);
    POP();
    return 1;
}

int wr_exit(eVm* vm)
{
    vm->run = false;
    POP();
    PUSH(NIL);
    return 1;
}

int wr_len(eVm* vm)
{
    eObject v = POP();
    POP();
    v = NUMBER(OBJ_AS_VEC(v)->size);
    PUSH(v);
    return 1;
}

int wr_make_iter(eVm* vm)
{
    eObject* ary = emalloc(sizeof(eObject));
    *ary = POP();
    eObject objit = eobj_iterator(make_iter(ary, has_next));
    POP();
    PUSH(objit);
    return 1;
}

int wr_iter(eVm* vm)
{
    array_iterator* it = OBJ_AS_ITER(POP());
    POP();

    if (it->has_next(it->ary, it))
    {
        PUSH(iter(it));
    }
    else
    {
        PUSH(NIL);
    }
    return 1;
}

int wr_next(eVm* vm)
{
    POP();
    array_iterator* it = OBJ_AS_ITER(POP());
    PUSH(it->next(it));
    return 1;
}

int wr_has_next(eVm* vm)
{
    POP();
    if (!objisiterator(TOS))
    {
        fprintf(stderr, "Type '%s' has no method named 'has_next'.\n", eobj_type(TOS));
        exit(EXIT_FAILURE);/* for now */
    }
    array_iterator* it = OBJ_AS_ITER(POP());
    if (it->has_next(it->ary, it))
    {
        PUSH(BOOL(true));
    }
    else
    {
        PUSH(BOOL(false));
    }
    return 1;
}


int wr_typename(eVm* vm)
{
    eObject obj = POP();
    POP();
    char* tname = eobj_type(obj);
    eObject tn = NEW_STRING(tname);
    PUSH(tn);

    return 1;
}

int wr_instanceof(eVm* vm)
{
    eObject obj = POP();
    if (!objisclass(obj))
        runtime_error(vm, "instanceof: argument is not an instance");
    POP();
    PUSH(OBJ_AS_CLASS(obj)->name);

    return 1;
}

int wr_range(eVm* vm)
{
    eObject e = POP();
    eObject s;
    if (vm->r1[0] == 2)
        s = POP();
    else
        s = NUMBER(0);
    POP();
    eVector* v = evector_new();
    for (long i = OBJ_AS_INT(s); i<OBJ_AS_INT(e); i++)
        evector_push(v, NUMBER(i));
    PUSH(eobj_vector(v));
    return 1;
}

int wr_input(eVm* vm)
{
    const char* str = OBJ_AS_CSTR(POP());
    printf("%s", str);
    POP();
    char buf[LARGE_BUFFER];
    if (!fgets(buf, sizeof(buf), stdin))
    {
        PUSH(NIL);
        return 0;
    }
    PUSH(eval_str(buf));
    return 1;
}

//
// library creation and loaders
//
void init_library (char* name,
                  struct LibManager* manager,
                  struct Library* library,
                  struct LibMethod* methods,
                  struct LibField* fields)
{
    library->name = name;
    library->methods = methods;
    library->fields = fields;
    manager->libraries[manager->count++] = library;
    elog("init %s module\n", name);
}

// export some internal api to extensions in windows
#if defined(_WIN32)

typedef void (*_SetPopCallback)(popcallback);

typedef void (*_SetPushCallback)(pushcallback);

typedef void (*_SetInitlibCallback)(initlibcallback);

typedef double(*_SetEObjToCNumber)(eobj_to_cnumber_callback);

static void init_windows_callbacks(HINSTANCE library)
{
    _SetPopCallback popcb = (_SetPopCallback)GetProcAddress(library, "SetPopCallback");
    popcb(pop);

    _SetPushCallback pushcb = (_SetPushCallback)GetProcAddress(library, "SetPushCallback");
    pushcb(push);

    _SetInitlibCallback ilcb = (_SetInitlibCallback)GetProcAddress(library, "SetLibinitCallback");
    ilcb(init_library);

    _SetEObjToCNumber eotcn = (_SetEObjToCNumber)GetProcAddress(library, "SetEObjToCNumber");
    eotcn(eobj_to_cnum);

    printf("Window Callbacks set\n");
}
#endif


static Library* lib_lookup(eVm* vm, const char* name)
{
    Library* library = NULL;

    for (esize_t i=0; i<vm->lib_manager.count; i++)
    {
        if (strcmp(vm->lib_manager.libraries[i]->name, name) == 0)
        {
            library = vm->lib_manager.libraries[i];

            return library;
        }
    }
    return (NULL);
}


#if defined(_WIN32)
#define LIBTYPE HINSTANCE
#define LIBEXT ".dll"
#define OPENLIB(libname) LoadLibrary(libname)
#define LIBFUNC(lib, fn) GetProcAddress((lib), (fn))
#define LIBERROR GetLastError
#else
#define LIBTYPE void*
#define LIBEXT ".so"
#define OPENLIB(libname) dlopen((libname), RTLD_LAZY)
#define LIBFUNC(lib, fn) dlsym((lib), (fn))
#define LIBERROR dlerror
#endif


int call_open_func(eVm* vm, const char *libname, const char* name)
{
    LIBTYPE h = OPENLIB(libname);
    if (h == NULL)
    {
        fprintf(stderr, "Error loading %s\n:%s", libname, LIBERROR());
        return 0;
    }
    else puts("Module open");
#if defined(_WIN32)
    init_windows_callbacks(h);
#endif
    char symbol[32] = { '\0' };
    snprintf(symbol, 31, "open%s", name);
    printf("loading  %s\n", symbol);
    native_fn f = (native_fn) LIBFUNC(h, symbol);
    if (f == NULL)
    {
        fprintf(stderr, "Error loading %s\n:%s", symbol, LIBERROR());
        return 0;
    }
    else puts("Module init");
    f(vm);
    return 1;
}


int load_dynamic_ctor(eVm* vm, char* name)
{
    Library* library = lib_lookup(vm, name);

    if (!library)
    {
        char path[LARGE_BUFFER] = EMME_LIB_PATH;
        strcat(path, name);
        strcat(path, LIBEXT);

        if (!call_open_func(vm, path, name))
        {
            printf("Failed to load module\n");
            return 0;
        }
    }

    for (esize_t i=0; i<vm->lib_manager.count; i++)
        if (strcmp(name, vm->lib_manager.libraries[i]->name) == 0)
            library = vm->lib_manager.libraries[i];

    if (!library)
    {
        printf("error loading library %s\n", name);
        return 0;
    }

    eClass* c = make_type(name, library,  (LibMethod*)library->methods);

    if (!c){ printf("Error building imported type %s\n", name); return 0; }

    /* if the first method is a ctor call it */
    if (strcmp(library->methods[0].name, name) == 0)
    {
        PUSH(OBJ_CLASS(c));
        library->methods[0].fp(vm);
    }
    else
    {
        PUSH(OBJ_CLASS(c));
    }
    return 1;

}

static void load_classdict(eClass* c, Library* library)
{
    edict_init(&c->env->fields, EMME_DICT_SIZE);

    const char* name;
    for (esize_t i=0; (name = library->methods[i].name); i++)
    {
        eFunction* f = efunction_new(name, NULL);
        f->fp = library->methods[i].fp;
        f->is_native = true;
        eObject func = ((eObject){ EFUNCTION, .value.object=f });
        edict_insert(&c->env->fields, (char*)name, func);
    }
    if (library->fields)
    for (esize_t i=0; (name = library->fields[i].name); i++)
    {
        edict_insert(&c->env->fields, (char*)name, library->fields[i].value);
    }
}

eClass* make_type(char* name, Library* library, LibMethod* methods)
{
    elog("make '%s' type\n", name);
    eClass* type = (eClass*)egcobject_new(ECLASS, sizeof(eClass));
    type->name = eobj_string(name);
    type->is_c = true;
    type->has_init = false;
    type->is_inst = true;
    type->env = malloc(sizeof(ClassEnv));
    type->code = NULL;
    library->name = name;
    library->methods = methods;
    load_classdict(type, library);
    return type;
}

// libraries metadata

static struct Library stream_lib;
static LibMethod stream_methods[] =
{
   {"iostream", wr_iostream},
   {"open", wr_stream_open},
   {"close", wr_stream_close},
   {"setfile", wr_stream_setfile},
   {"setmode", wr_stream_setmode},
   {"read", wr_stream_read},
   {"write", wr_stream_write},
   {NULL, NULL}
};

//-------------------------------------------------

static Library vector_lib;
static LibMethod vector_methods[] =
{
    {"size", wr_size},
    {"push", wr_push},
    {"pop", wr_pop},
    {"splice", wr_splice},
    {NULL, NULL}
};

static Library dict_lib;
static LibMethod dict_methods[] =
{
    {"size", wr_dict_size},
    {"insert", wr_insert},
    {"sortk", wr_sortk},
    {"sortv", wr_sortv},
    {"first", wr_first},
    {"second", wr_second},
    {NULL, NULL}
};

static Library string_lib;
static LibMethod string_methods[] =
{
    {"size", wr_string_size},
    {"append", wr_append},
    {"format", wr_format},
    {"indexof", wr_indexof},
    {"isspace", wr_isspace},
    {"isdigit", wr_isdigit},
    {"toupper", wr_toupper},
    {"tolower", wr_tolower},
    {"toascii", wr_toascii},
    {NULL, NULL}
};

static Library number_lib;
static LibMethod number_methods[] =
{
    {"Num", wr_num_ctor},
    {"__add", wr_add},
    {"__sub", wr_sub},
    {"__mul", wr_mul},
    {NULL, NULL}
};

static Library time_lib;
static LibMethod time_methods[] =
{
//    {"time", wr_time_ctor},
    {"elapsed", wr_elapsed},
    {"nanosec", wr_nanosec},
    {"seconds", wr_seconds},
    {"minutes", wr_minutes},
    {"hour", wr_hour},
    {"strftime", wr_strftime},
    {NULL, NULL}
};

static LibField time_fields[] =
{
//    {"etime", NIL},
    {0, 0}
};

static Library math_lib;
static LibMethod math_methods[] =
{
//    {"math",wr_math_ctor, 0},
    {"pow", wr_pow},
    {"sqrt", wr_sqrt},
    {"sin", wr_sin},
    {NULL, NULL}
};

static LibField math_fields[] =
{
    {"PI", NUMBER(3.141592653589793)},
    {0, 0}
};

static Library iterator_lib;
static LibMethod iterator_methods[] =
{
    {"has_next", wr_has_next},
    {"next", wr_next},
    {NULL, NULL}
};

//----------------------------------------
static BuiltinFunc bifuncs[] =
{
    {"len", wr_len},
    {"range", wr_range},
    {"make_iter", wr_make_iter},
    {"iter", wr_iter},
    {"type", wr_typename},
    {"instanceof", wr_instanceof},
    {"printf", wr_printf},
    {"eval", wr_eval},
    {"exit", wr_exit},
    {"Num", wr_num_ctor},
    {"input", wr_input},
    {"stream", wr_iostream},
    {"dis", wr_dis},
    {"printv", wr_test_print},
    {"char", wr_tochar},
    {"int", wr_toint},
    {"float", wr_tofloat},
    {NULL, NULL}
};



static eClass* num_ptr = NULL;
static eClass* vec_ptr = NULL;
static eClass* dict_ptr = NULL;
static eClass* string_ptr = NULL;
static eClass* stream_ptr = NULL;

void make_bltins_type()
{
    dict_ptr = make_type("dict", &dict_lib, (LibMethod*)dict_methods);
    vec_ptr = make_type("vector", &vector_lib, (LibMethod*)vector_methods);
    num_ptr = make_type("number", &number_lib, (LibMethod*)number_methods);
    string_ptr = make_type("string", &string_lib, (LibMethod*)string_methods);
    stream_ptr = make_type("stream", &stream_lib, (LibMethod*)stream_methods);
}

static void load_bifunc(eVm* vm, const BuiltinFunc* bf)
{
    for (esize_t i=0; bf[i].name; i++)
    {
//        const char* name = bf[i].name;
//        eFunction* f = efunction_new(name, NULL);
//        f->fp = bf[i].fp;
//        f->is_native = true;
//        eObject func = ((eObject){ EFUNCTION, .value.object=f });
//        edict_insert(&vm->globals, (char*)name, func);
//        printf("load %s function\n", name);
        loadfn(vm, bf[i].name, bf[i].fp);
    }
}

void loadfn(eVm* vm, const char* name, native_fn fp)
{
    eFunction* f = efunction_new(name, NULL);
    f->fp = fp;
    f->is_native = true;
    eObject func = ((eObject){ EFUNCTION, .value.object=f });
    edict_insert(&vm->globals, (char*)name, func);
}



void init_libs(eVm* vm)
{

//    store_so(vm, "./modtest/libtest_add.so", "wr_add_test", "addtest");

    load_bifunc(vm, bifuncs);
    vm->lib_manager.count = 0;

    init_library("time",&vm->lib_manager, &time_lib, time_methods, 0);
    init_library("math",&vm->lib_manager, &math_lib, math_methods, math_fields);
    init_library("iterator", &vm->lib_manager, &iterator_lib, iterator_methods, 0);
//    init_library("stream", &vm->lib_manager, &stream_lib, stream_methods, 0);

}

eClass* get_builtin_instance(ValueType type)
{
    eClass* the_class = NULL;
    switch(type){
    case ENUMBER:
        the_class = num_ptr;
        break;
    case EVECTOR:
        the_class = vec_ptr;
        break;
    case EDICT:
        the_class = dict_ptr;
        break;
    case ESTRING:
        the_class = string_ptr;
        break;
    case ESTREAM:
        the_class = stream_ptr;
        break;
    default:
        break;
    }
    return the_class;
}
