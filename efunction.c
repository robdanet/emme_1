#include "efunction.h"


eFunction* efunction_new(const char* name, eBytecode* bytecode)
{
    eFunction* f = (eFunction*)egcobject_new(EFUNCTION, sizeof(eFunction));
    f->name = eobj_string(name);
    f->argc = 0;
    f->dargc = 0;
    f->outer = NULL;
    f->cl_env = NULL;
    f->scope = 0;
    f->has_vargs = false;
    f->is_closure = false;
    f->is_native = false;
    f->is_method = false;
    f->is_child = false;
    f->fp = NULL;
    f->is_ctor = false;
    f->true_argc = 0;
//    f->klass = NIL;

    memset(f->skip_addr, 0, FUNC_ARITY);
    f->code = bytecode;

    f->let_count = 0;
    f->let_depth = 0;

    return f;
}

void efunction_delete(eFunction* f)
{
    free(f);
    f = NULL;
}

eObject eobj_function(const char* name, eBytecode* bytecode)
{
    eObject obj;
    obj.type = EFUNCTION;
    obj.value.object = efunction_new(name, bytecode);

    return obj;
}


eFunction* efunction_clone(eFunction* f)
{
    eFunction* fclone = (eFunction*)egcobject_new(EFUNCTION, sizeof(eFunction));
    fclone->scope = f->scope;
    fclone->argc = f->argc;
    fclone->dargc = f->dargc;
    fclone->name = f->name;
    fclone->vargname = f->vargname;
    fclone->has_vargs = f->has_vargs;
    fclone->is_closure = f->is_closure;
    fclone->is_native = f->is_native;
    fclone->is_ctor = f->is_ctor;
    fclone->is_method = f->is_method;
    fclone->is_child = f->is_child;
    fclone->true_argc = f->true_argc;
    fclone->outer = f->outer;
    fclone->fp = f->fp;
    fclone->cl_env = f->cl_env;
    fclone->klass = f->klass;
    fclone->lself = f->lself;
    fclone->code = f->code;memcpy(fclone->skip_addr, f->skip_addr, FUNC_ARITY);
    for (esize_t i=0; i<FUNC_ARITY; i++)
//        fclone->skip_addr[i] =
        printf("%d-%d\n",fclone->skip_addr[i],i);


    return fclone;
}

ClosureFunc* closuref_new(eObject func, ClosureEnv* closure_env)
{
    ClosureFunc* cf = (ClosureFunc*)egcobject_new(ECLOSURE, sizeof(ClosureFunc));
    cf->f = efunction_clone(OBJ_AS_FUNC(func));
    cf->cl_env = closure_env;

    return cf;
}

void closuref_delete(ClosureFunc* cl)
{
    edict_erase(&cl->cl_env->upvalues);
    free(cl->cl_env);
    free(cl);
}

eObject eobj_closure_func(eObject func, ClosureEnv* closure_env)
{
    eObject obj;
    obj.type = ECLOSURE;
    obj.value.object = closuref_new(func, closure_env);

    return obj;
}


eClass* eclass_new(const char* name, eBytecode* bytecode)
{
    eClass* c = (eClass*)egcobject_new(ECLASS, sizeof(eClass));
    c->name = eobj_string(name);
    c->env = malloc(sizeof(ClassEnv));
    c->parent = NULL;
    c->is_c = false;
    c->has_init = false;
    c->is_inst = false;
    c->has_parent = false;
    c->is_parent = false;
    c->has_innerc = false;
    c->is_innerc = false;
    edict_init(&c->env->fields, EMME_DICT_SIZE);
    c->code = bytecode;

    return c;
}

void eclass_delete(eClass* klass)
{
    edict_erase(&klass->env->fields);
    free(klass->env);
    free(klass);
}

eObject eobj_class(const char* name, eBytecode* bytecode)
{
    eObject obj;
    obj.type = ECLASS;
    obj.value.object = eclass_new(name, bytecode);

    return obj;
}

eClass* eclass_clone(eClass* c)
{
    eClass* clone = (eClass*)egcobject_new(ECLASS, sizeof(eClass));
    clone->name = c->name;
    clone->is_c = c->is_c;
    clone->has_init = c->has_init;
    clone->code = c->code;
    clone->env = NULL;
    clone->parent = c->parent;
    clone->is_inst = false;
    clone->has_parent = c->has_parent;
    clone->is_parent = c->is_parent;
    clone->child = c->child;
    return clone;
}
