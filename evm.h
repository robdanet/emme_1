#ifndef EVM
#define EVM

#include "emme.h"


#define FETCH() (*vm->ip++)
#define POP() pop(vm)
#define CPOP() cpop(vm)
#define TOS tos(vm)
#define TOS1 tos1(vm)
#define TOS2 tos2(vm)
#define TOSN(n) tosN(vm,n)
#define PUSH(v) push(vm,v)
#define CPUSH(v) cpush(vm,v)

#define fn_code(f) OBJ_AS_FUNC(f)->code
#define fn_frame(f) OBJ_AS_FUNC(func)->frame

typedef struct LibManager
{
    esize_t count;
    struct Library* libraries[SMALL_BUFFER];
}LibManager;

typedef struct LoopBlock
{
    int start;
    int end;
}LoopBlock;

typedef struct eVm
{
    eObject stack[STACK_MAX];
    eObject cstack[STACK_MAX];
    eFrame call_stack[CALL_STACK_MAX];
    eObject callers_stack[CALL_STACK_MAX];
    LoopBlock block_stack[BLOCK_STACK_MAX];
    eFunction* last_call;
    eFrame* fp;
    eBytecode* bcode;
    eObject* sp;
    eObject* csp;
    LoopBlock* bsp;
    eObject* cp;
    ebyte_t* ip;
    edict_t globals;
    LibManager lib_manager;
    ebyte_t r1[1];
    bool run;
    const char* filename;
}eVm;

int line_index(eVm* vm);

//void init_library(const char* name,
//                  LibManager* manager,
//                  Library* library,
//                  LibMethod* methods,
//                  LibField* fields);

double eobj_to_cnum(eVm *vm, eObject obj);


bool is_inside(eObject * stack, int size, eObject * pointer);

extern eObject pop(eVm* vm);
extern void push(eVm* vm, eObject value);

eObject cpop(eVm* vm);

void cpush(eVm* vm, eObject value);

eObject tos(eVm* vm);

eObject tos1(eVm* vm);

eObject tos2(eVm* vm);

eObject tosN(eVm* vm, int n);

void repl_auto_print(eVm* vm);//main
//void make_bltins_types();
//void init_libs(eVm* vm);
void evm_init(eVm* vm, eBytecode* bcode, const char *filename);
void evm_delete(eVm* vm);
ErrorType runtime_error(eVm* vm, const char* msg);
ErrorType eval_impl(const char* source, const char* filename);
eObject eval_str(const char* source);
ErrorType run_bcode(eVm* vm);

#endif // EVM
