#include "eobject.h"
#include "emme.h"


#define GC_THRESOLD 80



//double eobj_to_cnum(eObject obj)
//{
//    if (objisnumber(obj))
//        return OBJ_AS_FLOAT(obj);
//    if (objisstring(obj))
//    {
//        char* str = OBJ_AS_CSTR(obj);
//        double num = atof(str);
//        return num;
//    }
//    runtime_error(&vm, "Object cannot be converted to number\n");
//    return -1;
//}

void gc_init(eGc* gc)
{
    gc->obj_max = GC_THRESOLD;
    gc->obj_count = 0;
    gc->objects = NULL;
}

void gc_mark(eObject* objp)
{
    if (OBJP2GCOBJP(objp)->marked)return;
    OBJP2GCOBJP(objp)->marked = 1;
}

int gc_sweep(eGc* gc)
{

    int ocount = gc->obj_count;

    eGcObject* object = gc->objects;
    while (object)
    {
        if (!object->marked)
        {
            eGcObject* unreached = object;
//            printf("sweep object @ %p\n", unreached);
            object = unreached->next;
            collect(unreached);

            gc->obj_count--;
        }
        else
        {
            object->marked = 0;
            object = object->next;
        }

//        if (gc->obj_count < GC_THRESOLD)break;
    }
//    printf("objects remaining %lu\n", gc->obj_count);
    return gc->obj_count;
}

void run_gc(eGc* gc)
{
    eGcObject* curr = gc->objects;
    while (curr != NULL)
    {
        eGcObject* next = curr->next;
        collect(curr);
        curr = next;
    }
}

eGcObject* egcobject_new(ValueType type, esize_t size)
{
//    printf("Allocating obj %lu \n",gc.obj_count);
//    if (gc.obj_count == gc.obj_max)
//    {  gc_run(&gc, &vm); }

    eGcObject* gcobj = emalloc(size);
    gcobj->type = type;
    gcobj->marked = 0;
    gcobj->next  = gc.objects ;
    gc.objects = gcobj;
    gc.obj_count++;

    return gcobj;
}

eString* estring_new(const char* text)
{
    eString* s = (eString*)egcobject_new(ESTRING, sizeof(eString));
    s->size = strlen(text);
    s->text = copy_buffer(text, s->size);

    return s;
}

void estring_delete(eString* s)
{
    free(s->text);
    free(s);
    s = NULL;
}

void estring_append(eString* s, int ch)
{
    s->text = erealloc(s->text, s->size + 2);
    s->text[s->size] = ch;
    s->text[s->size + 1] = '\0';
    s->size++;
}

char* copy_buffer(const char* ptr, int len)
{
    char* heap = emalloc(sizeof(char) * (len + 1));
    memcpy(heap, ptr, len + 1);
    heap[len] = '\0';
    return heap;
}

eObject estring_fromtext(const char* text, int len)
{
    char* buf = copy_buffer(text, len);
    eObject obj = eobj_string(buf);
    free(buf);
    return obj;
}

eObject efmtstring_fromtext(const char* text, int len)
{
    char* str = copy_buffer(text, len);
    char* ps = str;

    char* buf = emalloc(len + 1);
    char* pb = buf;

    do
    {
        switch(*ps){
        case '\\':
            ps++;
            switch (*ps) {
            case '\\':
                *pb++ = '\\';
                break;
            case 'n':
                *pb++ = '\n';
                break;
            case 't':
                *pb++ = '\t';
                break;
            case '\'':
                *pb++ = '\'';
                break;
            case '"':
                *pb++ = '"';
                break;
            }
            break;
        default:
            *pb = *ps; pb++;
            break;
        }
    }
    while (*ps++);
    buf[len]='\0';
    eObject obj = eobj_string(buf);
    free(buf); free(str);
    return obj;
}

eObject eobj_string(const char* text)
{
    eObject obj;
    obj.type = ESTRING;
    obj.value.object = estring_new(text);

    return obj;
}

bool eobj_isequal(eObject a, eObject b)
{
    if (a.type != b.type)return false;

    switch (a.type)
    {
    case ENIL:return b.type == ENIL;
    case EBOOL:return OBJ_AS_BOOL(a) == OBJ_AS_BOOL(b);
    case ENUMBER:return OBJ_AS_NUM(a) == OBJ_AS_NUM(b);
    case ESTRING:
        if (OBJ_AS_STR(a)->size != OBJ_AS_STR(b)->size)
            return false;
        return memcmp(OBJ_AS_CSTR(a), OBJ_AS_CSTR(b), OBJ_AS_STR(a)->size) == 0;
    case EFUNCTION:
        return memcmp(OBJ_AS_FUNC(a), OBJ_AS_FUNC(b), sizeof(eFunction)) == 0;
    case ECLOSURE:
        return memcmp(OBJ_AS_FUNC(a), OBJ_AS_FUNC(b), sizeof(ClosureFunc)) == 0;
    case ECLASS:
        return memcmp(OBJ_AS_CLASS(a), OBJ_AS_CLASS(b), sizeof(eClass)) == 0;
    case ENONE:
        return b.type == ENONE;
    default:
        break;
    }
    return false;
}

bool eobj_istrue(eObject obj)
{
    switch (obj.type)
    {
    case ENIL:return false;
    case EBOOL: return OBJ_AS_BOOL(obj);
    case ENUMBER:return (bool)(OBJ_AS_NUM(obj) != 0);
    case ESTRING:
        return (bool)(OBJ_AS_STR(obj)->size > 0);
    case EVECTOR:
        return (bool)(OBJ_AS_VEC(obj)->size > 0);
    default:
        return false;
        break;
    }
    return false;
}

char* eobj_to_cstr(eObject obj)
{
    char* buf = emalloc(LARGE_BUFFER);
    memset(buf, 0, LARGE_BUFFER);
    switch (obj.type)
    {
    case ENIL:
        sprintf(buf, "%s", "nil");
        break;
    case ENONE:
        sprintf(buf, "%s", "none");
        break;
    case EBOOL:
        sprintf(buf,"%s", OBJ_AS_BOOL(obj) ? "true" : "false");
        break;
    case ENUMBER:
        sprintf(buf, NUM_FRMT, OBJ_AS_FLOAT(obj));
        break;
    case ESTRING:
        sprintf(buf, "%s", OBJ_AS_STR(obj)->text);
        break;
    case EVECTOR:
        evector_to_cstring(OBJ_AS_VEC(obj), buf);
        break;
    case EDICT:
        edict_to_cstring(OBJ_AS_DICT(obj), buf);
        break;
    case EITER:
        sprintf(buf, PTR_FMT, "iterator", OBJ_AS_ITER(obj));
        break;
    case ESTREAM:
        sprintf(buf, PTR_FMT, "stream", OBJ_AS_STREAM(obj));
        break;
    case ETTIME:
        sprintf(buf, PTR_FMT, "time", OBJ_AS_TIME(obj));
        break;
    case EFUNCTION:
        sprintf(buf, "<func, '%s'>",  funcname(obj));
        break;
    case ECLOSURE:
        sprintf(buf, "<closure, '%s'> ",  closurename(obj));
        break;
    case ECLASS:
        sprintf(buf, "<%s, '%s'>", OBJ_AS_CLASS(obj)->is_inst ?
                    "instance" : "class", classname(obj));
        break;
    default:
        break;
    }

    return buf;
}

void print_object(eObject obj)
{
    switch (obj.type) {
    case ENIL:
        printf("nil");
        break;
    case ENONE:
        printf("none");
        break;
    case EBOOL:
        printf("%s", OBJ_AS_BOOL(obj) ? "true" : "false");
        break;
    case ENUMBER:
        printf(NUM_FRMT, OBJ_AS_NUM(obj));
        break;
    case ESTRING:
        printf("%s", OBJ_AS_CSTR(obj));
        break;
    case EVECTOR:
    {
        char buf[LARGE_BUFFER];
        evector_to_cstring(OBJ_AS_VEC(obj), buf);
        printf("%s", buf);
    }
        break;
    case EDICT:
    {
        char buf[LARGE_BUFFER];
        edict_to_cstring(OBJ_AS_DICT(obj), buf);
        printf("%s", buf);
    }
        break;
    case EITER:
        printf("<%s @ %p> ", "iterator", OBJ_AS_ITER(obj));
        break;
    case ETTIME:
        printf("<%s @ %p> ", "time", OBJ_AS_TIME(obj));
        break;

    case ETTMATH:
        printf("<%s @ %p> ", "math",  &obj);
        break;
    case EFUNCTION:
        printf("<func, %s> ",  funcname(obj));
        break;
    case ECLOSURE:
        printf("<closure, %s> ",  closurename(obj));
        break;
    case ECLASS:
        printf("<%s, %s> ", OBJ_AS_CLASS(obj)->is_inst ? "instance" : "class", classname(obj));
        break;
    default:
        break;
    }
}


char* eobj_type(eObject obj)
{
    switch (obj.type) {
    case ENIL: return "nil";
    case EBOOL: return "boolean";
    case ENUMBER: return "number";
    case ESTRING: return "string";
    case EVECTOR: return "vector";
    case EDICT: return "dict";
    case EFUNCTION: return "function";
    case ECLOSURE: return "closure";
    case ECLASS: return OBJ_AS_CLASS(obj)->is_inst ? "instance" : "class";
    case EITER: return "iterator";
    case ETTIME:return "time";
    case ESTREAM:return "stream";
    default: return "none";
    }
    return "none";
}

void eobj_delete(eObject obj)
{
    switch (obj.type)
    {
    case ESTRING:
        estring_delete(OBJ_AS_STR(obj));
        break;
    case EVECTOR:
        evector_delete(OBJ_AS_VEC(obj));
        break;
    case EFUNCTION:
        //efunction_delete(OBJ2FUNC(obj));
        break;
    default:
        break;
    }
}

void collect(eGcObject* obj)
{
    switch(obj->type)
    {
    case ESTRING:
    {
//        puts("DELETE STRING");
        eString* s = (eString*)obj;
        estring_delete(s);
    }
        break;
    case EVECTOR:
    {
//        puts("DELETE VECTOR");
        eVector* v = (eVector*)obj;
        evector_delete(v);
    }
        break;
    case EDICT:
    {
//        puts("DELETE DICT");
        edict_t* d = (edict_t*)obj;
        edict_delete(d);
    }
        break;
    case ESTREAM:
    {
//        puts("DELETE STRING");
        eStream* s = (eStream*)obj;
        estream_delete(s);
    }
        break;
    case EITER:
    {
//        puts("DELETE ITER");
        array_iterator* it = (array_iterator*)obj;
        delete_iter(it);
    }
        break;
    case EFUNCTION:
    {
//        puts("DELETE FUNCTION");
        eFunction* f = (eFunction*)obj;
        efunction_delete(f);
    }
        break;
    case ECLOSURE:
    {
        ClosureFunc* cl = (ClosureFunc*)obj;
        closuref_delete(cl);
    }
        break;;
    case ECLASS:
    {
        eClass* c = (eClass*)obj;
        eclass_delete(c);
    }
        break;
    case EBCODE:
    {
        eBytecode* b = (eBytecode*)obj;
        bytecode_delete(b);
    }
        break;
    case ETTIME:
    {
        eTime* time = (eTime*)obj;
        etime_delete(time);
    }
        break;

    default:
        break;
    }
}

array_iterator* make_iter(eObject* ary, bool (*has_next)(eObject*, array_iterator*))
{
    array_iterator* it = (array_iterator*)egcobject_new(EITER, sizeof(array_iterator));
    it->ary = ary;
    it->idx = 0;
    it->has_next = has_next;
    it->next = next;
    return it;
}

eObject eobj_iterator(array_iterator* it)
{
    eObject obj;
    obj.type = EITER;
    obj.value.object = it;

    return obj;
}

void delete_iter(array_iterator* it)
{
    free(it);
}

bool has_next(eObject* ary, array_iterator* it)
{
    if (ary == NULL) return false;
    if (objisstring(OBJDREF(ary)))
    {
        return it->idx < OBJ_AS_STR(*ary)->size ? true : false;
    }
    if (objisvector(OBJDREF(ary)))
    {
        return it->idx < OBJ_AS_VEC(*ary)->size ? true : false;
    }
    return false;
}

eObject next(array_iterator* it)
{
    if (it->has_next(it->ary, it))
    {
        if (objisstring(OBJDREF(it->ary)))
        {
            char* str = OBJ_AS_CSTR(OBJDREF(it->ary));
            char buf[2] = {str[it->idx], '\0'};
            it->idx++;
            return  eobj_string(buf);
        }
        if (objisvector(OBJDREF(it->ary)))
        {
            return OBJ_AS_VEC(OBJDREF(it->ary))->data[it->idx++];
        }
    }
    return NIL;
}

eObject iter(array_iterator* it)
{
    if (it->has_next(it->ary, it))
    {
        if (objisstring(OBJDREF(it->ary)))
        {
            char* str = OBJ_AS_CSTR(OBJDREF(it->ary));
            char buf[2] = {str[it->idx], '\0'};
            it->idx++;
            return  eobj_string(buf);
        }
        if (objisvector(OBJDREF(it->ary)))
        {
            return OBJ_AS_VEC(*it->ary)->data[it->idx++];
        }
    }
    return NIL;
}


// ESTREAM
//---------------------------------------------------------
eStream* estream_new()
{
    eStream* s = (eStream*)egcobject_new(ESTREAM, sizeof(eStream));
    s->size = 0;
    s->name = NULL;
    s->mode = NULL;
    s->fp = NULL;
    return s;
}

void estream_delete(eStream* stream)
{
    free(stream);
}

eObject eobj_stream(eStream* stream)
{
    eObject obj;
    obj.type = ESTREAM;
    obj.value.object = stream;

    return obj;
}

void sset_mode(eStream* s, const char* mode)
{
    s->mode = strdup(mode);
}

void sset_filename(eStream* s, const char* filename)
{
    s->name = strdup(filename);
}

int sopen(eStream* s)
{
    s->fp = fopen(s->name, s->mode);
    if (s->fp)
    {
        fseek(s->fp, 0L, SEEK_END);
        s->size = ftell(s->fp);
        rewind(s->fp);
        return 1;
    }
    return 0;
}

char* sread(eStream* s)
{
    char* buf = emalloc(s->size + 1);
    esize_t check = fread(buf, sizeof(char) ,s->size, s->fp);
    if(check != s->size){ printf("error reading file"); }
        return buf;
}

void sclose(eStream* s)
{
    fclose(s->fp);
}


eTime* etime_new()
{
    eTime* etime = (eTime*)egcobject_new(ETTIME, sizeof(eTime));

    return etime;
}

void etime_delete(eTime* time)
{
    free(time);
}

eObject eobj_time(eTime* time)
{
    eObject obj;
    obj.type = ETTIME;
    obj.value.object = time;

    return obj;
}


eMath* emath_new()
{
    eMath* emath = (eMath*)egcobject_new(ETTMATH, sizeof(eMath));
    return emath;
}

void emath_delete(eMath* math)
{
    free(math);
}

eObject eobj_math(eMath* math)
{
    eObject obj;
    obj.type = ETTMATH;
    obj.value.object = math;

    return obj;
}

eObject eobj_clone(eObject obj)
{
    switch(obj.type)
    {
    case ENIL:
        return NIL;
        break;
    case EBOOL:
        return OBJ_AS_BOOL(obj) ? BOOL(true) : BOOL(false);
        break;
    case ENUMBER:
        return NUMBER(obj.value.number);
        break;
    case ESTRING:
        return NEW_STRING(OBJ_AS_CSTR(obj));
        break;
    case EFUNCTION:
    {
        eFunction* f = efunction_clone(OBJ_AS_FUNC(obj));
        eObject func = ((eObject){ EFUNCTION, .value.object=f });

        return func;
    }
        break;
    case ECLASS:
    {
        eClass* c = eclass_clone(OBJ_AS_CLASS(obj));
        eObject klass = ((eObject){ ECLASS, .value.object=c });

        return klass;
    }
        break;
    default:
        break;
    }
    return NIL;
}

bool obj_isnumeric(eObject obj)
{
    if (objisnumber(obj) || (objisstring(obj) && isnumeric(OBJ_AS_CSTR(obj))))
    {
        return true;
    }
    return false;
}
