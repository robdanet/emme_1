#ifndef ECONSTANTS_H
#define ECONSTANTS_H

#define STACK_MAX 256

#define CALL_STACK_MAX 2048

#define BLOCK_STACK_MAX CALL_STACK_MAX

#define NUM_FRMT "%.12g"

#define QUOTE_STR_FMT "'%s'\n"

#define STR_FMT "%s\n"

#define PTR_FMT "<%s @ %p>"

#define FILE_LINE_FMT(err) "[%s][Line %lu]\n"err

#define NUM_DIGITS_MAX 20

#define PATH_LENGTH_MAX 256

#define SMALL_BUFFER 256

#define LARGE_BUFFER 1024

#define FUNC_ARITY 32

#define ARRAY_GROW_SZ   8

#if defined(_WIN32)
#define EMME_LIB_PATH ".\\modtest\\"
#else
#define EMME_LIB_PATH "./modtest/"
#endif

#endif // ECONSTANTS
