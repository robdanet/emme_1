TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
#QMAKE_CFLAGS += -O2 -march=native -fomit-frame-pointer -fno-signed-zeros -funsafe-math-optimizations -fassociative-math -freciprocal-math -Wall -Wextra -Wstrict-aliasing

#QMAKE_CFLAGS += -DDEBUG_LOG
#QMAKE_CFLAGS += -DTRACE_STACK
#QMAKE_CFLAGS += -DTRACE_CSTACK
#QMAKE_CFLAGS += -DTRACE_CALLERS_STACK
#QMAKE_CFLAGS += -DTRACE_LOOP_STACK
QMAKE_CFLAGS += -DTRACE_ASM
QMAKE_CFLAGS += -DEMME_EXEC
#QMAKE_LFLAGS += -rdynamic -lm -L/-dl


LIBS +=  /usr/lib/x86_64-linux-gnu/libdl.so

SOURCES += main.c \
    eobject.c \
    edict.c \
    elexer.c \
    etypes.c \
    evector.c \
    ebytecode.c \
    efunction.c \
    edisasm.c \
    ecompiler.c \
    elibs.c \
    evm.c \
    eerrors.c

HEADERS += \
    eobject.h \
    edict.h \
    elexer.h \
    etypes.h \
    evector.h \
    ebytecode.h \
    efunction.h \
    econstants.h \
    edisasm.h \
    ecompiler.h \
    elibs.h \
    evm.h \
    eerrors.h \
    emme.h

DISTFILES += \
    file.emme

