#ifndef EVECTOR_H
#define EVECTOR_H

#include "eobject.h"


#define VECTOR_GROW_SZ 2

typedef struct eVector
{
    struct eGcObject gcobj;
    esize_t size;
    esize_t space;
    eObject* data;
}eVector;

eVector* evector_new();
eObject eobj_vector(eVector *v);
void evector_delete(eVector* v);
esize_t evector_push(eVector* v, eObject element);
eObject evector_pop(eVector* v);
eObject evector_find(eVector* v, eObject look, bool* found, int *idx);
eObject evector_splice(eVector* v, esize_t idx, esize_t num);
void evector_to_cstring(eVector* v, char* result);

#endif // EVECTOR_H

