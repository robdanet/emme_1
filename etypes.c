#include "etypes.h"

void* emalloc(esize_t n)
{
    void* p = malloc(n);
    if (!p)
    {
        fprintf(stderr, "[%s:%d]Out of memory(%lu bytes)\n",
                __FILE__, __LINE__, (unsigned long int)n);
        exit(EXIT_FAILURE);
    }
    return p;
}

void* erealloc(void* p, esize_t n)
{
    void * t = realloc(p,n);
    if (!t && n != 0)
    {
        free(p);/* free the original pointer */
        fprintf(stderr, "[%s:%d]Out of memory(%lu bytes)\n",
                __FILE__, __LINE__, (unsigned long int)n);
        exit(EXIT_FAILURE);
    }

    return t;
}

/*
  Easy embeddable cross-platform high resolution timer function. For each
  platform we select the high resolution timer. You can call the 'ns()'
  function in your file after embedding this.

  Time 	            In nanoseconds
  1 second 	        1.000.000.000 ns
  1 milli second 	1.000.000 ns
  1 micro second 	1.000 ns

  http://roxlu.com
*/

#include <stdint.h>
#if defined(__linux)
#  define HAVE_POSIX_TIMER
#  include <time.h>
#  ifdef CLOCK_MONOTONIC
#     define CLOCKID CLOCK_MONOTONIC
#  else
#     define CLOCKID CLOCK_REALTIME
#  endif
#elif defined(__APPLE__)
#  define HAVE_MACH_TIMER
#  include <mach/mach_time.h>
#elif defined(_WIN32)
#  define WIN32_LEAN_AND_MEAN
#  include <windows.h>
#endif
static uint64_t ns() {
    static uint64_t is_init = 0;
#if defined(__APPLE__)
    static mach_timebase_info_data_t info;
    if (0 == is_init) {
        mach_timebase_info(&info);
        is_init = 1;
    }
    uint64_t now;
    now = mach_absolute_time();
    now *= info.numer;
    now /= info.denom;
    return now;
#elif defined(__linux)
    static struct timespec linux_rate;
    if (0 == is_init) {
        clock_getres(CLOCKID, &linux_rate);
        is_init = 1;
    }
    uint64_t now;
    struct timespec spec;
    clock_gettime(CLOCKID, &spec);
    now = spec.tv_sec * 1.0e9 + spec.tv_nsec;
    return now;
#elif defined(_WIN32)
    static LARGE_INTEGER win_frequency;
    if (0 == is_init) {
        QueryPerformanceFrequency(&win_frequency);
        is_init = 1;
    }
    LARGE_INTEGER now;
    QueryPerformanceCounter(&now);
    return (uint64_t) ((1e9 * now.QuadPart)  / win_frequency.QuadPart);
#endif
}

uint64_t eraw_time(void)
{
    return ns();
}

Input* input_new(FILE* file, const char* path)
{
    Input* input =  emalloc(sizeof(Input));

    file = fopen(path, "rb");

    if (!file)
    {
        fprintf(stderr, "Error opening file %s.\n", path);
        free(input);
        return NULL;
    }

    fseek(file ,0L ,SEEK_END);

    input->text_len = ftell(file);
    rewind(file);

    input->text = emalloc(sizeof(char) * (input->text_len + 1));
    memset(input->text, '\0', input->text_len + 1);

    size_t result = fread(input->text ,sizeof(char) ,input->text_len , file);

    if (result != input->text_len)
    {
        fprintf(stderr, "Error reading file %s.\n", path);
        free(input);
        return NULL;
    }

    input->text[result] = '\0';

    fclose(file);

    strcpy(input->fname, path);
    input->path_len = strlen(path);
    input->fname[input->path_len] = '\0';

    return input;
}

char* file_text_get(const char* filename)
{
    FILE* fp = NULL;
    Input* in = input_new(fp, filename);
    char* text = in->text;
    input_delete(in);
    return text;
}

void input_delete(Input* input)
{
    free(input->text);
    free(input);
    input = NULL;
}

char* quotes(const char* str)
{
    esize_t l = strlen(str) + 3;
    char* buf = emalloc(l);
    strcpy(buf, "\'");
    strcat(buf, str);
    strcat(buf, "\'");
    buf[l] = '\0';

    return buf;
}

int isnumeric (const char * s)
{
    if (s == NULL || *s == '\0' || isspace(*s))
      return 0;
    char * p;
    strtod (s, &p);
    return *p == '\0';
}
